# lime-i18n

## 安装
在市场导入即可

## 使用

```js
// main.uts
import { createI18n } from '@/uni_modules/lime-i18n'

//目录自己决定
import zhCN from './locales/zh-CN'
import enUS from './locales/en_US'

const i18n = createI18n({
  locale: 'zh-cn', // 默认显示语言
  fallbackLocale: 'en-us',
  messages: {
    'zh-cn': zhCN,
    'en-us': enUS
  }
}) 

export function createApp(){
	const app = createSSRApp(App);
	app.use(i18n)
	//....
}
```




### 切换语言

```html
<template>
	<view>
		<text>测试：{{ $t('headMenus.userName') }}</text>
		<button @click="onClick">切换{{locale}}</button>
	</view>
</template>
```
```js
import {useI18n} from '@/uni_modules/lime-i18n';
const { locale } = useI18n() 
	locale.value = locale.value != 'zh-cn' ? 'zh-cn' : 'en-us'
}
```


### 延迟加载
```js
import { setLocaleMessage } from '@/uni_modules/lime-i18n';

// 模拟请求后端接口返回
setTimeout(() => {
	setLocaleMessage('zh-cn', zhCN)
}, 5000)

```