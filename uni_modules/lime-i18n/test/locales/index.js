import en from './en.json'
import zh from './zh.json'
import id from './id.json'
import ms from './ms.json'
import th from './th.json'
import Vue from 'vue';
import VueI18n from 'vue-i18n'
import config from '@/config'

Vue.use(VueI18n)

const i18n = new VueI18n({
	// locale: uni.getStorageSync('locale') ? uni.getStorageSync('locale') : config.languageList.some(language =>
	// 	language.languageValue === uni.getLocale().split('-')[0]) ? uni.getLocale().split('-')[0] :
	// 'en', //默认为中文
	locale: uni.getStorageSync('locale') ? uni.getStorageSync('locale') : uni.getLocale().split('-')[0], //默认为中文
	messages: {
		en,
		id,
		ms,
		th,
		'zh': zh,
	},
	fallbackLocale: 'en', //如果没有找到要显示的语言，则默认显示 ‘en’
	silentTranslationWarn: true,
})
export default i18n