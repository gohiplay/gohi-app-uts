import request from '@/utils/request'

// export function listMemCollectEpisode(data):Promise<RootResponse<UTSJSONObject>> {
// 	return request({
// 		url: '/video/app/mem/collection/list',
// 		method: 'post',
// 		data: data
// 	})
// }

// export function listMemHistoryEpisode(data):Promise<RootResponse<UTSJSONObject>> {
// 	return request({
// 		url: '/video/app/mem/history/list',
// 		method: 'post',
// 		data: data
// 	})
// }

// export function like(data):Promise<RootResponse<UTSJSONObject>> {

// 	return request({
// 		url: `/video/app/mem/episode/like/${data.id}/${data.likeFlag}`,
// 		method: 'post',
// 		data: {}
// 	})
// }

// export function history(data) :Promise<RootResponse<UTSJSONObject>>{
// 	return request({
// 		url: `/video/app/mem/history/save`,
// 		method: 'post',
// 		data: data
// 	})
// }
// export function collect(data):Promise<RootResponse<UTSJSONObject>> {
// 	return request({
// 		url: `/video/app/mem/collection/collect/${data.id}/${data.collectFlag}`,
// 		method: 'post',
// 		data: {}
// 	})
// }
// export function unlock(data) :Promise<RootResponse<UTSJSONObject>>{
// 	return request({
// 		url: `/video/app/mem/episode/unlock`,
// 		method: 'post',
// 		data: data
// 	})
// }
// //是否自动下一级
// export function autoUnlockNextEpisode(autoUnlockFlag) {
// 	return request({
// 		url: `/member/app/member/autoUnlock/${autoUnlockFlag}`,
// 		method: 'get',
// 	})
// }
// export function score(data) :Promise<RootResponse<UTSJSONObject>>{
// 	return request({
// 		url: `/video/app/mem/episode/score/${data.id}/${data.score}`,
// 		method: 'post',
// 		data: {}
// 	})
// }


// export function getConfig(data):Promise<RootResponse<UTSJSONObject>> {
// 	return request({
// 		url: `/video/app/member/recharge/config`,
// 		method: 'post',
// 		data: data
// 	})
// }
// export function payment(data):Promise<RootResponse<UTSJSONObject>> {
// 	return request({
// 		url: `/video/app/member/recharge/payment`,
// 		method: 'post',
// 		data: data
// 	})
// }
// export function createPaymentIntent(data) {
// 	return request({
// 		url: `/video/app/member/recharge/createPaymentIntent`,
// 		method: 'post',
// 		data: data,
// 		showLoading: true
// 	})
// }

// export function listRecharge(data) {
// 	return request({
// 		url: '/video/app/member/recharge/list',
// 		method: 'post',
// 		data: data
// 	})
// }


// export function rechargeCallback(data) {
// 	return request({
// 		url: '/video/app/member/recharge/payBack',
// 		method: 'post',
// 		data: data
// 	})
// }
// export function returnQuery(id) {
// 	return request({
// 		url: `/video/app/notify/return?id=${id}`,
// 		method: 'get'
// 	})
// }

// export function queryResult(id, type) {
// 	return request({
// 		url: `/video/app/member/recharge/queryResult/${id}/${type}`,
// 		method: 'get'
// 	})
// }