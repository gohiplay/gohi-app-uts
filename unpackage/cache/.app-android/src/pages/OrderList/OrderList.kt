@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
open class GenPagesOrderListOrderList : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onReady(fun() {
            this.`$swiperWidth` = (this.`$refs`["swiper"] as Element).getBoundingClientRect().width;
        }
        , instance);
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_order_page = resolveComponent("order-page");
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("height" to ((50 + _ctx.statusBarHeight) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon.png", "class" to "image"))
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "search"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/fdj_icon.png", "class" to "icon")),
                    createElementVNode("input", utsMapOf("type" to "text", "placeholder" to "搜索我的订单", "class" to "input"))
                )),
                createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "text"), "搜索")
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "order-tab", "style" to normalizeStyle(utsMapOf("top" to ((50 + _ctx.statusBarHeight) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(0);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.swiperIndex === 0))
                    ))), "待付款", 2),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "line",
                        utsMapOf("active-line" to (_ctx.swiperIndex === 0))
                    ))), null, 2)
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(1);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.swiperIndex === 1))
                    ))), "待发货", 2),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "line",
                        utsMapOf("active-line" to (_ctx.swiperIndex === 1))
                    ))), null, 2)
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(2);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.swiperIndex === 2))
                    ))), "待收货", 2),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "line",
                        utsMapOf("active-line" to (_ctx.swiperIndex === 2))
                    ))), null, 2)
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(3);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.swiperIndex === 3))
                    ))), "已完成", 2),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "line",
                        utsMapOf("active-line" to (_ctx.swiperIndex === 3))
                    ))), null, 2)
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(4);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.swiperIndex === 4))
                    ))), "已取消", 2),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "line",
                        utsMapOf("active-line" to (_ctx.swiperIndex === 4))
                    ))), null, 2)
                ), 8, utsArrayOf(
                    "onClick"
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "order-info", "style" to normalizeStyle(utsMapOf("marginTop" to ((110 + _ctx.statusBarHeight) + "px")))), utsArrayOf(
                createElementVNode("swiper", utsMapOf("ref" to "swiper", "class" to "swiper-view", "current" to _ctx.swiperIndex, "onTransition" to _ctx.onSwiperTransition, "onAnimationfinish" to _ctx.onSwiperAnimationfinish), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.swiperList, fun(item, index, _, _): VNode {
                        return createElementVNode("swiper-item", utsMapOf("class" to "swiper-item", "key" to index), utsArrayOf(
                            createVNode(_component_order_page, utsMapOf("ref_for" to true, "ref" to "OrderPage", "type" to item.type, "preload" to item.preload), null, 8, utsArrayOf(
                                "type",
                                "preload"
                            ))
                        ));
                    }
                    ), 128)
                ), 40, utsArrayOf(
                    "current",
                    "onTransition",
                    "onAnimationfinish"
                ))
            ), 4)
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var swiperList: UTSArray<SwiperViewItem> by `$data`;
    open var swiperIndex: Number by `$data`;
    open var `$animationFinishIndex`: Number by `$data`;
    open var `$swiperWidth`: Number by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "swiperList" to utsArrayOf<SwiperViewItem>(SwiperViewItem(type = "UpdatedDate", name = "待付款", preload = true), SwiperViewItem(type = "FreeHot", name = "待发货"), SwiperViewItem(type = "PaymentHot", name = "待收货"), SwiperViewItem(type = "HotList", name = "已完成"), SwiperViewItem(type = "HotList", name = "已取消")), "swiperIndex" to 0, "\$animationFinishIndex" to 0, "\$swiperWidth" to 0);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.initSwiperItemData = fun(index: Number) {
            if (!this.swiperList[index].preload) {
                this.swiperList[index].preload = true;
            }
        }
        ;
        this.setSwiperIndex = fun(index: Number, updateIndicator: Boolean) {
            if (this.swiperIndex === index) {
                return;
            }
            this.swiperIndex = index;
            this.initSwiperItemData(index);
            if (updateIndicator) {
            }
        }
        ;
        this.onSwiperTransition = fun(e: SwiperTransitionEvent) {
            val offset_x = e.detail.dx;
            val current_offset_x = offset_x % this.`$swiperWidth`;
            val current_offset_i = offset_x / this.`$swiperWidth`;
            val current_index = this.`$animationFinishIndex` + current_offset_i.toInt();
            var move_to_index = current_index;
            if (current_offset_x > 0 && move_to_index < this.swiperList.length - 1) {
                move_to_index += 1;
            } else if (current_offset_x < 0 && move_to_index > 0) {
                move_to_index -= 1;
            }
            val percentage = Math.abs(current_offset_x) / this.`$swiperWidth`;
            this.initSwiperItemData(move_to_index);
        }
        ;
        this.onSwiperAnimationfinish = fun(e: SwiperAnimationFinishEvent) {
            this.setSwiperIndex(e.detail.current, true);
            this.`$animationFinishIndex` = e.detail.current;
        }
        ;
        this.onTab = fun(index: Number) {
            this.setSwiperIndex(index, false);
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var initSwiperItemData: (index: Number) -> Unit;
    open lateinit var setSwiperIndex: (index: Number, updateIndicator: Boolean) -> Unit;
    open lateinit var onSwiperTransition: (e: SwiperTransitionEvent) -> Unit;
    open lateinit var onSwiperAnimationfinish: (e: SwiperAnimationFinishEvent) -> Unit;
    open lateinit var onTab: (index: Number) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "head-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "back" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "image" to utsMapOf(".head-info .back " to utsMapOf("width" to "48rpx", "height" to "48rpx")), "search" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1, "height" to "60rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "backgroundColor" to "#f8f8f8", "borderRadius" to "40rpx")), "icon" to utsMapOf(".head-info .search " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "10rpx")), "input" to utsMapOf(".head-info .search " to utsMapOf("flex" to 1, "height" to "100%", "fontSize" to "28rpx", "color" to "#333333")), "btn" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "flex-end", "height" to "60rpx", "marginLeft" to "20rpx")), "text" to utsMapOf(".head-info .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-tab .tab " to utsMapOf("fontSize" to "30rpx", "color" to "#333333")), "order-tab" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff")), "tab" to utsMapOf(".order-tab " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "20%", "height" to "100%")), "active" to utsMapOf(".order-tab .tab " to utsMapOf("color" to "#fe9a5c", "fontSize" to "32rpx", "fontWeight" to "bold")), "line" to utsMapOf(".order-tab .tab " to utsMapOf("width" to "30%", "height" to "8rpx", "marginTop" to "6rpx", "backgroundColor" to "rgba(0,0,0,0)", "borderRadius" to "20rpx")), "active-line" to utsMapOf(".order-tab .tab " to utsMapOf("backgroundColor" to "#fe9a5c")), "order-info" to padStyleMapOf(utsMapOf("width" to "100%")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf("OrderPage" to GenPagesOrderListComponentsOrderPageOrderPageClass);
    }
}
