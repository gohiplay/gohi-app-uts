@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesOrderListComponentsOrderPageOrderPage : VueComponent {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            console.log(this.dataList, "1111111111", " at pages/OrderList/components/OrderPage/OrderPage.uvue:135");
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("list-view", utsMapOf("ref" to "listView", "id" to _ctx.id, "class" to "list", "rebound" to false, "scroll-y" to true, "custom-nested-scroll" to true, "show-scrollbar" to false, "nested-scroll-child" to "goods-many", "onScrolltolower" to fun(){
            _ctx.loadData(null);
        }
        ), utsArrayOf(
            createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.dataList, fun(item, index, _, _): VNode {
                return createElementVNode("list-item", utsMapOf("class" to "list-item", "onClick" to fun(){
                    _ctx.onOrder(item);
                }
                , "key" to index), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "shop-status"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "shop"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/shop_icon.png", "class" to "icon")),
                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.shopName), 1),
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "more"))
                            )),
                            createElementVNode("view", utsMapOf("class" to "status"), utsArrayOf(
                                if (item.status === 0) {
                                    createElementVNode("text", utsMapOf("key" to 0, "class" to "red-status"), "待付款");
                                } else {
                                    if (item.status === 1) {
                                        createElementVNode("text", utsMapOf("key" to 1, "class" to "success"), "待发货");
                                    } else {
                                        if (item.status === 2) {
                                            createElementVNode("text", utsMapOf("key" to 2, "class" to "success"), "待收货");
                                        } else {
                                            if (item.status === 3) {
                                                createElementVNode("text", utsMapOf("key" to 3, "class" to "green-status"), "已完成");
                                            } else {
                                                if (item.status === 4) {
                                                    createElementVNode("text", utsMapOf("key" to 4, "class" to "text"), "已取消");
                                                } else {
                                                    createCommentVNode("v-if", true);
                                                }
                                                ;
                                            }
                                            ;
                                        }
                                        ;
                                    }
                                    ;
                                }
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "goods-info"), utsArrayOf(
                            if (item.goodsList.length <= 1) {
                                createElementVNode("view", utsMapOf("key" to 0), utsArrayOf(
                                    createElementVNode(Fragment, null, RenderHelpers.renderList(item.goodsList, fun(value, index, _, _): VNode {
                                        return createElementVNode("view", utsMapOf("class" to "goods", "key" to index), utsArrayOf(
                                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                                createElementVNode("image", utsMapOf("src" to value.pic, "class" to "image"), null, 8, utsArrayOf(
                                                    "src"
                                                ))
                                            )),
                                            createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(value.name), 1)
                                            )),
                                            createElementVNode("view", utsMapOf("class" to "price-number"), utsArrayOf(
                                                createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                                    createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                                    createElementVNode("text", utsMapOf("class" to "max"), "99"),
                                                    createElementVNode("text", utsMapOf("class" to "min"), ".00")
                                                )),
                                                createElementVNode("view", utsMapOf("class" to "number"), utsArrayOf(
                                                    createElementVNode("text", utsMapOf("class" to "text"), "共1件")
                                                ))
                                            ))
                                        ));
                                    }), 128)
                                ));
                            } else {
                                createCommentVNode("v-if", true);
                            }
                            ,
                            if (item.goodsList.length > 1) {
                                createElementVNode("view", utsMapOf("key" to 1, "class" to "goods-many"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "goods-pic"), utsArrayOf(
                                        createElementVNode(Fragment, null, RenderHelpers.renderList(item.goodsList, fun(value, index, _, _): VNode {
                                            return createElementVNode("view", utsMapOf("class" to "pic", "key" to index), utsArrayOf(
                                                createElementVNode("image", utsMapOf("src" to value.pic, "class" to "image"), null, 8, utsArrayOf(
                                                    "src"
                                                ))
                                            ));
                                        }), 128)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "price-number"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                            createElementVNode("text", utsMapOf("class" to "max"), "99"),
                                            createElementVNode("text", utsMapOf("class" to "min"), ".00")
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "number"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "共1件")
                                        ))
                                    ))
                                ));
                            } else {
                                createCommentVNode("v-if", true);
                            }
                        )),
                        createElementVNode("view", utsMapOf("class" to "order-btn"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "删除订单")
                            )),
                            if (isTrue(item.status >= 2 && item.status < 4)) {
                                createElementVNode("view", utsMapOf("key" to 0, "class" to "btn"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "查看物流")
                                ));
                            } else {
                                createCommentVNode("v-if", true);
                            }
                            ,
                            if (item.status === 2) {
                                createElementVNode("view", utsMapOf("key" to 1, "class" to "btn active"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "确认收货")
                                ));
                            } else {
                                createCommentVNode("v-if", true);
                            }
                            ,
                            if (item.status === 3) {
                                createElementVNode("view", utsMapOf("key" to 2, "class" to "btn active"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "评价")
                                ));
                            } else {
                                createCommentVNode("v-if", true);
                            }
                            ,
                            if (item.status === 0) {
                                createElementVNode("view", utsMapOf("key" to 3, "class" to "btn active"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "继续付款")
                                ));
                            } else {
                                createCommentVNode("v-if", true);
                            }
                        ))
                    ))
                ), 8, utsArrayOf(
                    "onClick"
                ));
            }
            ), 128),
            createElementVNode("list-item", null, utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "loading"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "loading-text"), toDisplayString(_ctx.loadingText), 1)
                ))
            ))
        ), 40, utsArrayOf(
            "id",
            "onScrolltolower"
        ));
    }
    open var type: String by `$props`;
    open var preload: Boolean by `$props`;
    open var id: String by `$props`;
    open var dataList: UTSArray<OrderItem> by `$data`;
    open var loading: Boolean by `$data`;
    open var isEnded: Boolean by `$data`;
    open var loadingError: String by `$data`;
    open var loadingText: String by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("dataList" to OrderData as UTSArray<OrderItem>, "loading" to false, "isEnded" to false, "loadingError" to "", "loadingText" to computed<String>(fun(): String {
            if (this.loading) {
                return "加载中...";
            } else if (this.isEnded) {
                return "没有更多了";
            } else if (this.loadingError.length > 0) {
                return this.loadingError;
            } else {
                return "";
            }
        }
        ));
    }
    override fun `$initMethods`() {
        this.loadData = fun(loadComplete: (() -> Unit)?) {
            if (this.loading || this.isEnded) {
                return;
            }
            this.loading = true;
        }
        ;
        this.onOrder = fun(item: OrderItem) {
            uni_navigateTo(NavigateToOptions(url = "/pages/OrderDetails/OrderDetails?orderId=" + item.id));
        }
        ;
    }
    open lateinit var loadData: (loadComplete: (() -> Unit)?) -> Unit;
    open lateinit var onOrder: (item: OrderItem) -> Unit;
    companion object {
        var name = "OrderPage";
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("list-item" to padStyleMapOf(utsMapOf("paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "20rpx")), "item" to utsMapOf(".list-item " to utsMapOf("width" to "100%", "height" to "340rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "shop-status" to utsMapOf(".list-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx")), "shop" to utsMapOf(".list-item .item .shop-status " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "icon" to utsMapOf(".list-item .item .shop-status .shop " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "10rpx")), "text" to utsMapOf(".list-item .item .shop-status .shop " to utsMapOf("fontSize" to "26rpx", "fontWeight" to "bold", "color" to "#333333"), ".list-item .item .shop-status .status " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".list-item .item .goods-info .goods .name " to utsMapOf("width" to "100%", "height" to "80rpx", "lineHeight" to "40rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#333333"), ".list-item .item .goods-info .goods .price-number .number " to utsMapOf("fontSize" to "22rpx", "color" to "#333333"), ".list-item .item .goods-info .goods-many .price-number .number " to utsMapOf("fontSize" to "22rpx", "color" to "#333333"), ".list-item .item .order-btn .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".list-item .item .order-btn .active " to utsMapOf("color" to "#fe9a5c")), "more" to utsMapOf(".list-item .item .shop-status .shop " to utsMapOf("width" to "34rpx", "height" to "34rpx")), "status" to utsMapOf(".list-item .item .shop-status " to utsMapOf("display" to "flex", "alignItems" to "center")), "success" to utsMapOf(".list-item .item .shop-status .status " to utsMapOf("fontSize" to "26rpx", "color" to "#fe9a5c")), "green-status" to utsMapOf(".list-item .item .shop-status .status " to utsMapOf("fontSize" to "26rpx", "color" to "#158c15")), "red-status" to utsMapOf(".list-item .item .shop-status .status " to utsMapOf("fontSize" to "26rpx", "color" to "#FF0000")), "goods-info" to utsMapOf(".list-item .item " to utsMapOf("width" to "100%", "height" to "140rpx")), "goods" to utsMapOf(".list-item .item .goods-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100%")), "pic" to utsMapOf(".list-item .item .goods-info .goods " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "20rpx"), ".list-item .item .goods-info .goods-many .goods-pic " to utsMapOf("width" to "140rpx", "height" to "140rpx", "marginRight" to "10rpx")), "image" to utsMapOf(".list-item .item .goods-info .goods .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx"), ".list-item .item .goods-info .goods-many .goods-pic .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx", "borderRadius" to "10rpx")), "name" to utsMapOf(".list-item .item .goods-info .goods " to utsMapOf("flex" to 1)), "price-number" to utsMapOf(".list-item .item .goods-info .goods " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "marginLeft" to "10rpx"), ".list-item .item .goods-info .goods-many " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "flex-end", "justifyContent" to "center", "width" to "160rpx", "marginLeft" to "10rpx")), "price" to utsMapOf(".list-item .item .goods-info .goods .price-number " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "marginBottom" to "10rpx"), ".list-item .item .goods-info .goods-many .price-number " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "marginBottom" to "10rpx")), "min" to utsMapOf(".list-item .item .goods-info .goods .price-number .price " to utsMapOf("fontSize" to "22rpx", "fontWeight" to "bold", "color" to "#333333"), ".list-item .item .goods-info .goods-many .price-number .price " to utsMapOf("fontSize" to "22rpx", "fontWeight" to "bold", "color" to "#333333")), "max" to utsMapOf(".list-item .item .goods-info .goods .price-number .price " to utsMapOf("fontWeight" to "bold", "fontSize" to "34rpx", "color" to "#333333"), ".list-item .item .goods-info .goods-many .price-number .price " to utsMapOf("fontWeight" to "bold", "fontSize" to "34rpx", "color" to "#333333")), "number" to utsMapOf(".list-item .item .goods-info .goods .price-number " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".list-item .item .goods-info .goods-many .price-number " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "goods-many" to utsMapOf(".list-item .item .goods-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "100%")), "goods-pic" to utsMapOf(".list-item .item .goods-info .goods-many " to utsMapOf("display" to "flex", "flexDirection" to "row", "flex" to 1)), "order-btn" to utsMapOf(".list-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "flex-end", "width" to "100%", "height" to "100rpx")), "btn" to utsMapOf(".list-item .item .order-btn " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "160rpx", "height" to "60rpx", "marginLeft" to "20rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#333333", "borderRadius" to "60rpx")), "active" to utsMapOf(".list-item .item .order-btn " to utsMapOf("borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#fe9a5c")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf("type" to utsMapOf("type" to "String", "default" to ""), "preload" to utsMapOf("type" to "Boolean", "default" to false), "id" to utsMapOf("type" to "String", "default" to "")));
        var propsNeedCastKeys = utsArrayOf(
            "type",
            "preload",
            "id"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
