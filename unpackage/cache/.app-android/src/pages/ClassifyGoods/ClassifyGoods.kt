@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
import io.dcloud.uniapp.extapi.setNavigationBarTitle as uni_setNavigationBarTitle;
open class GenPagesClassifyGoodsClassifyGoods : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(options: OnLoadOptions) {
            val title = options.get("title") as String;
            uni_setNavigationBarTitle(SetNavigationBarTitleOptions(title = title));
            this.getData();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "classify-info"), utsArrayOf(
                createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.classifyList, fun(item, index, _, _): VNode {
                    return createElementVNode("view", utsMapOf("class" to "item", "onClick" to _ctx.onClassify, "key" to index), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                            "src"
                        )),
                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                    ), 8, utsArrayOf(
                        "onClick"
                    ));
                }
                ), 128)
            )),
            createElementVNode("view", utsMapOf("class" to "new-goods"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "上新")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "goods-item"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.dayGoods, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onGoodsItem(item);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(item.price), 1)
                            ))
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "goods-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                            _ctx.onGoodsItem(item);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                            )),
                            createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                    createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                } else {
                                    createCommentVNode("v-if", true);
                                }
                            ))
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ))
            ))
        ), 4);
    }
    open var classifyList: UTSArray<classifyItem> by `$data`;
    open var dayGoods: UTSArray<goodsItems1> by `$data`;
    open var goodsList: UTSArray<goodsItems1> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("classifyList" to ClassifyData as UTSArray<classifyItem>, "dayGoods" to dayData as UTSArray<goodsItems1>, "goodsList" to goodsData as UTSArray<goodsItems1>);
    }
    override fun `$initMethods`() {
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
        this.getData = fun() {
            this.classifyList = utsArrayOf();
            this.classifyList = ClassifyData[0].children[0].children;
            this.classifyList = this.classifyList.concat(ClassifyData[0].children[1].children);
            this.classifyList = this.classifyList.concat(ClassifyData[1].children[0].children);
        }
        ;
        this.onGoodsItem = fun(item: goodsItems1) {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsDetail/GoodsDetail?goodsId=" + item.id));
        }
        ;
        this.onClassify = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/SearchGoods/SearchGoods"));
        }
        ;
    }
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    open lateinit var getData: () -> Unit;
    open lateinit var onGoodsItem: (item: goodsItems1) -> Unit;
    open lateinit var onClassify: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "classify-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "width" to "100%", "paddingTop" to "20rpx", "paddingRight" to 0, "paddingBottom" to "20rpx", "paddingLeft" to 0)), "item" to utsMapOf(".classify-info " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "20%", "height" to "140rpx", "marginBottom" to "20rpx"), ".new-goods .goods-item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "image" to utsMapOf(".classify-info .item " to utsMapOf("width" to "80rpx", "height" to "80rpx", "marginBottom" to "10rpx", "borderRadius" to "20rpx"), ".new-goods .goods-item .item .pic " to utsMapOf("width" to "142rpx", "height" to "142rpx"), ".goods-info .goods-list .list .pic " to utsMapOf("width" to "334rpx", "height" to "334rpx", "borderRadius" to "10rpx")), "text" to utsMapOf(".classify-info .item " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".new-goods .title-info .title " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#333333"), ".new-goods .goods-item .item .price " to utsMapOf("fontWeight" to "bold", "fontSize" to "28rpx", "color" to "#ff6e65"), ".goods-info .goods-list .list .title " to utsMapOf("lines" to 2, "textOverflow" to "ellipsis", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "width" to "334rpx", "height" to "70rpx", "fontSize" to "28rpx", "color" to "#333333")), "new-goods" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginRight" to "30rpx", "marginBottom" to 0, "marginLeft" to "30rpx", "height" to "300rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "title-info" to utsMapOf(".new-goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "80rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "title" to utsMapOf(".new-goods .title-info " to utsMapOf("display" to "flex", "alignItems" to "center"), ".goods-info .goods-list .list " to utsMapOf("display" to "flex", "marginTop" to "10rpx")), "goods-item" to utsMapOf(".new-goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "10rpx")), "pic" to utsMapOf(".new-goods .goods-item .item " to utsMapOf("display" to "flex", "alignItems" to "center"), ".goods-info .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "334rpx")), "price" to utsMapOf(".new-goods .goods-item .item " to utsMapOf("display" to "flex", "alignItems" to "center", "marginTop" to "10rpx"), ".goods-info .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "goods-info" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "20rpx")), "goods-list" to utsMapOf(".goods-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "justifyContent" to "space-between", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "list" to utsMapOf(".goods-info .goods-list " to utsMapOf("width" to "334rpx", "marginBottom" to "20rpx", "paddingBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "10rpx")), "min" to utsMapOf(".goods-info .goods-list .list .price " to utsMapOf("fontSize" to "22rpx", "fontWeight" to "bold", "color" to "#ff6e65", "marginBottom" to "4rpx")), "max" to utsMapOf(".goods-info .goods-list .list .price " to utsMapOf("fontSize" to "36rpx", "fontWeight" to "bold", "color" to "#ff6e65")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
