@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenPagesLoginLogin : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {}
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_radio = resolveComponent("radio");
        return createElementVNode("view", utsMapOf("class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "logo-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "logo"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/logo.png", "class" to "image"))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "login-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "login-account"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "input-text"), utsArrayOf(
                        createElementVNode("input", utsMapOf("type" to "text", "placeholder" to "请输入账号", "value" to _ctx.loginForm["email"], "class" to "input"), null, 8, utsArrayOf(
                            "value"
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "input-text"), utsArrayOf(
                        createElementVNode("input", utsMapOf("type" to "password", "placeholder" to "请输入密码", "value" to _ctx.loginForm["password"], "class" to "input"), null, 8, utsArrayOf(
                            "value"
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "protocol"), utsArrayOf(
                        createVNode(_component_radio, utsMapOf("color" to "#fe9a5c", "checked" to _ctx.isProtocol, "onClick" to fun(){
                            _ctx.isProtocol = !_ctx.isProtocol;
                        }
                        ), null, 8, utsArrayOf(
                            "checked",
                            "onClick"
                        )),
                        createElementVNode("text", utsMapOf("class" to "text"), "我已阅读并同意"),
                        createElementVNode("text", utsMapOf("class" to "link"), "《隐私政策》"),
                        createElementVNode("text", utsMapOf("class" to "text"), "和"),
                        createElementVNode("text", utsMapOf("class" to "link"), "《服务协议》")
                    )),
                    createElementVNode("view", utsMapOf("class" to "login-btn"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "btn active", "onClick" to _ctx.onLogin), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "登录")
                        ), 8, utsArrayOf(
                            "onClick"
                        ))
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "login-other"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "text"), "其他登录方式")
                )),
                createElementVNode("view", utsMapOf("class" to "login-way"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "way"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/wx_login.png", "class" to "image"))
                    )),
                    createElementVNode("view", utsMapOf("class" to "way"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/qq_login.png", "class" to "image"))
                    ))
                ))
            ))
        ));
    }
    open var email: String by `$data`;
    open var password: String by `$data`;
    open var isProtocol: Boolean by `$data`;
    open var loginForm: UTSJSONObject by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("email" to "", "password" to "", "isProtocol" to true, "loginForm" to object : UTSJSONObject() {
            var phone = ""
            var email = ""
            var password = ""
            var codeType: Number = 0
            var uuid = ""
            var validateCode = ""
            var sourceType = "password"
        });
    }
    override fun `$initMethods`() {
        this.onLogin = fun() {
            login(this.loginForm);
        }
        ;
    }
    open lateinit var onLogin: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("logo-info" to padStyleMapOf(utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "360rpx")), "logo" to utsMapOf(".logo-info " to utsMapOf("width" to "200rpx", "height" to "200rpx")), "image" to utsMapOf(".logo-info .logo " to utsMapOf("width" to "200rpx", "height" to "200rpx"), ".login-other .login-way .way " to utsMapOf("width" to "60rpx", "height" to "60rpx")), "login-info" to padStyleMapOf(utsMapOf("width" to "100%")), "login-account" to utsMapOf(".login-info " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "input-text" to utsMapOf(".login-info .login-account " to utsMapOf("width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "marginBottom" to "30rpx", "backgroundColor" to "#f8f8f8", "borderRadius" to "100rpx")), "input" to utsMapOf(".login-info .login-account .input-text " to utsMapOf("width" to "100%", "height" to "100%", "fontSize" to "32rpx", "color" to "#333333")), "protocol" to utsMapOf(".login-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "text" to utsMapOf(".login-info .protocol " to utsMapOf("fontSize" to "28rpx", "color" to "#aaaaaa"), ".login-info .login-btn .btn " to utsMapOf("fontSize" to "32rpx", "color" to "#ffffff"), ".login-info .login-btn .active " to utsMapOf("fontSize" to "32rpx", "color" to "#ffffff"), ".login-other .title " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa")), "link" to utsMapOf(".login-info .protocol " to utsMapOf("fontSize" to "28rpx", "color" to "#0044ff")), "login-btn" to utsMapOf(".login-info " to utsMapOf("width" to "100%", "marginTop" to "30rpx")), "btn" to utsMapOf(".login-info .login-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "100rpx", "backgroundColor" to "rgba(254,154,92,0.3)", "borderRadius" to "100rpx")), "active" to utsMapOf(".login-info .login-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "100rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "100rpx")), "login-other" to padStyleMapOf(utsMapOf("position" to "fixed", "bottom" to "300rpx", "width" to "100%")), "title" to utsMapOf(".login-other " to utsMapOf("display" to "flex", "flexDirection" to "row", "justifyContent" to "center", "width" to "100%")), "login-way" to utsMapOf(".login-other " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "marginTop" to "30rpx")), "way" to utsMapOf(".login-other .login-way " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginTop" to 0, "marginRight" to "20rpx", "marginBottom" to 0, "marginLeft" to "20rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
