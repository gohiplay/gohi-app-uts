@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
open class GenPagesCouponUseRecordCouponUseRecord : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "tab-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(1);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.status === 1))
                    ))), "已使用", 2),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "line",
                        utsMapOf("active-line" to (_ctx.status === 1))
                    ))), null, 2)
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(2);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.status === 2))
                    ))), "已过期", 2),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "line",
                        utsMapOf("active-line" to (_ctx.status === 2))
                    ))), null, 2)
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(3);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.status === 3))
                    ))), "已失效", 2),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "line",
                        utsMapOf("active-line" to (_ctx.status === 3))
                    ))), null, 2)
                ), 8, utsArrayOf(
                    "onClick"
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "coupon-info", "style" to normalizeStyle(utsMapOf("margin-top" to "120rpx"))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "coupon-list"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.couponList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/yhq_sx_bg.png", "class" to "bg", "mode" to "aspectFit")),
                            createElementVNode("view", utsMapOf("class" to "status"), utsArrayOf(
                                withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/icon/syysy_icon.png", "class" to "image"), null, 512), utsArrayOf(
                                    utsArrayOf(
                                        vShow,
                                        _ctx.status === 1
                                    )
                                )),
                                withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/icon/syygq_icon.png", "class" to "image"), null, 512), utsArrayOf(
                                    utsArrayOf(
                                        vShow,
                                        _ctx.status === 2
                                    )
                                )),
                                withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/icon/syysx_icon.png", "class" to "image"), null, 512), utsArrayOf(
                                    utsArrayOf(
                                        vShow,
                                        _ctx.status === 3
                                    )
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.title), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "date"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.startDate) + "-" + toDisplayString(item.endDate), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "more", "onClick" to fun(){
                                        _ctx.onMore(index);
                                    }
                                    ), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "tit"), "详细信息"),
                                        withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/icon/down_icon.png", "class" to "more-icon"), null, 512), utsArrayOf(
                                            utsArrayOf(
                                                vShow,
                                                _ctx.moreIndex !== index
                                            )
                                        )),
                                        withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/icon/up_icon.png", "class" to "more-icon"), null, 512), utsArrayOf(
                                            utsArrayOf(
                                                vShow,
                                                _ctx.moreIndex == index
                                            )
                                        ))
                                    ), 8, utsArrayOf(
                                        "onClick"
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                            createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(item.subPrice), 1)
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "full-price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "满" + toDisplayString(item.fullPrice) + "元可用", 1)
                                        ))
                                    ))
                                ))
                            )),
                            withDirectives(createElementVNode("view", utsMapOf("class" to "more-info"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "限品类：")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "仅购买部分商品可用")
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "券编号：")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "123123xx1231231")
                                    ))
                                ))
                            ), 512), utsArrayOf(
                                utsArrayOf(
                                    vShow,
                                    _ctx.moreIndex === index
                                )
                            ))
                        ));
                    }
                    ), 128)
                ))
            ), 4)
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var statusBarBottom: Number by `$data`;
    open var status: Number by `$data`;
    open var moreIndex: Number by `$data`;
    open var couponList: UTSArray<CouponItem> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "statusBarBottom" to 0, "status" to 1, "moreIndex" to -1, "couponList" to CouponData as UTSArray<CouponItem>);
    }
    override fun `$initMethods`() {
        this.onMore = fun(index: Number) {
            this.moreIndex = index;
        }
        ;
        this.onTab = fun(status: Number) {
            this.status = status;
        }
        ;
    }
    open lateinit var onMore: (index: Number) -> Unit;
    open lateinit var onTab: (status: Number) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "tab-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff")), "tab" to utsMapOf(".tab-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "33%", "height" to "100%")), "text" to utsMapOf(".tab-info .tab " to utsMapOf("fontSize" to "30rpx", "color" to "#333333"), ".coupon-info .coupon-list .list .item .info .title " to utsMapOf("width" to "100%", "height" to "80rpx", "lines" to 2, "textOverflow" to "ellipsis", "lineHeight" to "40rpx", "fontSize" to "26rpx", "color" to "#333333", "fontWeight" to "bold"), ".coupon-info .coupon-list .list .item .info .date " to utsMapOf("fontSize" to "24rpx", "color" to "#333333"), ".coupon-info .coupon-list .list .item .price-info .info .full-price " to utsMapOf("fontSize" to "22rpx", "fontWeight" to "bold", "color" to "#ffffff"), ".coupon-info .coupon-list .list .item .price-info .info .use-btn .btn " to utsMapOf("fontSize" to "22rpx", "color" to "#ff6e65"), ".coupon-info .coupon-list .list .more-info .info .title " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa")), "active" to utsMapOf(".tab-info .tab " to utsMapOf("color" to "#fe9a5c", "fontSize" to "32rpx", "fontWeight" to "bold")), "line" to utsMapOf(".tab-info .tab " to utsMapOf("width" to "20%", "height" to "8rpx", "marginTop" to "6rpx", "backgroundColor" to "rgba(0,0,0,0)", "borderRadius" to "20rpx")), "active-line" to utsMapOf(".tab-info .tab " to utsMapOf("backgroundColor" to "#fe9a5c")), "coupon-info" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "20rpx", "marginRight" to "auto", "marginBottom" to "20rpx", "marginLeft" to "auto")), "coupon-list" to utsMapOf(".coupon-info " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "boxSizing" to "border-box")), "list" to utsMapOf(".coupon-info .coupon-list " to utsMapOf("position" to "relative", "width" to "100%", "minHeight" to "200rpx", "marginBottom" to "14rpx")), "bg" to utsMapOf(".coupon-info .coupon-list .list " to utsMapOf("width" to "100%", "height" to "200rpx")), "status" to utsMapOf(".coupon-info .coupon-list .list " to utsMapOf("position" to "absolute", "right" to "240rpx", "top" to "70rpx", "zIndex" to 1)), "image" to utsMapOf(".coupon-info .coupon-list .list .status " to utsMapOf("width" to "80rpx", "height" to "80rpx")), "item" to utsMapOf(".coupon-info .coupon-list .list " to utsMapOf("position" to "absolute", "left" to 0, "top" to 0, "zIndex" to 10, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "200rpx")), "info" to utsMapOf(".coupon-info .coupon-list .list .item " to utsMapOf("flex" to 1, "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx"), ".coupon-info .coupon-list .list .item .price-info " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "100%"), ".coupon-info .coupon-list .list .more-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "40rpx")), "date" to utsMapOf(".coupon-info .coupon-list .list .item .info " to utsMapOf("marginTop" to "10rpx")), "more" to utsMapOf(".coupon-info .coupon-list .list .item .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "marginTop" to "10rpx")), "tit" to utsMapOf(".coupon-info .coupon-list .list .item .info .more " to utsMapOf("fontSize" to "22rpx", "color" to "#aaaaaa")), "more-icon" to utsMapOf(".coupon-info .coupon-list .list .item .info .more " to utsMapOf("width" to "28rpx", "height" to "28rpx")), "price-info" to utsMapOf(".coupon-info .coupon-list .list .item " to utsMapOf("width" to "220rpx")), "price" to utsMapOf(".coupon-info .coupon-list .list .item .price-info .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "justifyContent" to "center")), "min" to utsMapOf(".coupon-info .coupon-list .list .item .price-info .info .price " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#ffffff", "marginBottom" to "6rpx")), "max" to utsMapOf(".coupon-info .coupon-list .list .item .price-info .info .price " to utsMapOf("fontSize" to "54rpx", "fontWeight" to "bold", "color" to "#ffffff")), "full-price" to utsMapOf(".coupon-info .coupon-list .list .item .price-info .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center")), "use-btn" to utsMapOf(".coupon-info .coupon-list .list .item .price-info .info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginTop" to "10rpx")), "btn" to utsMapOf(".coupon-info .coupon-list .list .item .price-info .info .use-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "140rpx", "height" to "40rpx", "backgroundColor" to "#ffffff", "borderRadius" to "60rpx")), "more-info" to utsMapOf(".coupon-info .coupon-list .list " to utsMapOf("width" to "100%", "paddingTop" to "20rpx", "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "10rpx")), "title" to utsMapOf(".coupon-info .coupon-list .list .more-info .info " to utsMapOf("display" to "flex", "alignItems" to "center")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
