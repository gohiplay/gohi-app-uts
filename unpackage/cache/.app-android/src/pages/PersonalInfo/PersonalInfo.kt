@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenPagesPersonalInfoPersonalInfo : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {}
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "info-item"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "头像")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "https://picsum.photos/200/200?id=404", "class" to "pic"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "账号")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "wuhu123")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "昵称")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "哇哈哈哈"),
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "more"))
                    ))
                ))
            ))
        ), 4);
    }
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "info-item" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "item" to utsMapOf(".info-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "minHeight" to "80rpx")), "title" to utsMapOf(".info-item .item " to utsMapOf("display" to "flex", "alignItems" to "center")), "text" to utsMapOf(".info-item .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".info-item .item .content " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa")), "content" to utsMapOf(".info-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "more" to utsMapOf(".info-item .item .content " to utsMapOf("width" to "38rpx", "height" to "38rpx")), "pic" to utsMapOf(".info-item .item .content " to utsMapOf("width" to "100rpx", "height" to "100rpx", "borderRadius" to "60rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
