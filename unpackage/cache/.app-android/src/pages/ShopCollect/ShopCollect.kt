@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
open class GenPagesShopCollectShopCollect : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("style" to normalizeStyle(utsMapOf("paddingBottom" to ((_ctx.statusBarBottom + 50) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("height" to ((50 + _ctx.statusBarHeight) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "店铺收藏")
                    )),
                    createElementVNode("view", utsMapOf("class" to "edit", "onClick" to fun(){
                        _ctx.isEdit = !_ctx.isEdit;
                    }
                    ), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(if (_ctx.isEdit) {
                            "完成";
                        } else {
                            "编辑";
                        }
                        ), 1)
                    ), 8, utsArrayOf(
                        "onClick"
                    ))
                ), 4),
                createElementVNode("view", utsMapOf("class" to "shop-info", "style" to normalizeStyle(utsMapOf("marginTop" to ((60 + _ctx.statusBarHeight) + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "shop-list"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(10, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                                if (isTrue(_ctx.isEdit)) {
                                    createElementVNode("view", utsMapOf("key" to 0, "class" to "check"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/check_off.png", "class" to "image"))
                                    ));
                                } else {
                                    createCommentVNode("v-if", true);
                                }
                                ,
                                createElementVNode("view", utsMapOf("class" to "shop-right"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "shop-data"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "logo"), utsArrayOf(
                                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/shop_default.png", "class" to "image"))
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "shop"), utsArrayOf(
                                            createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                                createElementVNode("text", utsMapOf("class" to "text"), "哇哈哈官方旗舰店" + toDisplayString(item), 1)
                                            )),
                                            createElementVNode("view", utsMapOf("class" to "tag-item"), utsArrayOf(
                                                createElementVNode("view", utsMapOf("class" to "star"), utsArrayOf(
                                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_off.png", "class" to "image"))
                                                )),
                                                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                                                    createElementVNode("text", utsMapOf("class" to "text"), "自营")
                                                ))
                                            ))
                                        ))
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "goods-item"), utsArrayOf(
                                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, _, _, _): VNode {
                                            return createElementVNode("view", utsMapOf("class" to "item", "key" to item.id), utsArrayOf(
                                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                                    "src"
                                                ))
                                            ));
                                        }
                                        ), 128)
                                    ))
                                ))
                            ));
                        }
                        ), 64)
                    ))
                ), 4),
                if (isTrue(_ctx.isEdit)) {
                    createElementVNode("view", utsMapOf("key" to 0, "class" to "footer-check", "style" to normalizeStyle(utsMapOf("paddingBottom" to (_ctx.statusBarBottom + "px")))), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "check"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/check_off.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "全选")
                        )),
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "取消收藏(1)")
                        ))
                    ), 4);
                } else {
                    createCommentVNode("v-if", true);
                }
            ), 4)
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var statusBarBottom: Number by `$data`;
    open var isEdit: Boolean by `$data`;
    open var goodsList: UTSArray<goodsItems1> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "statusBarBottom" to 0, "isEdit" to false, "goodsList" to goodsData as UTSArray<goodsItems1>);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "head-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingLeft" to "30rpx", "paddingRight" to "30rpx", "backgroundColor" to "#ffffff")), "back" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "image" to utsMapOf(".head-info .back " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".shop-info .shop-list .list .check " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".shop-info .shop-list .list .shop-right .shop-data .logo " to utsMapOf("width" to "100rpx", "height" to "100rpx", "borderRadius" to "60rpx"), ".shop-info .shop-list .list .shop-right .shop-data .shop .tag-item .star " to utsMapOf("width" to "28rpx", "height" to "28rpx", "marginRight" to "4rpx"), ".shop-info .shop-list .list .shop-right .goods-item .item " to utsMapOf("width" to "150rpx", "height" to "150rpx", "borderRadius" to "10rpx"), ".footer-check .check " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "20rpx")), "title" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center")), "text" to utsMapOf(".head-info .title " to utsMapOf("fontSize" to "32rpx", "color" to "#333333"), ".head-info .edit " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".shop-info .shop-list .list .shop-right .shop-data .shop .name " to utsMapOf("fontSize" to "32rpx", "color" to "#333333"), ".shop-info .shop-list .list .shop-right .shop-data .shop .tag-item .item " to utsMapOf("fontSize" to "22rpx", "color" to "#ffffff"), ".footer-check .check " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".footer-check .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "edit" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center")), "shop-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "20rpx")), "shop-list" to utsMapOf(".shop-info " to utsMapOf("width" to "100%")), "list" to utsMapOf(".shop-info .shop-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "340rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "check" to utsMapOf(".shop-info .shop-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx"), ".footer-check " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center")), "shop-right" to utsMapOf(".shop-info .shop-list .list " to utsMapOf("flex" to 1, "height" to "100%")), "shop-data" to utsMapOf(".shop-info .shop-list .list .shop-right " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "140rpx")), "logo" to utsMapOf(".shop-info .shop-list .list .shop-right .shop-data " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "shop" to utsMapOf(".shop-info .shop-list .list .shop-right .shop-data " to utsMapOf("flex" to 1)), "name" to utsMapOf(".shop-info .shop-list .list .shop-right .shop-data .shop " to utsMapOf("width" to "100%", "marginBottom" to "20rpx")), "tag-item" to utsMapOf(".shop-info .shop-list .list .shop-right .shop-data .shop " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%")), "star" to utsMapOf(".shop-info .shop-list .list .shop-right .shop-data .shop .tag-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginRight" to "20rpx")), "item" to utsMapOf(".shop-info .shop-list .list .shop-right .shop-data .shop .tag-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "2rpx", "paddingRight" to "10rpx", "paddingBottom" to "2rpx", "paddingLeft" to "10rpx", "marginRight" to "20rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "4rpx"), ".shop-info .shop-list .list .shop-right .goods-item " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "10rpx")), "goods-item" to utsMapOf(".shop-info .shop-list .list .shop-right " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "200rpx", "marginTop" to "10rpx")), "footer-check" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "bottom" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "minHeight" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "btn" to utsMapOf(".footer-check " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "240rpx", "height" to "70rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "40rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
