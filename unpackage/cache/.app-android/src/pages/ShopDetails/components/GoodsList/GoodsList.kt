@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesShopDetailsComponentsGoodsListGoodsList : VueComponent {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onMounted(fun() {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode(Fragment, null, utsArrayOf(
            if (isTrue(_ctx.isSearch)) {
                createElementVNode("view", utsMapOf("key" to 0, "class" to "head-info", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "search-info"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon.png", "class" to "image"))
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "search"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/fdj_icon.png", "class" to "image")),
                            createElementVNode("input", utsMapOf("type" to "text", "placeholder" to "请输入搜索内容", "class" to "input"))
                        )),
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "搜索")
                        ))
                    ))
                ), 4);
            } else {
                createCommentVNode("v-if", true);
            }
            ,
            createElementVNode("view", utsMapOf("class" to "filtrate-info", "style" to normalizeStyle(utsMapOf("top" to if (_ctx.isSearch) {
                (_ctx.statusBarHeight + 50) + "px";
            } else {
                0;
            }
            ))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "filtrate-item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item active"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "推荐")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "销量")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "价格")
                        )),
                        createElementVNode("view", utsMapOf("class" to "cut"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/sort_icon.png", "class" to "up"))
                        ))
                    ))
                ))
            ), 4),
            createElementVNode("view", utsMapOf("style" to normalizeStyle(utsMapOf("paddingBottom" to ((_ctx.statusBarBottom + 50) + "px"), "marginTop" to if (_ctx.isSearch) {
                (_ctx.statusBarHeight + 50) + "px";
            } else {
                0;
            }
            ))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "goods-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                                _ctx.onGoodsItem(item);
                            }
                            , "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                        "src"
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "sales-volume"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.salesVolume) + "件售出", 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                            createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                            if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                                createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                            } else {
                                                createCommentVNode("v-if", true);
                                            }
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "old-price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(item.originalPrice), 1)
                                        ))
                                    ))
                                ))
                            ), 8, utsArrayOf(
                                "onClick"
                            ));
                        }
                        ), 128)
                    ))
                ))
            ), 4)
        ), 64);
    }
    open var isSearch: Boolean by `$props`;
    open var statusBarHeight: Number by `$data`;
    open var statusBarBottom: Number by `$data`;
    open var goodsList: UTSArray<goodsItems1> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "statusBarBottom" to 0, "goodsList" to goodsData as UTSArray<goodsItems1>);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
        this.onGoodsItem = fun(item: goodsItems1) {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsDetail/GoodsDetail?goodsId=" + item.id));
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    open lateinit var onGoodsItem: (item: goodsItems1) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("head-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff", "zIndex" to 100)), "search-info" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "back" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100rpx", "height" to "100%")), "image" to utsMapOf(".head-info .search-info .back " to utsMapOf("width" to "58rpx", "height" to "58rpx"), ".head-info .search-info .search " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "10rpx"), ".filtrate-info .filtrate-item .item .cut " to utsMapOf("width" to "34rpx", "height" to "34rpx"), ".filtrate-info .filtrate-item .item .icon " to utsMapOf("width" to "30rpx", "height" to "30rpx"), ".goods-info .goods-list .list .pic " to utsMapOf("width" to "200rpx", "height" to "200rpx", "borderRadius" to "10rpx"), ".goods-info .goods-list .list .info .shop-info .more " to utsMapOf("width" to "28rpx", "height" to "28rpx")), "search" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1, "height" to "60rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginRight" to "20rpx", "backgroundColor" to "#f8f8f8", "borderRadius" to "30rpx")), "input" to utsMapOf(".head-info .search-info .search " to utsMapOf("flex" to 1, "height" to "100%", "fontSize" to "26rpx", "color" to "#333333")), "btn" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100rpx", "height" to "50rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "30rpx")), "text" to utsMapOf(".head-info .search-info .btn " to utsMapOf("fontSize" to "26rpx", "color" to "#ffffff"), ".filtrate-info .filtrate-item .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".filtrate-info .filtrate-item .active .title " to utsMapOf("color" to "#fe9a5c"), ".goods-info .goods-list .list .info .title " to utsMapOf("width" to "100%", "height" to "70rpx", "fontSize" to "28rpx", "color" to "#333333", "lines" to 2, "textOverflow" to "ellipsis"), ".goods-info .goods-list .list .info .sales-volume " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".goods-info .goods-list .list .info .price-info .old-price " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa", "textDecorationLine" to "line-through"), ".goods-info .goods-list .list .info .shop-info .name " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".goods-info .goods-list .list .info .shop-info .more " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa")), "filtrate-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "width" to "100%", "height" to "80rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "filtrate-item" to utsMapOf(".filtrate-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100%")), "item" to utsMapOf(".filtrate-info .filtrate-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "33%")), "title" to utsMapOf(".filtrate-info .filtrate-item .item " to utsMapOf("display" to "flex"), ".goods-info .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "cut" to utsMapOf(".filtrate-info .filtrate-item .item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center")), "up" to utsMapOf(".filtrate-info .filtrate-item .item .cut " to utsMapOf("width" to "28rpx", "height" to "28rpx")), "icon" to utsMapOf(".filtrate-info .filtrate-item .item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "marginLeft" to "20rpx")), "goods-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "80rpx", "backgroundColor" to "#ffffff")), "goods-list" to utsMapOf(".goods-info " to utsMapOf("width" to "100%", "marginTop" to "20rpx")), "list" to utsMapOf(".goods-info .goods-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "200rpx", "marginBottom" to "20rpx")), "pic" to utsMapOf(".goods-info .goods-list .list " to utsMapOf("marginRight" to "20rpx")), "info" to utsMapOf(".goods-info .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "column", "flex" to 1)), "sales-volume" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex")), "price-info" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx")), "price" to utsMapOf(".goods-info .goods-list .list .info .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "min" to utsMapOf(".goods-info .goods-list .list .info .price-info .price " to utsMapOf("fontSize" to "22rpx", "color" to "#fe9a5c", "marginBottom" to "4rpx")), "max" to utsMapOf(".goods-info .goods-list .list .info .price-info .price " to utsMapOf("fontSize" to "34rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "old-price" to utsMapOf(".goods-info .goods-list .list .info .price-info " to utsMapOf("display" to "flex", "alignItems" to "center", "marginLeft" to "10rpx")), "shop-info" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx")), "name" to utsMapOf(".goods-info .goods-list .list .info .shop-info " to utsMapOf("display" to "flex", "alignItems" to "center")), "more" to utsMapOf(".goods-info .goods-list .list .info .shop-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginLeft" to "20rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf("isSearch" to utsMapOf("type" to "Boolean", "default" to false)));
        var propsNeedCastKeys = utsArrayOf(
            "isSearch"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
