@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.redirectTo as uni_redirectTo;
open class GenPagesShopDetailsComponentsShopTabNarShopTabNar : VueComponent {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onMounted(fun() {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("view", utsMapOf("class" to "tab-bar-info", "style" to normalizeStyle(utsMapOf("paddingBottom" to (_ctx.statusBarBottom + "px")))), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "tab-item"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                    _ctx.onToNav("home");
                }
                ), utsArrayOf(
                    withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/shop/shop_nav_home_ac.png", "class" to "image"), null, 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.flagStr === "home"
                        )
                    )),
                    withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/shop/shop_nav_home.png", "class" to "image"), null, 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.flagStr !== "home"
                        )
                    )),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.flagStr === "home"))
                    ))), "首页", 2)
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                    _ctx.onToNav("goods");
                }
                ), utsArrayOf(
                    withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/shop/shop_nav_goods_ac.png", "class" to "image"), null, 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.flagStr === "goods"
                        )
                    )),
                    withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/shop/shop_nav_goods.png", "class" to "image"), null, 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.flagStr !== "goods"
                        )
                    )),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.flagStr === "goods"))
                    ))), "商品", 2)
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                    _ctx.onToNav("class");
                }
                ), utsArrayOf(
                    withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/shop/shop_nav_class_ac.png", "class" to "image"), null, 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.flagStr === "class"
                        )
                    )),
                    withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/shop/shop_nav_class.png", "class" to "image"), null, 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.flagStr !== "class"
                        )
                    )),
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.flagStr === "class"))
                    ))), "分类", 2)
                ), 8, utsArrayOf(
                    "onClick"
                ))
            ))
        ), 4);
    }
    open var flagStr: String by `$props`;
    open var statusBarHeight: Number by `$data`;
    open var statusBarBottom: Number by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "statusBarBottom" to 0);
    }
    override fun `$initMethods`() {
        this.onToNav = fun(flag: String) {
            this.flagStr = flag;
            var url = "";
            when (flag) {
                "home" -> 
                    url = "/pages/ShopDetails/ShopDetails";
                "goods" -> 
                    url = "/pages/ShopDetails/ShopGoods/ShopGoods";
                "class" -> 
                    url = "/pages/ShopDetails/ShopClassify/ShopClassify";
            }
            val currentRoute = getCurrentPages()[getCurrentPages().length - 1].route;
            val nowUrl = "/" + currentRoute;
            if (nowUrl !== url) {
                uni_redirectTo(RedirectToOptions(url = url));
            }
        }
        ;
    }
    open lateinit var onToNav: (flag: String) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("tab-bar-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "bottom" to 0, "zIndex" to 999, "width" to "100%", "backgroundColor" to "#ffffff")), "tab-item" to utsMapOf(".tab-bar-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx")), "item" to utsMapOf(".tab-bar-info .tab-item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "33%", "height" to "100%")), "image" to utsMapOf(".tab-bar-info .tab-item .item " to utsMapOf("width" to "48rpx", "height" to "48rpx", "marginBottom" to "4rpx")), "text" to utsMapOf(".tab-bar-info .tab-item .item " to utsMapOf("fontSize" to "26rpx", "color" to "#333333")), "active" to utsMapOf(".tab-bar-info .tab-item .item " to utsMapOf("fontSize" to "26rpx", "color" to "#fe9a5c")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf("flagStr" to utsMapOf("type" to "String", "default" to "home")));
        var propsNeedCastKeys = utsArrayOf(
            "flagStr"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
