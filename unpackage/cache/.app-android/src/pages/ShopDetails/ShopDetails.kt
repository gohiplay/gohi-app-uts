@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesShopDetailsShopDetails : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(options: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
            this.shopName = options.get("name") as String;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_shop_tab_nar = resolveComponent("shop-tab-nar");
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("style" to normalizeStyle(utsMapOf("paddingBottom" to ((_ctx.statusBarBottom + 50) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "shop-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "shop"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "logo"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/shop_default.png", "class" to "image"))
                        )),
                        createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(_ctx.shopName), 1),
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "more"))
                            )),
                            createElementVNode("view", utsMapOf("class" to "star-info"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "star"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_off.png", "class" to "image")),
                                    createElementVNode("text", utsMapOf("class" to "text"), "4.0")
                                )),
                                createElementVNode("view", utsMapOf("class" to "tags"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "自营")
                                    ))
                                ))
                            ))
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "collect"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "关注")
                        ))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "hot-goods"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "hot-title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "热销商品")
                    )),
                    createElementVNode("scroll-view", utsMapOf("class" to "goods-list", "scroll-x" to true), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.dayGoods, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                                _ctx.onGoodsItem(item);
                            }
                            , "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                        "src"
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                    createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(item.price), 1)
                                ))
                            ), 8, utsArrayOf(
                                "onClick"
                            ));
                        }
                        ), 128)
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "goods-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                                _ctx.onGoodsItem(item);
                            }
                            , "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                        "src"
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "sales-volume"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.salesVolume) + "件售出", 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                            createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                            if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                                createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                            } else {
                                                createCommentVNode("v-if", true);
                                            }
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "old-price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(item.originalPrice), 1)
                                        ))
                                    ))
                                ))
                            ), 8, utsArrayOf(
                                "onClick"
                            ));
                        }
                        ), 128)
                    ))
                ))
            ), 4),
            createVNode(_component_shop_tab_nar, utsMapOf("flagStr" to "home"))
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var statusBarBottom: Number by `$data`;
    open var shopName: String by `$data`;
    open var dayGoods: UTSArray<goodsItems1> by `$data`;
    open var goodsList: UTSArray<goodsItems1> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "statusBarBottom" to 0, "shopName" to "", "dayGoods" to dayData as UTSArray<goodsItems1>, "goodsList" to goodsData as UTSArray<goodsItems1>);
    }
    override fun `$initMethods`() {
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
        this.onGoodsItem = fun(item: goodsItems1) {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsDetail/GoodsDetail?goodsId=" + item.id));
        }
        ;
    }
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    open lateinit var onGoodsItem: (item: goodsItems1) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "shop-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "120rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "30rpx")), "shop" to utsMapOf(".shop-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1)), "logo" to utsMapOf(".shop-info .shop " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "20rpx")), "image" to utsMapOf(".shop-info .shop .logo " to utsMapOf("width" to "100rpx", "height" to "100rpx", "borderRadius" to "10rpx"), ".shop-info .shop .info .star-info .star " to utsMapOf("width" to "28rpx", "height" to "28rpx", "marginRight" to "4rpx"), ".hot-goods .goods-list .list .pic " to utsMapOf("width" to "200rpx", "height" to "200rpx"), ".goods-info .goods-list .list .pic " to utsMapOf("width" to "335rpx", "height" to "335rpx", "borderRadius" to "10rpx")), "name" to utsMapOf(".shop-info .shop .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginBottom" to "10rpx"), ".hot-goods .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx")), "text" to utsMapOf(".shop-info .shop .info .name " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".shop-info .shop .info .star-info .star " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".shop-info .shop .info .star-info .tags .tag " to utsMapOf("fontSize" to "20rpx", "color" to "#ffffff"), ".shop-info .collect .btn " to utsMapOf("fontSize" to "24rpx", "color" to "#ffffff"), ".hot-goods .hot-title " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#333333"), ".hot-goods .goods-list .list .name " to utsMapOf("width" to "100%", "height" to "60rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "26rpx", "color" to "#333333"), ".goods-info .goods-list .list .info .title " to utsMapOf("width" to "100%", "height" to "70rpx", "fontSize" to "28rpx", "color" to "#333333", "lines" to 2, "textOverflow" to "ellipsis"), ".goods-info .goods-list .list .info .sales-volume " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".goods-info .goods-list .list .info .price-info .old-price " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa", "textDecorationLine" to "line-through")), "more" to utsMapOf(".shop-info .shop .info .name " to utsMapOf("width" to "34rpx", "height" to "34rpx")), "star-info" to utsMapOf(".shop-info .shop .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%")), "star" to utsMapOf(".shop-info .shop .info .star-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "tags" to utsMapOf(".shop-info .shop .info .star-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginLeft" to "20rpx")), "tag" to utsMapOf(".shop-info .shop .info .star-info .tags " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "4rpx", "paddingRight" to "10rpx", "paddingBottom" to "4rpx", "paddingLeft" to "10rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "4rpx")), "collect" to utsMapOf(".shop-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginLeft" to "20rpx")), "btn" to utsMapOf(".shop-info .collect " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "120rpx", "height" to "50rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "40rpx")), "hot-goods" to padStyleMapOf(utsMapOf("height" to "420rpx", "marginTop" to "30rpx", "marginRight" to "30rpx", "marginBottom" to "30rpx", "marginLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "hot-title" to utsMapOf(".hot-goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "80rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "goods-list" to utsMapOf(".hot-goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx"), ".goods-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "justifyContent" to "space-between", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "list" to utsMapOf(".hot-goods .goods-list " to utsMapOf("width" to "200rpx", "borderRadius" to "10rpx", "marginRight" to "20rpx"), ".goods-info .goods-list " to utsMapOf("width" to "335rpx", "marginBottom" to "20rpx", "paddingBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "pic" to utsMapOf(".hot-goods .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center"), ".goods-info .goods-list .list " to utsMapOf("display" to "flex")), "price" to utsMapOf(".hot-goods .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "marginTop" to "10rpx"), ".goods-info .goods-list .list .info .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "min" to utsMapOf(".hot-goods .goods-list .list .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".goods-info .goods-list .list .info .price-info .price " to utsMapOf("fontSize" to "22rpx", "color" to "#fe9a5c", "marginBottom" to "4rpx")), "max" to utsMapOf(".hot-goods .goods-list .list .price " to utsMapOf("fontWeight" to "bold", "fontSize" to "32rpx", "color" to "#fe9a5c"), ".goods-info .goods-list .list .info .price-info .price " to utsMapOf("fontSize" to "34rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "goods-info" to padStyleMapOf(utsMapOf("width" to "100%", "marginBottom" to "20rpx")), "info" to utsMapOf(".goods-info .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "column", "width" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "title" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx")), "sales-volume" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex")), "price-info" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx")), "old-price" to utsMapOf(".goods-info .goods-list .list .info .price-info " to utsMapOf("display" to "flex", "alignItems" to "center", "marginLeft" to "10rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf("ShopTabNar" to GenPagesShopDetailsComponentsShopTabNarShopTabNarClass);
    }
}
