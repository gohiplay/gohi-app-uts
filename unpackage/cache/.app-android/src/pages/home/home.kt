@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesHomeHome : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.getEpisodeList();
            this.getEpisodeList2();
            this.getBannerList();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_Skeleton = resolveEasyComponent("Skeleton", GenComponentsSkeletonSkeletonClass);
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "scroll-y" to true), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-search", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "search-info", "onClick" to _ctx.onSearch), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "//static/images/icon/fdj_icon.png", "class" to "image")),
                    createElementVNode("text", utsMapOf("class" to "text"), "请输入搜索内容")
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "scan-icon"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/sm_icon.png", "class" to "image"))
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "banner-info", "style" to normalizeStyle(utsMapOf("marginTop" to ((_ctx.statusBarHeight + 60) + "px")))), utsArrayOf(
                createVNode(_component_Skeleton, utsMapOf("loading" to _ctx.bannerFlag, "imgTitle" to true), null, 8, utsArrayOf(
                    "loading"
                )),
                createElementVNode("swiper", utsMapOf("class" to "swiper", "autoplay" to true, "circular" to true, "indicator-dots" to false, "onChange" to _ctx.changeBanner), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.bannerList, fun(item, index, _, _): VNode {
                        return createElementVNode("swiper-item", utsMapOf("key" to index), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to item.coverUrl, "mode" to "aspectFill", "class" to "image"), null, 8, utsArrayOf(
                                "src"
                            ))
                        ));
                    }
                    ), 128)
                ), 40, utsArrayOf(
                    "onChange"
                )),
                createElementVNode("view", utsMapOf("class" to "indicator-dot"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.bannerList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                            "dots",
                            utsMapOf("active" to (_ctx.bannerCurrent === index))
                        )), "key" to index), utsArrayOf(
                            createElementVNode("text", utsMapOf("style" to normalizeStyle(utsMapOf("display" to "none"))), toDisplayString(item), 5)
                        ), 2);
                    }
                    ), 128)
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "day-recommend"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "recommend-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "line")),
                            createElementVNode("text", utsMapOf("class" to "tit"), "每日推荐")
                        )),
                        createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                        ))
                    )),
                    createVNode(_component_Skeleton, utsMapOf("showWaterfall" to true, "loading" to _ctx.episodeList2Flag, "imgHeight" to "220rpx", "borderRadius" to "5rpx", "column" to 3), null, 8, utsArrayOf(
                        "loading"
                    )),
                    createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.episodeList2, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                                _ctx.onGoodsItem(item);
                            }
                            , "key" to index), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.coverUrl, "class" to "pic"), null, 8, utsArrayOf(
                                    "src"
                                )),
                                createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.title), 1)
                                ))
                            ), 8, utsArrayOf(
                                "onClick"
                            ));
                        }
                        ), 128)
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "guess-like"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "line")),
                        createElementVNode("text", utsMapOf("class" to "tit"), "猜你喜欢")
                    ))
                )),
                createVNode(_component_Skeleton, utsMapOf("showWaterfall" to true, "loading" to _ctx.episodeListFlag, "imgHeight" to "320rpx", "borderRadius" to "10rpx", "column" to 2, "row" to 3), null, 8, utsArrayOf(
                    "loading"
                )),
                createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.episodeList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                            _ctx.onGoodsItem(item);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.coverUrl, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.title), 1)
                            ))
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ))
            ))
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var bannerCurrent: Number by `$data`;
    open var bannerFlag: Boolean by `$data`;
    open var episodeList2Flag: Boolean by `$data`;
    open var episodeListFlag: Boolean by `$data`;
    open var episodeList: UTSArray<goodsItems> by `$data`;
    open var episodeList2: UTSArray<goodsItems> by `$data`;
    open var bannerList: UTSArray<bannerItem> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "bannerCurrent" to 0, "bannerFlag" to true as Boolean, "episodeList2Flag" to true as Boolean, "episodeListFlag" to true as Boolean, "episodeList" to utsArrayOf<goodsItems>(), "episodeList2" to utsArrayOf<goodsItems>(), "bannerList" to utsArrayOf<bannerItem>());
    }
    override fun `$initMethods`() {
        this.getEpisodeList2 = fun(): Unit {
            listEpisode(object : UTSJSONObject() {
                var ctype: Number = 2
                var pageNum: Number = 1
                var pageSize: Number = 6
            }).then(fun(res: UTSJSONObject){
                this.episodeList2 = res!!.getArray<goodsItems>("rows") as UTSArray<goodsItems>;
                this.episodeList2Flag = false;
            }
            );
        }
        ;
        this.getEpisodeList = fun(): Unit {
            listEpisode(object : UTSJSONObject() {
                var ctype: Number = 2
                var pageNum: Number = 1
                var pageSize: Number = 6
            }).then(fun(res: UTSJSONObject){
                this.episodeList = res!!.getArray<goodsItems>("rows") as UTSArray<goodsItems>;
                this.episodeListFlag = false;
            }
            );
        }
        ;
        this.getBannerList = fun(): Unit {
            listBanner(UTSJSONObject()).then(fun(res: UTSJSONObject){
                console.log(JSON.stringify(res), " at pages/home/home.uvue:188");
                this.bannerList = res!!.getArray<bannerItem>("rows") as UTSArray<bannerItem>;
                this.bannerFlag = false;
            }
            );
        }
        ;
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
        this.changeBanner = fun(e: SwiperChangeEvent) {
            var index = e.detail.current;
            this.bannerCurrent = index;
        }
        ;
        this.onSearch = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/search/search"));
        }
        ;
        this.onGoodsItem = fun(item: goodsItems) {
            uni_navigateTo(NavigateToOptions(url = "/pages/detail/detail?episodeId=" + item.id));
        }
        ;
        this.onTile = fun(url: String) {
            uni_navigateTo(NavigateToOptions(url = "/pages/" + url + "/" + url));
        }
        ;
        this.onMenu = fun(item: memuItem) {
            uni_navigateTo(NavigateToOptions(url = "/pages/ClassifyGoods/ClassifyGoods?title=" + item.title));
        }
        ;
    }
    open lateinit var getEpisodeList2: () -> Unit;
    open lateinit var getEpisodeList: () -> Unit;
    open lateinit var getBannerList: () -> Unit;
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    open lateinit var changeBanner: (e: SwiperChangeEvent) -> Unit;
    open lateinit var onSearch: () -> Unit;
    open lateinit var onGoodsItem: (item: goodsItems) -> Unit;
    open lateinit var onTile: (url: String) -> Unit;
    open lateinit var onMenu: (item: memuItem) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8", "paddingBottom" to "30rpx")), "head-search" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff", "paddingLeft" to "30rpx", "paddingRight" to "30rpx")), "search-info" to utsMapOf(".head-search " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1, "height" to "70rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "backgroundColor" to "#f8f8f8", "borderRadius" to "35rpx")), "text" to utsMapOf(".head-search .search-info " to utsMapOf("color" to "#aaaaaa", "fontSize" to "24rpx"), ".menu-info .menu-list .list " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".day-recommend .recommend-info .goods-list .list .title " to utsMapOf("width" to "100%", "height" to "70rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#333333"), ".guess-like .goods-list .list .title " to utsMapOf("lines" to 2, "textOverflow" to "ellipsis", "width" to "334rpx", "height" to "70rpx", "fontSize" to "28rpx", "color" to "#333333")), "image" to utsMapOf(".head-search .search-info " to utsMapOf("width" to "34rpx", "height" to "34rpx", "marginRight" to "20rpx"), ".head-search .scan-icon " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".banner-info .swiper " to utsMapOf("width" to "100%", "height" to "340rpx", "borderRadius" to "20rpx"), ".menu-info .menu-list .list " to utsMapOf("width" to "80rpx", "height" to "80rpx", "marginBottom" to "10rpx"), ".tile-info .tile-left " to utsMapOf("width" to "100%", "height" to "300rpx", "borderRadius" to "20rpx"), ".tile-info .tile-right .tile " to utsMapOf("width" to "100%", "height" to "140rpx", "borderRadius" to "20rpx"), ".title-info .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".guess-like .goods-list .list .pic " to utsMapOf("width" to "334rpx", "height" to "334rpx", "borderRadius" to "10rpx")), "scan-icon" to utsMapOf(".head-search " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginLeft" to "20rpx")), "banner-info" to padStyleMapOf(utsMapOf("position" to "relative", "width" to "100%", "height" to "340rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "20rpx")), "swiper" to utsMapOf(".banner-info " to utsMapOf("width" to "100%", "height" to "340rpx")), "indicator-dot" to utsMapOf(".banner-info " to utsMapOf("position" to "absolute", "left" to 0, "bottom" to "20rpx", "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "100%")), "dots" to utsMapOf(".banner-info .indicator-dot " to utsMapOf("width" to "16rpx", "height" to "16rpx", "backgroundColor" to "rgba(255,255,255,0.3)", "borderRadius" to "10rpx", "marginTop" to 0, "marginRight" to "6rpx", "marginBottom" to 0, "marginLeft" to "6rpx")), "active" to utsMapOf(".banner-info .indicator-dot " to utsMapOf("width" to "34rpx", "height" to "16rpx", "backgroundColor" to "#ffffff", "borderRadius" to "16rpx")), "menu-info" to padStyleMapOf(utsMapOf("width" to "100%", "height" to "160rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "20rpx")), "menu-list" to utsMapOf(".menu-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "160rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "list" to utsMapOf(".menu-info .menu-list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "25%", "height" to "140rpx"), ".day-recommend .recommend-info .goods-list " to utsMapOf("width" to "200rpx", "height" to "100%", "marginRight" to "24rpx"), ".guess-like .goods-list " to utsMapOf("width" to "334rpx", "marginBottom" to "20rpx")), "tile-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "300rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "20rpx")), "tile-left" to utsMapOf(".tile-info " to utsMapOf("width" to "49%", "height" to "300rpx")), "tile-right" to utsMapOf(".tile-info " to utsMapOf("justifyContent" to "space-between", "width" to "49%", "height" to "300rpx")), "tile" to utsMapOf(".tile-info .tile-right " to utsMapOf("width" to "100%", "height" to "140rpx")), "title-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "title" to utsMapOf(".title-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".day-recommend .recommend-info .goods-list .list " to utsMapOf("display" to "flex"), ".guess-like .goods-list .list " to utsMapOf("display" to "flex", "marginTop" to "10rpx")), "line" to utsMapOf(".title-info .title " to utsMapOf("width" to "10rpx", "height" to "40rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "10rpx", "marginRight" to "10rpx")), "tit" to utsMapOf(".title-info .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333")), "more" to utsMapOf(".title-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "day-recommend" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "20rpx")), "recommend-info" to utsMapOf(".day-recommend " to utsMapOf("width" to "100%", "height" to "420rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "goods-list" to utsMapOf(".day-recommend .recommend-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "320rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx"), ".guess-like " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "justifyContent" to "space-between", "width" to "100%")), "pic" to utsMapOf(".day-recommend .recommend-info .goods-list .list " to utsMapOf("width" to "200rpx", "height" to "200rpx", "borderRadius" to "10rpx"), ".guess-like .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "334rpx")), "price" to utsMapOf(".day-recommend .recommend-info .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end"), ".guess-like .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "min" to utsMapOf(".day-recommend .recommend-info .goods-list .list .price " to utsMapOf("fontSize" to "20rpx", "fontWeight" to "bold", "color" to "#fe9a5c", "marginBottom" to "4rpx"), ".guess-like .goods-list .list .price " to utsMapOf("fontSize" to "22rpx", "fontWeight" to "bold", "color" to "#fe9a5c", "marginBottom" to "4rpx")), "max" to utsMapOf(".day-recommend .recommend-info .goods-list .list .price " to utsMapOf("fontSize" to "30rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".guess-like .goods-list .list .price " to utsMapOf("fontSize" to "36rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "guess-like" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "30rpx", "backgroundColor" to "#ffffff")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
