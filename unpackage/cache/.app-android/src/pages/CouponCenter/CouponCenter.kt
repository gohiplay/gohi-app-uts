@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesCouponCenterCouponCenter : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            this.getData();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "tab-info"), utsArrayOf(
                createElementVNode("scroll-view", utsMapOf("class" to "tab-item", "scroll-x" to true), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.classifyList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onClassify(item.id);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("active" to (_ctx.classifyId === item.id))
                            ))), toDisplayString(item.name), 3),
                            createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "line",
                                utsMapOf("active-line" to (_ctx.classifyId === item.id))
                            ))), null, 2)
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "coupon-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "coupon-list"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.couponList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "goods-item"), utsArrayOf(
                                createElementVNode(Fragment, null, RenderHelpers.renderList(item.goodsList, fun(value, idx, _, _): VNode {
                                    return createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                                        _ctx.onGoodsItem(value);
                                    }
                                    , "key" to idx), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                            createElementVNode("image", utsMapOf("src" to value.pic, "class" to "image"), null, 8, utsArrayOf(
                                                "src"
                                            ))
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(value.price), 1)
                                        ))
                                    ), 8, utsArrayOf(
                                        "onClick"
                                    ));
                                }
                                ), 128)
                            )),
                            createElementVNode("view", utsMapOf("class" to "coupon-data"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "full-price"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "满" + toDisplayString(item.fullPrice), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "min"), "减"),
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.subPrice), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "立即领取")
                                ))
                            ))
                        ));
                    }
                    ), 128)
                ))
            ))
        ), 4);
    }
    open var classifyList: UTSArray<classifyItem> by `$data`;
    open var couponList: UTSArray<CouponItem> by `$data`;
    open var classifyId: Number by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("classifyList" to utsArrayOf<classifyItem>(), "couponList" to CouponData as UTSArray<CouponItem>, "classifyId" to 0);
    }
    override fun `$initMethods`() {
        this.getData = fun() {
            if (ClassifyData[0].id === 0) {
                ClassifyData.splice(0, 1);
            }
            this.classifyList = ClassifyData;
            this.classifyList.unshift(classifyItem(id = 0, parent = 0, name = "精选推荐", pic = "", children = utsArrayOf<classifyItem>()));
        }
        ;
        this.onClassify = fun(id: Number) {
            this.classifyId = id;
        }
        ;
        this.onGoodsItem = fun(item: goodsItems1) {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsDetail/GoodsDetail?goodsId=" + item.id));
        }
        ;
    }
    open lateinit var getData: () -> Unit;
    open lateinit var onClassify: (id: Number) -> Unit;
    open lateinit var onGoodsItem: (item: goodsItems1) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "tab-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff")), "tab-item" to utsMapOf(".tab-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "100%")), "item" to utsMapOf(".tab-info .tab-item " to utsMapOf("position" to "relative", "display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "160rpx", "height" to "100%"), ".coupon-info .coupon-list .list .goods-item " to utsMapOf("width" to "140rpx", "marginRight" to "10rpx")), "text" to utsMapOf(".tab-info .tab-item .item " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".coupon-info .coupon-list .list .goods-item .item .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#333333"), ".coupon-info .coupon-list .list .coupon-data .full-price " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa"), ".coupon-info .coupon-list .list .coupon-data .price " to utsMapOf("fontSize" to "42rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".coupon-info .coupon-list .list .coupon-data .btn " to utsMapOf("fontSize" to "24rpx", "color" to "#ffffff")), "line" to utsMapOf(".tab-info .tab-item .item " to utsMapOf("position" to "absolute", "bottom" to "16rpx", "width" to "30%", "height" to "8rpx", "backgroundColor" to "rgba(0,0,0,0)", "borderRadius" to "8rpx")), "active" to utsMapOf(".tab-info .tab-item .item " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "active-line" to utsMapOf(".tab-info .tab-item .item " to utsMapOf("backgroundColor" to "#fe9a5c")), "coupon-info" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "120rpx")), "coupon-list" to utsMapOf(".coupon-info " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "list" to utsMapOf(".coupon-info .coupon-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "230rpx", "marginBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "goods-item" to utsMapOf(".coupon-info .coupon-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1, "height" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "pic" to utsMapOf(".coupon-info .coupon-list .list .goods-item .item " to utsMapOf("display" to "flex", "alignItems" to "center")), "image" to utsMapOf(".coupon-info .coupon-list .list .goods-item .item .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx", "borderRadius" to "10rpx")), "price" to utsMapOf(".coupon-info .coupon-list .list .goods-item .item " to utsMapOf("display" to "flex", "alignItems" to "center", "marginTop" to "10rpx"), ".coupon-info .coupon-list .list .coupon-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "coupon-data" to utsMapOf(".coupon-info .coupon-list .list " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "200rpx", "height" to "180rpx", "borderLeftWidth" to 1, "borderLeftStyle" to "dashed", "borderLeftColor" to "#f8f8f8")), "full-price" to utsMapOf(".coupon-info .coupon-list .list .coupon-data " to utsMapOf("display" to "flex", "alignItems" to "center")), "min" to utsMapOf(".coupon-info .coupon-list .list .coupon-data .price " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#fe9a5c", "marginBottom" to "6rpx")), "btn" to utsMapOf(".coupon-info .coupon-list .list .coupon-data " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "140rpx", "height" to "40rpx", "marginTop" to "20rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "40rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
