@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesClassifyClassify : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            this.getData();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_page = resolveEasyComponent("page", GenComponentsPagePageClass);
        return createVNode(_component_page, null, utsMapOf("default" to withSlotCtx(fun(): UTSArray<Any> {
            return utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "classify-main"), utsArrayOf(
                    createElementVNode("scroll-view", utsMapOf("scroll-y" to true, "class" to "classify-left", "scroll-with-animation" to "true"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.oneClassifyList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "item",
                                utsMapOf("active" to (_ctx.oneClassifyId === item.id))
                            )), "onClick" to fun(){
                                _ctx.onOneClassify(item.id, index);
                            }
                            , "key" to index), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                    "line",
                                    utsMapOf("line-active" to (_ctx.oneClassifyId === item.id))
                                ))), null, 2),
                                createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                    "text",
                                    utsMapOf("text-active" to (_ctx.oneClassifyId === item.id))
                                ))), toDisplayString(item.name), 3)
                            ), 10, utsArrayOf(
                                "onClick"
                            ));
                        }
                        ), 128)
                    )),
                    createElementVNode("scroll-view", utsMapOf("scroll-y" to true, "class" to "classify-right", "scroll-with-animation" to "true"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.classifyList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "classify-list", "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "two-classify"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "three-list"), utsArrayOf(
                                    createElementVNode(Fragment, null, RenderHelpers.renderList(item.children, fun(value, idx, _, _): VNode {
                                        return createElementVNode("view", utsMapOf("class" to "list", "onClick" to _ctx.onThreeClassify, "key" to idx), utsArrayOf(
                                            createElementVNode("image", utsMapOf("src" to value.pic, "class" to "image"), null, 8, utsArrayOf(
                                                "src"
                                            )),
                                            createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(value.name), 1)
                                        ), 8, utsArrayOf(
                                            "onClick"
                                        ));
                                    }
                                    ), 128)
                                ))
                            ));
                        }
                        ), 128)
                    ))
                ))
            );
        }
        ), "_" to 1));
    }
    open var oneClassifyList: UTSArray<classifyItem> by `$data`;
    open var classifyList: UTSArray<classifyItem> by `$data`;
    open var oneClassifyId: Number by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("oneClassifyList" to utsArrayOf<classifyItem>(), "classifyList" to utsArrayOf<classifyItem>(), "oneClassifyId" to 0);
    }
    override fun `$initMethods`() {
        this.getData = fun() {
            this.oneClassifyId = ClassifyData[0].id;
            if (this.oneClassifyId === 0) {
                ClassifyData.splice(0, 1);
            }
            run {
                var i: Number = 0;
                while(i < ClassifyData.length){
                    this.oneClassifyList.push(ClassifyData[i]);
                    run {
                        var j: Number = 0;
                        while(j < ClassifyData[i].children.length){
                            if (this.oneClassifyId === ClassifyData[i].children[j].parent) {
                                this.classifyList.push(ClassifyData[i].children[j]);
                            }
                            j++;
                        }
                    }
                    i++;
                }
            }
        }
        ;
        this.onOneClassify = fun(id: Number, index: Number) {
            this.oneClassifyId = id;
            this.classifyList = this.oneClassifyList[index].children;
        }
        ;
        this.onThreeClassify = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/SearchGoods/SearchGoods"));
        }
        ;
    }
    open lateinit var getData: () -> Unit;
    open lateinit var onOneClassify: (id: Number, index: Number) -> Unit;
    open lateinit var onThreeClassify: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#ffffff")), "classify-main" to padStyleMapOf(utsMapOf("position" to "fixed", "display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "100%", "backgroundColor" to "#ffffff")), "classify-left" to utsMapOf(".classify-main " to utsMapOf("width" to "25%", "height" to "100%", "backgroundColor" to "#f8f8f8")), "item" to utsMapOf(".classify-main .classify-left " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "120rpx")), "line" to utsMapOf(".classify-main .classify-left .item " to utsMapOf("display" to "none")), "text" to utsMapOf(".classify-main .classify-left .item " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".classify-main .classify-right .classify-list .two-classify " to utsMapOf("fontSize" to "32rpx", "color" to "#333333"), ".classify-main .classify-right .classify-list .three-list .list " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa")), "line-active" to utsMapOf(".classify-main .classify-left .item " to utsMapOf("position" to "absolute", "left" to 0, "display" to "flex", "width" to "8rpx", "height" to "40rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "8rpx")), "text-active" to utsMapOf(".classify-main .classify-left .item " to utsMapOf("color" to "#fe9a5c")), "active" to utsMapOf(".classify-main " to utsMapOf("position" to "relative", "backgroundColor" to "#ffffff")), "classify-right" to utsMapOf(".classify-main " to utsMapOf("width" to "85%", "height" to "100%", "backgroundColor" to "#ffffff")), "classify-list" to utsMapOf(".classify-main .classify-right " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "two-classify" to utsMapOf(".classify-main .classify-right .classify-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx")), "three-list" to utsMapOf(".classify-main .classify-right .classify-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap")), "list" to utsMapOf(".classify-main .classify-right .classify-list .three-list " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "30%", "height" to "150rpx", "marginBottom" to "20rpx")), "image" to utsMapOf(".classify-main .classify-right .classify-list .three-list .list " to utsMapOf("width" to "100rpx", "height" to "100rpx", "marginBottom" to "10rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
