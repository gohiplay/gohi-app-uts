@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
open class GenPagesGoodsCollectGoodsCollect : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("style" to normalizeStyle(utsMapOf("paddingBottom" to ((_ctx.statusBarBottom + 50) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("height" to ((50 + _ctx.statusBarHeight) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "商品收藏")
                    )),
                    createElementVNode("view", utsMapOf("class" to "edit", "onClick" to fun(){
                        _ctx.isEdit = !_ctx.isEdit;
                    }
                    ), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(if (_ctx.isEdit) {
                            "完成";
                        } else {
                            "编辑";
                        }
                        ), 1)
                    ), 8, utsArrayOf(
                        "onClick"
                    ))
                ), 4),
                createElementVNode("view", utsMapOf("class" to "goods-info", "style" to normalizeStyle(utsMapOf("marginTop" to ((60 + _ctx.statusBarHeight) + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                                if (isTrue(_ctx.isEdit)) {
                                    createElementVNode("view", utsMapOf("key" to 0, "class" to "check"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/check_off.png", "class" to "image"))
                                    ));
                                } else {
                                    createCommentVNode("v-if", true);
                                }
                                ,
                                createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                        "src"
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "goods"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(item.price), 1)
                                    ))
                                ))
                            ));
                        }
                        ), 128)
                    ))
                ), 4),
                if (isTrue(_ctx.isEdit)) {
                    createElementVNode("view", utsMapOf("key" to 0, "class" to "footer-check", "style" to normalizeStyle(utsMapOf("paddingBottom" to (_ctx.statusBarBottom + "px")))), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "check"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/check_off.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "全选")
                        )),
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "取消收藏(1)")
                        ))
                    ), 4);
                } else {
                    createCommentVNode("v-if", true);
                }
            ), 4)
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var statusBarBottom: Number by `$data`;
    open var isEdit: Boolean by `$data`;
    open var goodsList: UTSArray<goodsItems1> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "statusBarBottom" to 0, "isEdit" to false, "goodsList" to goodsData as UTSArray<goodsItems1>);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "head-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingLeft" to "30rpx", "paddingRight" to "30rpx", "backgroundColor" to "#ffffff")), "back" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "image" to utsMapOf(".head-info .back " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".goods-info .goods-list .list .check " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".goods-info .goods-list .list .pic " to utsMapOf("width" to "180rpx", "height" to "180rpx", "borderRadius" to "10rpx"), ".footer-check .check " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "20rpx")), "title" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center")), "text" to utsMapOf(".head-info .title " to utsMapOf("fontSize" to "32rpx", "color" to "#333333"), ".head-info .edit " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".goods-info .goods-list .list .goods .name " to utsMapOf("width" to "100%", "height" to "80rpx", "lineHeight" to "40rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#333333"), ".goods-info .goods-list .list .goods .price " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".footer-check .check " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".footer-check .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "edit" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center")), "goods-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "20rpx")), "goods-list" to utsMapOf(".goods-info " to utsMapOf("width" to "100%")), "list" to utsMapOf(".goods-info .goods-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "200rpx", "paddingTop" to "20rpx", "paddingRight" to "20rpx", "paddingBottom" to "20rpx", "paddingLeft" to "20rpx", "marginBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "check" to utsMapOf(".goods-info .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx"), ".footer-check " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center")), "pic" to utsMapOf(".goods-info .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "20rpx")), "goods" to utsMapOf(".goods-info .goods-list .list " to utsMapOf("flex" to 1, "height" to "100%")), "name" to utsMapOf(".goods-info .goods-list .list .goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginBottom" to "20rpx", "width" to "100%")), "price" to utsMapOf(".goods-info .goods-list .list .goods " to utsMapOf("display" to "flex")), "footer-check" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "bottom" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "minHeight" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "btn" to utsMapOf(".footer-check " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "240rpx", "height" to "70rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "40rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
