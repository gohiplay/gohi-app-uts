@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getStorageSync as uni_getStorageSync;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesMyMy : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
        }
        , instance);
        onPageShow(fun() {
            this.isLogin = uni_getStorageSync("isLogin") as Boolean;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1"))), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 140) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "operation-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "operation-btn"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/message_icon.png", "class" to "btn", "onClick" to fun(){
                            _ctx.onTool("message");
                        }
                        ), null, 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/setting_icon.png", "class" to "btn", "onClick" to fun(){
                            _ctx.onTool("setting");
                        }
                        ), null, 8, utsArrayOf(
                            "onClick"
                        ))
                    ))
                )),
                if (isTrue(_ctx.isLogin)) {
                    createElementVNode("view", utsMapOf("key" to 0, "class" to "user-info", "onClick" to fun(){
                        _ctx.onTool("PersonalInfo");
                    }), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "profile"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "https://picsum.photos/200/200?id=404", "class" to "image"))
                        )),
                        createElementVNode("view", utsMapOf("class" to "user-data"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "nickname"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "哇哈哈")
                            )),
                            createElementVNode("view", utsMapOf("class" to "phone"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "178****8888")
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                        ))
                    ), 8, utsArrayOf(
                        "onClick"
                    ));
                } else {
                    createElementVNode("view", utsMapOf("key" to 1, "class" to "user-info", "onClick" to fun(){
                        _ctx.onTool("login");
                    }
                    ), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "profile"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "https://picsum.photos/200/200?id=404", "class" to "image"))
                        )),
                        createElementVNode("view", utsMapOf("class" to "user-data"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "nickname"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "登录/注册")
                            )),
                            createElementVNode("view", utsMapOf("class" to "phone"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"))
                            ))
                        ))
                    ), 8, utsArrayOf(
                        "onClick"
                    ));
                }
            ), 4),
            createElementVNode("view", utsMapOf("class" to "vip-info", "onClick" to fun(){
                _ctx.onTool("MemberCenter");
            }
            ), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/hybj_bg.png", "mode" to "aspectFit", "class" to "bg")),
                    createElementVNode("view", utsMapOf("class" to "vip-data"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title-desc"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/zshy_icon.png", "class" to "image")),
                                createElementVNode("text", utsMapOf("class" to "text"), "会员尊享99+项权益")
                            )),
                            createElementVNode("view", utsMapOf("class" to "desc"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "新用户专享，限时惊喜红包")
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "去开通")
                        ))
                    ))
                ))
            ), 8, utsArrayOf(
                "onClick"
            )),
            createElementVNode("view", utsMapOf("class" to "order-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "order-data"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "line")),
                            createElementVNode("text", utsMapOf("class" to "tit"), "我的订单")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "order-item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to _ctx.onOrder), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/order_dfk.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "待付款")
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to _ctx.onOrder), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/order_dfh.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "待发货")
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to _ctx.onOrder), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/order_dsh.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "待收货")
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to _ctx.onOrder), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/order_dpj.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "待评价")
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onTool("AfterSale");
                        }
                        ), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/order_tksh.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "退款/售后")
                        ), 8, utsArrayOf(
                            "onClick"
                        ))
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "tool-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "tool-data"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "line")),
                            createElementVNode("text", utsMapOf("class" to "tit"), "工具与服务")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "tool-item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onTool("GoodsCollect");
                        }
                        ), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/tool_sc.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "收藏")
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onTool("ShopCollect");
                        }
                        ), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/tool_dp.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "店铺")
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onTool("AddressList");
                        }
                        ), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/tool_dz.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "地址")
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onTool("MyCoupon");
                        }
                        ), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/tool_yhq.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "优惠券")
                        ), 8, utsArrayOf(
                            "onClick"
                        ))
                    ))
                ))
            ))
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var isLogin: Boolean by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "isLogin" to false);
    }
    override fun `$initMethods`() {
        this.onTool = fun(url: String) {
            uni_navigateTo(NavigateToOptions(url = "/pages/" + url + "/" + url));
        }
        ;
        this.onOrder = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/OrderList/OrderList"));
        }
        ;
    }
    open lateinit var onTool: (url: String) -> Unit;
    open lateinit var onOrder: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("head-info" to padStyleMapOf(utsMapOf("width" to "100%", "height" to "400rpx", "backgroundImage" to "linear-gradient(to top, #f8f8f8, #fef3e2)")), "operation-info" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "flex-end", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "boxSizing" to "border-box")), "operation-btn" to utsMapOf(".head-info .operation-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "btn" to utsMapOf(".head-info .operation-info .operation-btn " to utsMapOf("width" to "48rpx", "height" to "48rpx", "marginLeft" to "30rpx"), ".vip-info .info .vip-data " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "140rpx", "height" to "70rpx", "backgroundColor" to "#ffffff", "borderRadius" to "35rpx")), "user-info" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "140rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "profile" to utsMapOf(".head-info .user-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "image" to utsMapOf(".head-info .user-info .profile " to utsMapOf("width" to "140rpx", "height" to "140rpx", "borderRadius" to "70rpx"), ".head-info .user-info .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".vip-info .info .vip-data .title-desc .title " to utsMapOf("width" to "48rpx", "height" to "48rpx", "marginRight" to "10rpx"), ".order-info .order-data .order-item .item " to utsMapOf("width" to "48rpx", "height" to "48rpx", "marginBottom" to "10rpx"), ".tool-info .tool-data .tool-item .item " to utsMapOf("width" to "58rpx", "height" to "58rpx", "marginBottom" to "10rpx")), "user-data" to utsMapOf(".head-info .user-info " to utsMapOf("display" to "flex", "flexDirection" to "column", "justifyContent" to "center", "flex" to 1, "height" to "140rpx")), "nickname" to utsMapOf(".head-info .user-info .user-data " to utsMapOf("marginBottom" to "10rpx")), "text" to utsMapOf(".head-info .user-info .user-data .nickname " to utsMapOf("fontSize" to "30rpx", "fontWeight" to "bold", "color" to "#333333"), ".head-info .user-info .user-data .phone " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".vip-info .info .vip-data .title-desc .title " to utsMapOf("fontSize" to "28rpx", "color" to "#a95a37"), ".vip-info .info .vip-data .title-desc .desc " to utsMapOf("fontSize" to "26rpx", "color" to "#bc8760"), ".vip-info .info .vip-data .btn " to utsMapOf("fontSize" to "26rpx", "fontWeight" to "bold", "color" to "#a95a37"), ".order-info .order-data .order-item .item " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".tool-info .tool-data .tool-item .item " to utsMapOf("fontSize" to "26rpx", "color" to "#333333")), "phone" to utsMapOf(".head-info .user-info .user-data " to utsMapOf("marginBottom" to "10rpx")), "more" to utsMapOf(".head-info .user-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "48rpx", "height" to "140rpx")), "vip-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "40rpx")), "info" to utsMapOf(".vip-info " to utsMapOf("position" to "relative", "width" to "100%", "height" to "154rpx")), "bg" to utsMapOf(".vip-info .info " to utsMapOf("width" to "100%", "height" to "154rpx")), "vip-data" to utsMapOf(".vip-info .info " to utsMapOf("position" to "absolute", "left" to 0, "top" to 0, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100%", "paddingTop" to 0, "paddingRight" to "50rpx", "paddingBottom" to 0, "paddingLeft" to "50rpx")), "title-desc" to utsMapOf(".vip-info .info .vip-data " to utsMapOf("display" to "flex", "flexDirection" to "column")), "title" to utsMapOf(".vip-info .info .vip-data .title-desc " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".title-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "desc" to utsMapOf(".vip-info .info .vip-data .title-desc " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "6rpx")), "title-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "marginTop" to "30rpx")), "line" to utsMapOf(".title-info .title " to utsMapOf("width" to "12rpx", "height" to "34rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "12rpx", "marginRight" to "10rpx")), "tit" to utsMapOf(".title-info .title " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#333333")), "order-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "20rpx", "marginRight" to "auto", "marginBottom" to "20rpx", "marginLeft" to "auto")), "order-data" to utsMapOf(".order-info " to utsMapOf("width" to "100%", "height" to "220rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "order-item" to utsMapOf(".order-info .order-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "140rpx")), "item" to utsMapOf(".order-info .order-data .order-item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "20%", "height" to "100%"), ".tool-info .tool-data .tool-item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "25%", "height" to "120rpx")), "tool-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "tool-data" to utsMapOf(".tool-info " to utsMapOf("width" to "100%", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx", "paddingBottom" to "20rpx")), "tool-item" to utsMapOf(".tool-info .tool-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "width" to "100%", "marginTop" to "10rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
