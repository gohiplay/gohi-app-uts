@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.createVideoContext as uni_createVideoContext;
import io.dcloud.uniapp.extapi.getSystemInfoSync as uni_getSystemInfoSync;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
open class GenPagesDetailDetail : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onCreated(fun() {}, instance);
        onLoad(fun(options: OnLoadOptions) {
            this.episodeId = options!!.get("episodeId") as String;
            getEpisode(this.episodeId).then(fun(res: UTSJSONObject){
                console.log(res!!.get("data") as episodeItem, " at pages/detail/detail.uvue:57");
                var episodeInfo = res!!.get("data") as UTSJSONObject;
                this.list = episodeInfo!!.getArray<episodeDetailItem>("episodeDetails") as UTSArray<episodeDetailItem>;
                this.visibleList = this.list.slice(0, 3);
                this.doPlay(0);
            }
            );
        }
        , instance);
        onReady(fun() {
            var backElement = this.`$refs`["back"] as UniElement;
            backElement.style.setProperty("top", uni_getSystemInfoSync().statusBarHeight + "px");
        }
        , instance);
        onPageShow(fun() {
            currentPageIsShow = true;
        }
        , instance);
        onHide(fun() {
            currentPageIsShow = false;
            console.log("pages-onHide", " at pages/detail/detail.uvue:121");
            this.doPause(this.current);
        }
        , instance);
        onUnload(fun() {
            this.doPause(this.current);
        }
        , instance);
        this.`$watch`(fun(): Any? {
            return this.current;
        }
        , fun(current: Number, oldCurrent: Number) {
            console.log(JSON.stringify(current) + "+++++++++" + JSON.stringify(oldCurrent), " at pages/detail/detail.uvue:70");
            var changeNumber = current - oldCurrent;
            if (changeNumber == 1 || changeNumber == -2) {
                this.index++;
            } else {
                this.index--;
            }
            if (Math.abs(changeNumber) == 2) {
                page = Math.floor(this.index / 3);
                console.log(this.index, " at pages/detail/detail.uvue:83");
                var visibleList = this.list.slice(3 * page, 3 * page + 3);
                this.visibleList = visibleList;
            }
            this.state.forEach(fun(_: String, index: Number){
                if (index === current) {
                    this.doPlay(current);
                } else {
                    this.doStop(index);
                    console.log("index:" + index + "已被执行停止", " at pages/detail/detail.uvue:102");
                }
            }
            );
        }
        );
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("view", utsMapOf("class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("ref" to "back", "onClick" to _ctx.back, "class" to "nav-back"), utsArrayOf(
                createElementVNode("image", utsMapOf("class" to "back-img", "src" to "/static/template/scroll-fold-nav/back.png", "mode" to "widthFix"))
            ), 8, utsArrayOf(
                "onClick"
            )),
            createElementVNode("swiper", utsMapOf("class" to "swiper", "current" to _ctx.current, "circular" to (_ctx.index != 0), "vertical" to true, "onChange" to _ctx.onSwiperChange, "onTransition" to _ctx.onTransition), utsArrayOf(
                createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.visibleList, fun(item, i, _, _): VNode {
                    return createElementVNode("swiper-item", utsMapOf("class" to "swiper-item", "key" to i), utsArrayOf(
                        createElementVNode("video", utsMapOf("onClick" to fun(){
                            _ctx.changeState(i);
                        }
                        , "ref_for" to true, "ref" to "video", "class" to "video-box", "objectFit" to "cover", "id" to ("video-" + i), "onLoadstart" to fun(){
                            _ctx.onLoadstart(i);
                        }
                        , "src" to item.fileUrl, "autoplay" to false, "show-center-play-btn" to false, "loop" to true, "onPlay" to fun(){
                            _ctx.onPlay(i);
                        }
                        , "onPause" to fun(){
                            _ctx.onPause(i);
                        }
                        , "http-cache" to "true"), null, 40, utsArrayOf(
                            "onClick",
                            "id",
                            "onLoadstart",
                            "src",
                            "onPlay",
                            "onPause"
                        )),
                        createElementVNode("view", utsMapOf("class" to "video-cover", "onClick" to fun(){
                            _ctx.changeState(i);
                        }
                        ), utsArrayOf(
                            if (_ctx.state[i] === "pause") {
                                createElementVNode("image", utsMapOf("key" to 0, "class" to "play-btn", "src" to "/static/template/swiper-vertical-video/play.png", "mode" to "widthFix"));
                            } else {
                                createCommentVNode("v-if", true);
                            }
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        if (isTrue(0)) {
                            createElementVNode("view", utsMapOf("key" to 0, "class" to "video-info"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "video-info-text"), "容器：第 " + toDisplayString(i) + " 个", 1),
                                createElementVNode("text", utsMapOf("class" to "video-info-text"), "内容：" + toDisplayString(item.title), 1)
                            ));
                        } else {
                            createCommentVNode("v-if", true);
                        }
                    ));
                }
                ), 128)
            ), 40, utsArrayOf(
                "current",
                "circular",
                "onChange",
                "onTransition"
            )),
            if (isTrue(0)) {
                createElementVNode("view", utsMapOf("key" to 0, "class" to "debug-info"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "status-text"), "debug-info 播放状态:"),
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.state, fun(value, index, _, _): VNode {
                        return createElementVNode("text", utsMapOf("class" to "status-text"), "第" + toDisplayString(index + 1) + "个:" + toDisplayString(value), 1);
                    }), 256)
                ));
            } else {
                createCommentVNode("v-if", true);
            }
        ));
    }
    open var `$videoContextMap`: Map<String, VideoContext> by `$data`;
    open var episodeId: String by `$data`;
    open var episodeInfo: UTSJSONObject by `$data`;
    open var list: UTSArray<episodeDetailItem> by `$data`;
    open var visibleList: UTSArray<episodeDetailItem> by `$data`;
    open var current: Number by `$data`;
    open var index: Number by `$data`;
    open var state: UTSArray<String> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("\$videoContextMap" to Map<String, VideoContext>(), "episodeId" to "" as String, "episodeInfo" to UTSJSONObject(), "list" to utsArrayOf<episodeDetailItem>(), "visibleList" to utsArrayOf<episodeDetailItem>(), "current" to 0 as Number, "index" to 0, "state" to utsArrayOf<String>("unPlay", "unPlay", "unPlay"));
    }
    override fun `$initMethods`() {
        this.changeState = fun(index: Number) {
            if (this.state[index] === "play") {
                this.doPause(index);
            } else {
                this.doPlay(this.current);
            }
        }
        ;
        this.onLoadstart = fun(index: Number) {
            console.error("onLoadstart  video" + index, " at pages/detail/detail.uvue:136");
        }
        ;
        this.getVideoContext = fun(index: Number): VideoContext {
            val videoContextMap = this.`$data`["\$videoContextMap"] as Map<String, VideoContext>;
            var videoContext: VideoContext? = videoContextMap.get("video-" + index);
            if (videoContext == null) {
                videoContext = uni_createVideoContext("video-" + index, this) as VideoContext;
                videoContextMap.set("video-" + index, videoContext);
            }
            return videoContext;
        }
        ;
        this.doPlay = fun(index: Number) {
            console.log("doPlay  video" + index, " at pages/detail/detail.uvue:148");
            var item: episodeDetailItem = this.list[index] as episodeDetailItem;
            this.list[index].fileUrl = "https://cdn.gohiplay.com/90099a65aa3071eebff65114c1ca0102/271194acda084d1ef60681d2b5b4713c-sd-nbv1-encrypt-stream.m3u8";
            this.getVideoContext(index).play();
        }
        ;
        this.doStop = fun(index: Number) {
            console.log("doStop  video-" + index, " at pages/detail/detail.uvue:170");
            this.getVideoContext(index).stop();
            setTimeout(fun(){
                this.state[index] = "unPlay";
            }
            , 1000);
        }
        ;
        this.doPause = fun(index: Number) {
            this.getVideoContext(index).pause();
            console.log("doPause  video-" + index, " at pages/detail/detail.uvue:179");
        }
        ;
        this.onPause = fun(index: Number) {
            this.state[index] = "pause";
            console.log("onPause", index, " at pages/detail/detail.uvue:183");
        }
        ;
        this.onPlay = fun(index: Number) {
            if (this.current != index || !currentPageIsShow) {
                this.onPause(index);
            } else {
                this.state[index] = "play";
                console.log("onPlay", index, " at pages/detail/detail.uvue:190");
            }
        }
        ;
        this.onSwiperChange = fun(e: SwiperChangeEvent) {
            this.current = e.detail.current;
        }
        ;
        this.onTransition = fun() {};
        this.back = fun() {
            uni_navigateBack(NavigateBackOptions(success = fun(result) {
                console.log("navigateBack success", result.errMsg, " at pages/detail/detail.uvue:229");
            }
            , fail = fun(error) {
                console.log("navigateBack fail", error.errMsg, " at pages/detail/detail.uvue:232");
            }
            , complete = fun(result) {
                console.log("navigateBack complete", result.errMsg, " at pages/detail/detail.uvue:235");
            }
            ));
        }
        ;
    }
    open lateinit var changeState: (index: Number) -> Unit;
    open lateinit var onLoadstart: (index: Number) -> Unit;
    open lateinit var getVideoContext: (index: Number) -> VideoContext;
    open lateinit var doPlay: (index: Number) -> Unit;
    open lateinit var doStop: (index: Number) -> Unit;
    open lateinit var doPause: (index: Number) -> Unit;
    open lateinit var onPause: (index: Number) -> Unit;
    open lateinit var onPlay: (index: Number) -> Unit;
    open lateinit var onSwiperChange: (e: SwiperChangeEvent) -> Unit;
    open lateinit var onTransition: () -> Unit;
    open lateinit var back: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("flex" to 1)), "swiper" to padStyleMapOf(utsMapOf("width" to "100%", "flex" to 1, "height" to "100%")), "swiper-item" to padStyleMapOf(utsMapOf("width" to "100%", "flex" to 1, "height" to "100%", "position" to "relative")), "video-box" to padStyleMapOf(utsMapOf("width" to "100%", "flex" to 1, "height" to "100%")), "video-cover" to padStyleMapOf(utsMapOf("width" to "100%", "flex" to 1, "height" to "100%", "position" to "absolute", "justifyContent" to "center", "alignItems" to "center", "alignContent" to "center")), "play-btn" to padStyleMapOf(utsMapOf("width" to 40, "height" to 40)), "video-info" to padStyleMapOf(utsMapOf("position" to "absolute", "bottom" to 0, "paddingTop" to 15, "paddingRight" to 15, "paddingBottom" to 15, "paddingLeft" to 15)), "video-info-text" to padStyleMapOf(utsMapOf("fontSize" to 14, "color" to "#FF0000", "lineHeight" to "20px")), "debug-info" to padStyleMapOf(utsMapOf("position" to "fixed", "top" to 15, "width" to "100%", "backgroundColor" to "rgba(255,255,255,0.3)")), "status-text" to padStyleMapOf(utsMapOf("color" to "#FF0000", "paddingTop" to 15, "paddingRight" to 15, "paddingBottom" to 15, "paddingLeft" to 15)), "nav-back" to padStyleMapOf(utsMapOf("position" to "absolute", "top" to 35, "backgroundColor" to "rgba(220,220,220,0.8)", "borderRadius" to 100, "marginTop" to 6, "marginRight" to 6, "marginBottom" to 6, "marginLeft" to 6, "width" to 32, "height" to 32, "justifyContent" to "center", "alignItems" to "center", "zIndex" to 10)), "back-img" to utsMapOf(".nav-back " to utsMapOf("width" to 18, "height" to 18)));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
