@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesBrandBrand : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.getData();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("paddingTop" to ((_ctx.statusBarHeight + 50) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "head-title", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon1.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/pp_tit.png", "mode" to "aspectFit", "class" to "image"))
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_icon2.png", "class" to "image"))
                    ))
                ), 4)
            ), 4),
            createElementVNode("view", utsMapOf("class" to "banner-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "banner"), utsArrayOf(
                    createElementVNode("swiper", utsMapOf("class" to "swiper", "autoplay" to false, "circular" to true, "indicator-dots" to true, "indicator-active-color" to "#ffffff", "indicator-color" to "rgba(255,255,255,0.5)"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.bannerList, fun(item, index, _, _): VNode {
                            return createElementVNode("swiper-item", utsMapOf("key" to index), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "mode" to "aspectFit", "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            ));
                        }
                        ), 128)
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "brand-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "brand-view"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "classify-info"), utsArrayOf(
                        createElementVNode("scroll-view", utsMapOf("scroll-x" to true, "class" to "classify-item"), utsArrayOf(
                            createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.classifyList, fun(item, index, _, _): VNode {
                                return createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                                    _ctx.onClassify(item.id);
                                }
                                , "key" to index), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                        "text",
                                        utsMapOf("active" to (_ctx.classifyId === item.id))
                                    ))), toDisplayString(item.name), 3),
                                    createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                                        "line",
                                        utsMapOf("active-line" to (_ctx.classifyId === item.id))
                                    ))), null, 2)
                                ), 8, utsArrayOf(
                                    "onClick"
                                ));
                            }
                            ), 128)
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "brand-list"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.brandList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "goods"), utsArrayOf(
                                    createElementVNode(Fragment, null, RenderHelpers.renderList(item.goodsList, fun(value, idx, _, _): VNode {
                                        return createElementVNode("view", utsMapOf("class" to "pic", "onClick" to fun(){
                                            _ctx.onGoodsItem(value.id as Number);
                                        }
                                        , "key" to idx), utsArrayOf(
                                            createElementVNode("image", utsMapOf("src" to value.pic, "class" to "image"), null, 8, utsArrayOf(
                                                "src"
                                            )),
                                            createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                                createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                                createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(value.price), 1)
                                            ))
                                        ), 8, utsArrayOf(
                                            "onClick"
                                        ));
                                    }
                                    ), 128)
                                )),
                                createElementVNode("view", utsMapOf("class" to "brand"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.bgPic, "mode" to "aspectFit", "class" to "bg"), null, 8, utsArrayOf(
                                        "src"
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "logo"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to item.logo, "mode" to "aspectFit", "class" to "image"), null, 8, utsArrayOf(
                                            "src"
                                        ))
                                    ))
                                ))
                            ));
                        }
                        ), 128)
                    ))
                ))
            ))
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var bannerList: UTSArray<bannerItem1> by `$data`;
    open var classifyList: UTSArray<classifyItem> by `$data`;
    open var classifyId: Number by `$data`;
    open var brandList: UTSArray<breadGoodsItem> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "bannerList" to BannerData as UTSArray<bannerItem1>, "classifyList" to utsArrayOf<classifyItem>(), "classifyId" to 0, "brandList" to brandGoodsData as UTSArray<breadGoodsItem>);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.getData = fun() {
            if (ClassifyData[0].id === 0) {
                ClassifyData.splice(0, 1);
            }
            this.classifyList = ClassifyData;
            this.classifyList.unshift(classifyItem(id = 0, parent = 0, name = "精选推荐", pic = "", children = utsArrayOf<classifyItem>()));
        }
        ;
        this.onClassify = fun(id: Number) {
            this.classifyId = id;
        }
        ;
        this.onGoodsItem = fun(id: Number) {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsDetail/GoodsDetail?goodsId=" + id));
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var getData: () -> Unit;
    open lateinit var onClassify: (id: Number) -> Unit;
    open lateinit var onGoodsItem: (id: Number) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "head-info" to padStyleMapOf(utsMapOf("width" to "100%", "backgroundColor" to "#ffffff")), "head-title" to utsMapOf(".head-info " to utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "back" to utsMapOf(".head-info .head-title " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "image" to utsMapOf(".head-info .head-title .back " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".head-info .head-title .title " to utsMapOf("width" to "280rpx", "height" to "48rpx"), ".head-info .head-title .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".banner-info .banner .swiper " to utsMapOf("width" to "100%", "height" to "100%", "borderRadius" to "20rpx"), ".brand-info .brand-view .brand-list .list .goods .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx"), ".brand-info .brand-view .brand-list .list .brand .logo " to utsMapOf("width" to "130rpx", "height" to "60rpx", "borderRadius" to "80rpx", "backgroundColor" to "#ffffff")), "title" to utsMapOf(".head-info .head-title " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "more" to utsMapOf(".head-info .head-title " to utsMapOf("display" to "flex", "alignItems" to "center")), "banner-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "20rpx")), "banner" to utsMapOf(".banner-info " to utsMapOf("width" to "100%", "height" to "300rpx")), "swiper" to utsMapOf(".banner-info .banner " to utsMapOf("width" to "100%", "height" to "300rpx")), "brand-info" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "20rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "boxSizing" to "border-box")), "brand-view" to utsMapOf(".brand-info " to utsMapOf("width" to "100%", "backgroundColor" to "#0f0e13", "borderTopLeftRadius" to "20rpx", "borderTopRightRadius" to "20rpx", "borderBottomRightRadius" to 0, "borderBottomLeftRadius" to 0)), "classify-info" to utsMapOf(".brand-info .brand-view " to utsMapOf("width" to "100%", "height" to "100rpx")), "classify-item" to utsMapOf(".brand-info .brand-view .classify-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "100rpx")), "item" to utsMapOf(".brand-info .brand-view .classify-info .classify-item " to utsMapOf("position" to "relative", "display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "180rpx", "height" to "100rpx")), "text" to utsMapOf(".brand-info .brand-view .classify-info .classify-item .item " to utsMapOf("fontSize" to "28rpx", "color" to "#b5b4b9")), "line" to utsMapOf(".brand-info .brand-view .classify-info .classify-item .item " to utsMapOf("position" to "absolute", "bottom" to "18rpx", "width" to "20%", "height" to "6rpx", "backgroundColor" to "rgba(0,0,0,0)", "borderRadius" to "8rpx")), "active" to utsMapOf(".brand-info .brand-view .classify-info .classify-item .item " to utsMapOf("color" to "#ffffff", "fontSize" to "32rpx", "fontWeight" to "bold")), "active-line" to utsMapOf(".brand-info .brand-view .classify-info .classify-item .item " to utsMapOf("backgroundColor" to "#ffffff")), "brand-list" to utsMapOf(".brand-info .brand-view " to utsMapOf("width" to "100%", "marginTop" to "10rpx", "paddingTop" to "20rpx", "paddingRight" to "20rpx", "paddingBottom" to "20rpx", "paddingLeft" to "20rpx", "backgroundColor" to "#f7f7f7", "borderTopLeftRadius" to "20rpx", "borderTopRightRadius" to "20rpx", "borderBottomRightRadius" to 0, "borderBottomLeftRadius" to 0)), "list" to utsMapOf(".brand-info .brand-view .brand-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "justifyContent" to "space-between", "width" to "100%", "height" to "240rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx", "marginBottom" to "20rpx")), "goods" to utsMapOf(".brand-info .brand-view .brand-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1, "height" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "pic" to utsMapOf(".brand-info .brand-view .brand-list .list .goods " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "marginRight" to "20rpx")), "price" to utsMapOf(".brand-info .brand-view .brand-list .list .goods .pic " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "marginTop" to "6rpx")), "min" to utsMapOf(".brand-info .brand-view .brand-list .list .goods .pic .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#0f0e13")), "max" to utsMapOf(".brand-info .brand-view .brand-list .list .goods .pic .price " to utsMapOf("fontSize" to "34rpx", "fontWeight" to "bold", "color" to "#0f0e13")), "brand" to utsMapOf(".brand-info .brand-view .brand-list .list " to utsMapOf("position" to "relative", "width" to "150rpx", "height" to "100%")), "bg" to utsMapOf(".brand-info .brand-view .brand-list .list .brand " to utsMapOf("width" to "100%", "height" to "100%", "borderRadius" to "20rpx")), "logo" to utsMapOf(".brand-info .brand-view .brand-list .list .brand " to utsMapOf("position" to "absolute", "bottom" to "60rpx", "left" to 0, "display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
