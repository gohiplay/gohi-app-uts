@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesCartCart : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {}
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1", "padding-bottom" to "120rpx"))), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "cart-info"), utsArrayOf(
                createElementVNode(Fragment, null, RenderHelpers.renderList(4, fun(item, index, _, _): VNode {
                    return createElementVNode("view", utsMapOf("class" to "cart-list", "key" to index), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "shop-info"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "check"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/check_off.png", "class" to "image"))
                            )),
                            createElementVNode("view", utsMapOf("class" to "shop-data"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/shop_icon.png", "class" to "image")),
                                createElementVNode("text", utsMapOf("class" to "text"), "官方旗舰店" + toDisplayString(item), 1)
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "cart-goods"), utsArrayOf(
                            createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.cartList, fun(value, idx, _, _): VNode {
                                return createElementVNode("view", utsMapOf("class" to "goods-item", "key" to idx), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "check"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/check_off.png", "class" to "image"))
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "goods"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                            createElementVNode("image", utsMapOf("src" to value.pic, "class" to "image"), null, 8, utsArrayOf(
                                                "src"
                                            ))
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(value.name), 1)
                                            )),
                                            createElementVNode("view", utsMapOf("class" to "attr"), utsArrayOf(
                                                createElementVNode("text", utsMapOf("class" to "text"), "经典包装：20x20")
                                            )),
                                            createElementVNode("view", utsMapOf("class" to "price-num"), utsArrayOf(
                                                createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                                    createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                                    createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(value.price as Number, 0)), 1),
                                                    if (isTrue(_ctx.formatPrice(value.price as Number, 1))) {
                                                        createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(value.price as Number, 1)), 1);
                                                    } else {
                                                        createCommentVNode("v-if", true);
                                                    }
                                                )),
                                                createElementVNode("view", utsMapOf("class" to "goods-num"), utsArrayOf(
                                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/sub_icon.png", "class" to "add-sub")),
                                                    createElementVNode("text", utsMapOf("class" to "nums"), "12"),
                                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/add_icon.png", "class" to "add-sub"))
                                                ))
                                            ))
                                        ))
                                    ))
                                ));
                            }
                            ), 128)
                        ))
                    ));
                }
                ), 64)
            )),
            createElementVNode("view", utsMapOf("class" to "lose-efficacy"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "cart-goods"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-infos"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "失效商品（2）")
                        )),
                        createElementVNode("view", utsMapOf("class" to "del"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "全部删除")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "goods-lists"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "lists", "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                        "src"
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "商品已失效，暂不支持购买")
                                    ))
                                ))
                            ));
                        }
                        ), 128)
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "footer-total"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "check-all"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/check_off.png", "class" to "image")),
                    createElementVNode("text", utsMapOf("class" to "text"), "全选")
                )),
                createElementVNode("view", utsMapOf("class" to "total-btn"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "total"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "tit"), "合计："),
                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                            createElementVNode("text", utsMapOf("class" to "max"), "99"),
                            createElementVNode("text", utsMapOf("class" to "min"), ".00")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "去结算")
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "guess-like"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "tit"), "猜你喜欢")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                            _ctx.onGoodsItem(item);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                            )),
                            createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                    createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                } else {
                                    createCommentVNode("v-if", true);
                                }
                            ))
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ))
            ))
        ), 4);
    }
    open var cartList: UTSArray<goodsItems1> by `$data`;
    open var goodsList: UTSArray<goodsItems1> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("cartList" to cartData as UTSArray<goodsItems1>, "goodsList" to goodsData as UTSArray<goodsItems1>);
    }
    override fun `$initMethods`() {
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
        this.onGoodsItem = fun(item: goodsItems1) {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsDetail/GoodsDetail?goodsId=" + item.id));
        }
        ;
    }
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    open lateinit var onGoodsItem: (item: goodsItems1) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("cart-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingLeft" to "30rpx", "paddingRight" to "30rpx", "paddingTop" to "30rpx")), "cart-list" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "marginBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "shop-info" to utsMapOf(".cart-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "100rpx")), "check" to utsMapOf(".cart-list .shop-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginRight" to "20rpx"), ".cart-list .cart-goods .goods-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginRight" to "20rpx")), "image" to utsMapOf(".cart-list .shop-info .check " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".cart-list .shop-info .shop-data " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "10rpx"), ".cart-list .cart-goods .goods-item .check " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".cart-list .cart-goods .goods-item .goods .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx", "borderRadius" to "10rpx"), ".footer-total .check-all " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "20rpx"), ".lose-efficacy .cart-goods .goods-lists .lists .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx"), ".guess-like .goods-list .list .pic " to utsMapOf("width" to "334rpx", "height" to "334rpx", "borderTopLeftRadius" to "10rpx", "borderTopRightRadius" to "10rpx", "borderBottomRightRadius" to 0, "borderBottomLeftRadius" to 0)), "shop-data" to utsMapOf(".cart-list .shop-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "text" to utsMapOf(".cart-list .shop-info .shop-data " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".cart-list .cart-goods .goods-item .goods .info .title " to utsMapOf("width" to "100%", "height" to "50rpx", "whiteSpace" to "nowrap", "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#333333"), ".cart-list .cart-goods .goods-item .goods .info .attr " to utsMapOf("width" to "100%", "height" to "40rpx", "whiteSpace" to "nowrap", "textOverflow" to "ellipsis", "fontSize" to "24rpx", "color" to "#aaaaaa"), ".footer-total .check-all " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".footer-total .total-btn .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff"), ".lose-efficacy .cart-goods .title-infos .title " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".lose-efficacy .cart-goods .title-infos .del " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".lose-efficacy .cart-goods .goods-lists .lists .info .name " to utsMapOf("width" to "100%", "height" to "60rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#aaaaaa"), ".lose-efficacy .cart-goods .goods-lists .lists .info .tag " to utsMapOf("fontSize" to "26rpx", "color" to "#fe9a5c"), ".guess-like .goods-list .list .title " to utsMapOf("lines" to 2, "textOverflow" to "ellipsis", "width" to "334rpx", "height" to "70rpx", "fontSize" to "28rpx", "color" to "#333333")), "cart-goods" to utsMapOf(".cart-list " to utsMapOf("width" to "100%"), ".lose-efficacy " to utsMapOf("width" to "100%", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "goods-item" to utsMapOf(".cart-list .cart-goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "140rpx", "marginBottom" to "20rpx")), "goods" to utsMapOf(".cart-list .cart-goods .goods-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "flex" to 1, "height" to "100%")), "pic" to utsMapOf(".cart-list .cart-goods .goods-item .goods " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "20rpx"), ".lose-efficacy .cart-goods .goods-lists .lists " to utsMapOf("marginRight" to "20rpx"), ".guess-like .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "334rpx")), "info" to utsMapOf(".cart-list .cart-goods .goods-item .goods " to utsMapOf("flex" to 1, "height" to "100%"), ".lose-efficacy .cart-goods .goods-lists .lists " to utsMapOf("flex" to 1, "height" to "140rpx")), "title" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info " to utsMapOf("display" to "flex", "alignItems" to "center"), ".title-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".guess-like .goods-list .list " to utsMapOf("display" to "flex", "marginTop" to "10rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "attr" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "price-num" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "justifyContent" to "space-between", "width" to "100%", "height" to "50rpx")), "price" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info .price-num " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end"), ".footer-total .total-btn .total " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".guess-like .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "min" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info .price-num .price " to utsMapOf("fontSize" to "20rpx", "color" to "#fe9a5c"), ".footer-total .total-btn .total .price " to utsMapOf("fontSize" to "24rpx", "color" to "#fe9a5c", "marginTop" to "10rpx"), ".guess-like .goods-list .list .price " to utsMapOf("fontSize" to "22rpx", "fontWeight" to "bold", "marginBottom" to "4rpx", "color" to "#fe9a5c")), "max" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info .price-num .price " to utsMapOf("fontSize" to "30rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".footer-total .total-btn .total .price " to utsMapOf("fontSize" to "42rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".guess-like .goods-list .list .price " to utsMapOf("fontSize" to "36rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "goods-num" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info .price-num " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "add-sub" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info .price-num .goods-num " to utsMapOf("width" to "38rpx", "height" to "38rpx")), "nums" to utsMapOf(".cart-list .cart-goods .goods-item .goods .info .price-num .goods-num " to utsMapOf("width" to "80rpx", "textAlign" to "center", "fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#333333")), "footer-total" to padStyleMapOf(utsMapOf("position" to "fixed", "bottom" to 0, "left" to 0, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "120rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "check-all" to utsMapOf(".footer-total " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "total-btn" to utsMapOf(".footer-total " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "total" to utsMapOf(".footer-total .total-btn " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "tit" to utsMapOf(".footer-total .total-btn .total " to utsMapOf("fontSize" to "28rpx", "color" to "#aaaaaa"), ".title-info .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333")), "btn" to utsMapOf(".footer-total .total-btn " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "200rpx", "height" to "70rpx", "marginLeft" to "20rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "35rpx")), "title-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "marginBottom" to "20rpx")), "line" to utsMapOf(".title-info .title " to utsMapOf("width" to "10rpx", "height" to "40rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "10rpx", "marginTop" to 0, "marginRight" to "20rpx", "marginBottom" to 0, "marginLeft" to "20rpx")), "lose-efficacy" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "boxSizing" to "border-box", "marginTop" to "20rpx", "marginBottom" to "20rpx")), "title-infos" to utsMapOf(".lose-efficacy .cart-goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "del" to utsMapOf(".lose-efficacy .cart-goods .title-infos " to utsMapOf("display" to "flex", "alignItems" to "center")), "goods-lists" to utsMapOf(".lose-efficacy .cart-goods " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx")), "lists" to utsMapOf(".lose-efficacy .cart-goods .goods-lists " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "20rpx")), "name" to utsMapOf(".lose-efficacy .cart-goods .goods-lists .lists .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%")), "tag" to utsMapOf(".lose-efficacy .cart-goods .goods-lists .lists .info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "60rpx", "marginTop" to "20rpx", "backgroundColor" to "rgba(254,154,92,0.3)", "borderRadius" to "10rpx")), "guess-like" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "goods-list" to utsMapOf(".guess-like " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "justifyContent" to "space-between", "width" to "100%")), "list" to utsMapOf(".guess-like .goods-list " to utsMapOf("width" to "334rpx", "marginBottom" to "20rpx", "paddingBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
