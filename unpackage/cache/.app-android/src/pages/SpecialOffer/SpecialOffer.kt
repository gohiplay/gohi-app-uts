@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
open class GenPagesSpecialOfferSpecialOffer : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.getData();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("paddingTop" to ((_ctx.statusBarHeight + 50) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "head-title", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon2.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/tttj_tit.png", "mode" to "aspectFit", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "《真的便宜》")
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_icon2.png", "class" to "image"))
                    ))
                ), 4),
                createElementVNode("view", utsMapOf("class" to "banner-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "banner"), utsArrayOf(
                        createElementVNode("swiper", utsMapOf("class" to "swiper", "autoplay" to false, "circular" to true, "indicator-dots" to true, "indicator-active-color" to "#ffffff", "indicator-color" to "rgba(255,255,255,0.5)"), utsArrayOf(
                            createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.bannerList, fun(item, index, _, _): VNode {
                                return createElementVNode("swiper-item", utsMapOf("item-id" to item.id), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), toDisplayString(index), 9, utsArrayOf(
                                        "src"
                                    ))
                                ), 8, utsArrayOf(
                                    "item-id"
                                ));
                            }
                            ), 256)
                        ))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "hint"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "随便买"),
                        createElementVNode("text", utsMapOf("class" to "text"), "都便宜")
                    )),
                    createElementVNode("view", utsMapOf("class" to "desc"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "全场包邮·便宜到家")
                    ))
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "goods-classify"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "classify-info"), utsArrayOf(
                    createElementVNode("scroll-view", utsMapOf("scroll-x" to true, "class" to "classify-item"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.classifyList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                                _ctx.onClassify(item.id);
                            }
                            , "key" to index), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                    "text",
                                    utsMapOf("active" to (_ctx.classifyId === item.id))
                                ))), toDisplayString(item.name), 3),
                                createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                                    "line",
                                    utsMapOf("active-line" to (_ctx.classifyId === item.id))
                                ))), null, 2)
                            ), 8, utsArrayOf(
                                "onClick"
                            ));
                        }
                        ), 128)
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                            )),
                            createElementVNode("view", utsMapOf("class" to "tag-item"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "包邮")
                                )),
                                createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "特价")
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                    createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                    if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                        createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                    } else {
                                        createCommentVNode("v-if", true);
                                    }
                                )),
                                createElementVNode("view", utsMapOf("class" to "sales"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "已售" + toDisplayString(item.salesVolume) + "件", 1)
                                ))
                            ))
                        ));
                    }
                    ), 128)
                ))
            ))
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var classifyList: UTSArray<classifyItem> by `$data`;
    open var goodsList: UTSArray<goodsItems1> by `$data`;
    open var bannerList: UTSArray<bannerItem1> by `$data`;
    open var classifyId: Number by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "classifyList" to utsArrayOf<classifyItem>(), "goodsList" to goodsData as UTSArray<goodsItems1>, "bannerList" to BannerData as UTSArray<bannerItem1>, "classifyId" to 0);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
        this.getData = fun() {
            if (ClassifyData[0].id === 0) {
                ClassifyData.splice(0, 1);
            }
            this.classifyList = ClassifyData;
            this.classifyList.unshift(classifyItem(id = 0, parent = 0, name = "精选推荐", pic = "", children = utsArrayOf<classifyItem>()));
        }
        ;
        this.onClassify = fun(id: Number) {
            this.classifyId = id;
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    open lateinit var getData: () -> Unit;
    open lateinit var onClassify: (id: Number) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "head-info" to padStyleMapOf(utsMapOf("width" to "100%", "backgroundColor" to "#fe9a5c", "paddingBottom" to "40rpx")), "head-title" to utsMapOf(".head-info " to utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#fe9a5c")), "back" to utsMapOf(".head-info .head-title " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "image" to utsMapOf(".head-info .head-title .back " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".head-info .head-title .title " to utsMapOf("width" to "300rpx", "height" to "78rpx"), ".head-info .head-title .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".head-info .banner-info .banner .swiper " to utsMapOf("width" to "100%", "height" to "100%", "borderRadius" to "20rpx"), ".goods-classify .goods-list .list .pic " to utsMapOf("width" to "100%", "height" to "100%", "borderTopLeftRadius" to "10rpx", "borderTopRightRadius" to "10rpx", "borderBottomRightRadius" to "10rpx", "borderBottomLeftRadius" to "10rpx")), "title" to utsMapOf(".head-info .head-title " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".head-info .hint " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "text" to utsMapOf(".head-info .head-title .title " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff", "marginLeft" to "-40rpx"), ".head-info .hint .title " to utsMapOf("fontSize" to "28rpx", "fontStyle" to "italic", "color" to "#ffffff", "marginRight" to "20rpx"), ".head-info .hint .desc " to utsMapOf("fontSize" to "26rpx", "color" to "#ffffff", "opacity" to 0.8), ".goods-classify .classify-info .classify-item .item " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".goods-classify .goods-list .list .name " to utsMapOf("paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "width" to "100%", "height" to "80rpx", "lineHeight" to "40rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#333333"), ".goods-classify .goods-list .list .tag-item .tag " to utsMapOf("fontSize" to "22rpx", "color" to "#fe9a5c"), ".goods-classify .goods-list .list .price-info .sales " to utsMapOf("fontSize" to "22rpx", "color" to "#aaaaaa")), "more" to utsMapOf(".head-info .head-title " to utsMapOf("display" to "flex", "alignItems" to "center")), "banner-info" to utsMapOf(".head-info " to utsMapOf("width" to "100%", "height" to "300rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "10rpx", "boxSizing" to "border-box")), "banner" to utsMapOf(".head-info .banner-info " to utsMapOf("width" to "100%", "height" to "300rpx")), "swiper" to utsMapOf(".head-info .banner-info .banner " to utsMapOf("width" to "100%", "height" to "300rpx")), "hint" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "60rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "desc" to utsMapOf(".head-info .hint " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "goods-classify" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "-30rpx", "backgroundColor" to "#f8f8f8", "borderTopLeftRadius" to "20rpx", "borderTopRightRadius" to "20rpx", "borderBottomRightRadius" to 0, "borderBottomLeftRadius" to 0)), "classify-info" to utsMapOf(".goods-classify " to utsMapOf("width" to "100%", "height" to "100rpx")), "classify-item" to utsMapOf(".goods-classify .classify-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "100rpx")), "item" to utsMapOf(".goods-classify .classify-info .classify-item " to utsMapOf("position" to "relative", "display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "180rpx", "height" to "100rpx")), "line" to utsMapOf(".goods-classify .classify-info .classify-item .item " to utsMapOf("position" to "absolute", "bottom" to "18rpx", "width" to "20%", "height" to "8rpx", "backgroundColor" to "rgba(0,0,0,0)", "borderRadius" to "8rpx")), "active" to utsMapOf(".goods-classify .classify-info .classify-item .item " to utsMapOf("color" to "#fe9a5c", "fontSize" to "32rpx", "fontWeight" to "bold")), "active-line" to utsMapOf(".goods-classify .classify-info .classify-item .item " to utsMapOf("backgroundColor" to "#fe9a5c")), "goods-list" to utsMapOf(".goods-classify " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "justifyContent" to "space-between", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "list" to utsMapOf(".goods-classify .goods-list " to utsMapOf("width" to "334rpx", "paddingBottom" to "30rpx", "marginBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "10rpx")), "pic" to utsMapOf(".goods-classify .goods-list .list " to utsMapOf("width" to "100%", "height" to "334rpx")), "name" to utsMapOf(".goods-classify .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "boxSizing" to "border-box", "marginTop" to "10rpx")), "tag-item" to utsMapOf(".goods-classify .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "marginTop" to "10rpx")), "tag" to utsMapOf(".goods-classify .goods-list .list .tag-item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "2rpx", "paddingRight" to "10rpx", "paddingBottom" to "2rpx", "paddingLeft" to "10rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#fe9a5c", "marginRight" to "10rpx", "borderRadius" to "6rpx")), "price-info" to utsMapOf(".goods-classify .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "marginTop" to "16rpx")), "price" to utsMapOf(".goods-classify .goods-list .list .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "justifyContent" to "center")), "min" to utsMapOf(".goods-classify .goods-list .list .price-info .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#ff6e65")), "max" to utsMapOf(".goods-classify .goods-list .list .price-info .price " to utsMapOf("fontSize" to "38rpx", "fontWeight" to "bold", "color" to "#ff6e65")), "sales" to utsMapOf(".goods-classify .goods-list .list .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
