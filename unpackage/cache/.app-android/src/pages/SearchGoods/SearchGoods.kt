@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesSearchGoodsSearchGoods : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(options: OnLoadOptions) {
            this.goodsCut = 1;
            val windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.screenHeight = windowInfo.screenHeight;
            this.keyword = if (options.get("keyword") === null) {
                "";
            } else {
                options.get("keyword") as String;
            }
            ;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_cat_popup = resolveComponent("cat-popup");
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1", "background-color" to "#ffffff"))), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "search-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "search"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/fdj_icon.png", "class" to "image")),
                        createElementVNode("input", utsMapOf("type" to "text", "placeholder" to "请输入搜索内容", "modelValue" to _ctx.keyword, "onInput" to fun(`$event`: InputEvent){
                            _ctx.keyword = `$event`.detail.value;
                        }
                        , "class" to "input"), null, 40, utsArrayOf(
                            "modelValue",
                            "onInput"
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "搜索")
                    ))
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "filtrate-info", "style" to normalizeStyle(utsMapOf("top" to ((_ctx.statusBarHeight + 50) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "filtrate-item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item active"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "综合推荐")
                        )),
                        createElementVNode("view", utsMapOf("class" to "cut"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/pxx_ac_icon.png", "class" to "image"))
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "销量")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "价格")
                        )),
                        createElementVNode("view", utsMapOf("class" to "cut"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/sort_icon.png", "class" to "up"))
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title", "onClick" to _ctx.onFiltrate), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "筛选")
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "icon", "onClick" to _ctx.onGoodCut), utsArrayOf(
                            withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/icon/lbt_icon.png", "class" to "image"), null, 512), utsArrayOf(
                                utsArrayOf(
                                    vShow,
                                    _ctx.goodsCut === 0
                                )
                            )),
                            withDirectives(createElementVNode("image", utsMapOf("src" to "/static/images/icon/stt_icon.png", "class" to "image"), null, 512), utsArrayOf(
                                utsArrayOf(
                                    vShow,
                                    _ctx.goodsCut === 1
                                )
                            ))
                        ), 8, utsArrayOf(
                            "onClick"
                        ))
                    ))
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "goods-info", "style" to normalizeStyle(utsMapOf("backgroundColor" to if (_ctx.goodsCut === 1) {
                "#f8f8f8";
            } else {
                "#ffffff";
            }
            , "marginTop" to ((_ctx.statusBarHeight + 100) + "px")))), utsArrayOf(
                withDirectives(createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsLists, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                            _ctx.onGoodsItem(item);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "sales-volume"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.salesVolume) + "件售出", 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                        createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                        if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                            createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                        } else {
                                            createCommentVNode("v-if", true);
                                        }
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "old-price"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(item.originalPrice), 1)
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "shop-info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.shopName), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "进店"),
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                                    ))
                                ))
                            ))
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ), 512), utsArrayOf(
                    utsArrayOf(
                        vShow,
                        _ctx.goodsCut === 0
                    )
                )),
                withDirectives(createElementVNode("view", utsMapOf("class" to "goods-view"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsLists, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "onClick" to fun(){
                            _ctx.onGoodsItem(item);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "sales-volume"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.salesVolume) + "件售出", 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                        createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                        if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                            createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                        } else {
                                            createCommentVNode("v-if", true);
                                        }
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "old-price"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(item.originalPrice), 1)
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "shop-info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.shopName), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "进店"),
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                                    ))
                                ))
                            ))
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ), 512), utsArrayOf(
                    utsArrayOf(
                        vShow,
                        _ctx.goodsCut === 1
                    )
                ))
            ), 4),
            withDirectives(createElementVNode("view", utsMapOf("class" to "sort-popup", "style" to normalizeStyle(utsMapOf("paddingTop" to ((_ctx.statusBarHeight + 90) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "sort-item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item active"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "综合推荐")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "评价优先")
                    ))
                ))
            ), 4), utsArrayOf(
                utsArrayOf(
                    vShow,
                    false
                )
            )),
            createVNode(_component_cat_popup, utsMapOf("ref" to "CatPopup", "mode" to "right"), utsMapOf("default" to withSlotCtx(fun(): UTSArray<Any> {
                return utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "filtrate-popup", "style" to normalizeStyle(utsMapOf("paddingTop" to _ctx.statusBarHeight, "height" to (_ctx.screenHeight + "px")))), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "filtrate-data"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "filtrate-title"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "服务")
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "serve-item"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                                    "item",
                                    utsMapOf("active" to (_ctx.serveCut === 0))
                                )), "onClick" to fun(){
                                    _ctx.onFiltrateServe(0);
                                }
                                ), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                        "text",
                                        utsMapOf("active-text" to (_ctx.serveCut === 0))
                                    ))), "包邮", 2)
                                ), 10, utsArrayOf(
                                    "onClick"
                                )),
                                createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                                    "item",
                                    utsMapOf("active" to (_ctx.serveCut === 1))
                                )), "onClick" to fun(){
                                    _ctx.onFiltrateServe(1);
                                }
                                ), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                        "text",
                                        utsMapOf("active-text" to (_ctx.serveCut === 1))
                                    ))), "有货", 2)
                                ), 10, utsArrayOf(
                                    "onClick"
                                )),
                                createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                                    "item",
                                    utsMapOf("active" to (_ctx.serveCut === 2))
                                )), "onClick" to fun(){
                                    _ctx.onFiltrateServe(2);
                                }
                                ), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                        "text",
                                        utsMapOf("active-text" to (_ctx.serveCut === 2))
                                    ))), "新品", 2)
                                ), 10, utsArrayOf(
                                    "onClick"
                                ))
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "filtrate-data"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "filtrate-title"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "价格区间")
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "price-area"), utsArrayOf(
                                createElementVNode("input", utsMapOf("type" to "number", "placeholder" to "最低价", "class" to "price")),
                                createElementVNode("text", utsMapOf("class" to "line"), "-"),
                                createElementVNode("input", utsMapOf("type" to "number", "placeholder" to "最高价", "class" to "price"))
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "reset-confirm"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "rest"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "重置")
                            )),
                            createElementVNode("view", utsMapOf("class" to "confirm"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "确认")
                            ))
                        ))
                    ), 4)
                );
            }
            ), "_" to 1), 512)
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var screenHeight: Number by `$data`;
    open var goodsCut: Number by `$data`;
    open var keyword: String by `$data`;
    open var goodsLists: UTSArray<goodsItems1> by `$data`;
    open var serveCut: Number by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "screenHeight" to 0, "goodsCut" to 0, "keyword" to "", "goodsLists" to goodsData as UTSArray<goodsItems1>, "serveCut" to 0);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.onGoodCut = fun() {
            this.goodsCut = if (this.goodsCut === 1) {
                0;
            } else {
                1;
            }
            ;
        }
        ;
        this.onFiltrate = fun() {
            (this.`$refs`["CatPopup"] as ComponentPublicInstance).`$callMethod`("open");
        }
        ;
        this.onFiltrateServe = fun(type: Number) {
            this.serveCut = type;
        }
        ;
        this.onGoodsItem = fun(item: goodsItems1) {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsDetail/GoodsDetail?goodsId=" + item.id));
        }
        ;
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var onGoodCut: () -> Unit;
    open lateinit var onFiltrate: () -> Unit;
    open lateinit var onFiltrateServe: (type: Number) -> Unit;
    open lateinit var onGoodsItem: (item: goodsItems1) -> Unit;
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("head-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff", "zIndex" to 100)), "search-info" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "back" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100rpx", "height" to "100%")), "image" to utsMapOf(".head-info .search-info .back " to utsMapOf("width" to "58rpx", "height" to "58rpx"), ".head-info .search-info .search " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "10rpx"), ".filtrate-info .filtrate-item .item .cut " to utsMapOf("width" to "34rpx", "height" to "34rpx"), ".filtrate-info .filtrate-item .item .icon " to utsMapOf("width" to "30rpx", "height" to "30rpx"), ".goods-info .goods-list .list .pic " to utsMapOf("width" to "200rpx", "height" to "200rpx", "borderRadius" to "10rpx"), ".goods-info .goods-list .list .info .shop-info .more " to utsMapOf("width" to "28rpx", "height" to "28rpx"), ".goods-info .goods-view .list .pic " to utsMapOf("width" to "335rpx", "height" to "335rpx", "borderRadius" to "10rpx"), ".goods-info .goods-view .list .info .shop-info .more " to utsMapOf("width" to "28rpx", "height" to "28rpx")), "search" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1, "height" to "60rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginRight" to "20rpx", "backgroundColor" to "#f8f8f8", "borderRadius" to "30rpx")), "input" to utsMapOf(".head-info .search-info .search " to utsMapOf("flex" to 1, "height" to "100%", "fontSize" to "26rpx", "color" to "#333333")), "btn" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100rpx", "height" to "50rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "30rpx")), "text" to utsMapOf(".head-info .search-info .btn " to utsMapOf("fontSize" to "26rpx", "color" to "#ffffff"), ".filtrate-info .filtrate-item .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".filtrate-info .filtrate-item .active .title " to utsMapOf("color" to "#fe9a5c"), ".sort-popup .sort-item .item " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".sort-popup .sort-item .active " to utsMapOf("color" to "#fe9a5c"), ".goods-info .goods-list .list .info .title " to utsMapOf("width" to "100%", "height" to "70rpx", "fontSize" to "28rpx", "color" to "#333333", "lines" to 2, "textOverflow" to "ellipsis"), ".goods-info .goods-list .list .info .sales-volume " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".goods-info .goods-list .list .info .price-info .old-price " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa", "textDecorationLine" to "line-through"), ".goods-info .goods-list .list .info .shop-info .name " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".goods-info .goods-list .list .info .shop-info .more " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".goods-info .goods-view .list .info .title " to utsMapOf("width" to "100%", "height" to "70rpx", "fontSize" to "28rpx", "color" to "#333333", "lines" to 2, "textOverflow" to "ellipsis"), ".goods-info .goods-view .list .info .sales-volume " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".goods-info .goods-view .list .info .price-info .old-price " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa", "textDecorationLine" to "line-through"), ".goods-info .goods-view .list .info .shop-info .name " to utsMapOf("width" to "200rpx", "height" to "40rpx", "fontSize" to "26rpx", "color" to "#aaaaaa", "whiteSpace" to "nowrap", "textOverflow" to "ellipsis"), ".goods-info .goods-view .list .info .shop-info .more " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".filtrate-popup .filtrate-data .filtrate-title .title " to utsMapOf("fontSize" to "30rpx", "fontWeight" to "bold", "color" to "#333333"), ".filtrate-popup .filtrate-data .serve-item .item " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".filtrate-popup .reset-confirm .rest " to utsMapOf("fontSize" to "28rpx", "color" to "#fe9a5c"), ".filtrate-popup .reset-confirm .confirm " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "filtrate-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "width" to "100%", "height" to "80rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "filtrate-item" to utsMapOf(".filtrate-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100%")), "item" to utsMapOf(".filtrate-info .filtrate-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "25%"), ".sort-popup .sort-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "60rpx"), ".filtrate-popup .filtrate-data .serve-item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "160rpx", "height" to "60rpx", "marginBottom" to "20rpx", "backgroundColor" to "#f8f8f8", "borderRadius" to "30rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "rgba(0,0,0,0)")), "title" to utsMapOf(".filtrate-info .filtrate-item .item " to utsMapOf("display" to "flex"), ".goods-info .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".goods-info .goods-view .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx"), ".filtrate-popup .filtrate-data .filtrate-title " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "cut" to utsMapOf(".filtrate-info .filtrate-item .item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center")), "up" to utsMapOf(".filtrate-info .filtrate-item .item .cut " to utsMapOf("width" to "28rpx", "height" to "28rpx")), "down" to utsMapOf(".filtrate-info .filtrate-item .item .cut " to utsMapOf("width" to "24rpx", "height" to "24rpx", "marginTop" to "-6rpx")), "icon" to utsMapOf(".filtrate-info .filtrate-item .item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "marginLeft" to "20rpx")), "sort-popup" to padStyleMapOf(utsMapOf("position" to "fixed", "zIndex" to 10, "width" to "100%", "height" to "100%", "backgroundColor" to "rgba(0,0,0,0.5)")), "sort-item" to utsMapOf(".sort-popup " to utsMapOf("width" to "100%", "paddingTop" to "20rpx", "paddingRight" to "40rpx", "paddingBottom" to "20rpx", "paddingLeft" to "40rpx", "backgroundColor" to "#ffffff", "borderTopLeftRadius" to 0, "borderTopRightRadius" to 0, "borderBottomRightRadius" to "20rpx", "borderBottomLeftRadius" to "20rpx")), "goods-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#f8f8f8")), "goods-list" to utsMapOf(".goods-info " to utsMapOf("width" to "100%", "marginTop" to "20rpx")), "list" to utsMapOf(".goods-info .goods-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "height" to "200rpx", "marginBottom" to "20rpx"), ".goods-info .goods-view " to utsMapOf("width" to "335rpx", "marginBottom" to "20rpx", "paddingBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "pic" to utsMapOf(".goods-info .goods-list .list " to utsMapOf("marginRight" to "20rpx"), ".goods-info .goods-view .list " to utsMapOf("display" to "flex")), "info" to utsMapOf(".goods-info .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "column", "flex" to 1), ".goods-info .goods-view .list " to utsMapOf("display" to "flex", "flexDirection" to "column", "width" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "sales-volume" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex"), ".goods-info .goods-view .list .info " to utsMapOf("display" to "flex")), "price-info" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx"), ".goods-info .goods-view .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx")), "price" to utsMapOf(".goods-info .goods-list .list .info .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end"), ".goods-info .goods-view .list .info .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end"), ".filtrate-popup .filtrate-data .price-area " to utsMapOf("width" to "240rpx", "height" to "60rpx", "paddingTop" to 0, "paddingRight" to "10rpx", "paddingBottom" to 0, "paddingLeft" to "10rpx", "backgroundColor" to "#f8f8f8", "borderRadius" to "30rpx", "fontSize" to "26rpx", "color" to "#333333", "textAlign" to "center")), "min" to utsMapOf(".goods-info .goods-list .list .info .price-info .price " to utsMapOf("fontSize" to "22rpx", "color" to "#fe9a5c", "marginBottom" to "4rpx"), ".goods-info .goods-view .list .info .price-info .price " to utsMapOf("fontSize" to "22rpx", "color" to "#fe9a5c", "marginBottom" to "4rpx")), "max" to utsMapOf(".goods-info .goods-list .list .info .price-info .price " to utsMapOf("fontSize" to "34rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".goods-info .goods-view .list .info .price-info .price " to utsMapOf("fontSize" to "34rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "old-price" to utsMapOf(".goods-info .goods-list .list .info .price-info " to utsMapOf("display" to "flex", "alignItems" to "center", "marginLeft" to "10rpx"), ".goods-info .goods-view .list .info .price-info " to utsMapOf("display" to "flex", "alignItems" to "center", "marginLeft" to "10rpx")), "shop-info" to utsMapOf(".goods-info .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx"), ".goods-info .goods-view .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginTop" to "10rpx")), "name" to utsMapOf(".goods-info .goods-list .list .info .shop-info " to utsMapOf("display" to "flex", "alignItems" to "center"), ".goods-info .goods-view .list .info .shop-info " to utsMapOf("display" to "flex", "alignItems" to "center")), "more" to utsMapOf(".goods-info .goods-list .list .info .shop-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginLeft" to "20rpx"), ".goods-info .goods-view .list .info .shop-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginLeft" to "10rpx")), "goods-view" to utsMapOf(".goods-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "justifyContent" to "space-between", "width" to "100%", "marginTop" to "20rpx")), "filtrate-popup" to padStyleMapOf(utsMapOf("position" to "relative", "width" to "560rpx", "height" to "1334rpx", "backgroundColor" to "#ffffff", "borderTopLeftRadius" to "30rpx", "borderTopRightRadius" to 0, "borderBottomRightRadius" to 0, "borderBottomLeftRadius" to "30rpx")), "filtrate-data" to utsMapOf(".filtrate-popup " to utsMapOf("width" to "100%")), "filtrate-title" to utsMapOf(".filtrate-popup .filtrate-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "serve-item" to utsMapOf(".filtrate-popup .filtrate-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "justifyContent" to "space-between", "flexWrap" to "wrap", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "active-text" to utsMapOf(".filtrate-popup .filtrate-data .serve-item .item " to utsMapOf("color" to "#fe9a5c")), "active" to utsMapOf(".filtrate-popup .filtrate-data .serve-item " to utsMapOf("backgroundColor" to "rgba(254,154,92,0.3)", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#fe9a5c")), "price-area" to utsMapOf(".filtrate-popup .filtrate-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "line" to utsMapOf(".filtrate-popup .filtrate-data .price-area " to utsMapOf("marginTop" to 0, "marginRight" to "10rpx", "marginBottom" to 0, "marginLeft" to "10rpx", "fontSize" to "32rpx", "color" to "#333333")), "reset-confirm" to utsMapOf(".filtrate-popup " to utsMapOf("position" to "absolute", "left" to 0, "bottom" to "30rpx", "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-around", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "rest" to utsMapOf(".filtrate-popup .reset-confirm " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "220rpx", "height" to "70rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#fe9a5c", "borderRadius" to "40rpx")), "confirm" to utsMapOf(".filtrate-popup .reset-confirm " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "220rpx", "height" to "70rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "40rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf("CatPopup" to GenComponentsCatPopupCatPopupClass);
    }
}
