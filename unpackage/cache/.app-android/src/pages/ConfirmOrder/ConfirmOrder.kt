@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
import io.dcloud.uniapp.extapi.showToast as uni_showToast;
open class GenPagesConfirmOrderConfirmOrder : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(options: OnLoadOptions) {
            val windowInfo = uni_getWindowInfo();
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
            val goodsId = options.get("goodsId") as String;
            this.goodsId = goodsId.toInt();
            this.getData();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("style" to normalizeStyle(utsMapOf("paddingBottom" to ((_ctx.statusBarBottom + 50) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "address-info", "onClick" to _ctx.onAddress), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "address-data"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "default-city"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "tag-item"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "默认")
                                )),
                                createElementVNode("view", utsMapOf("class" to "item blue"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "公司")
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "address-city"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "浙江杭州市滨江区")
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "address"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "长河街道华洲中心中南一街123号")
                        )),
                        createElementVNode("view", utsMapOf("class" to "name-phone"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "哇哈哈")
                            )),
                            createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "17888888888")
                            ))
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                    ))
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "shop-goods"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(2, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "shop-list", "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "shop-name"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "icon"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/shop_icon.png", "class" to "image"))
                                )),
                                createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(_ctx.info.shopName) + toDisplayString(item), 1)
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                                createElementVNode(Fragment, null, RenderHelpers.renderList(2, fun(value, idx, _, _): VNode {
                                    return createElementVNode("view", utsMapOf("class" to "list", "key" to idx), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                            createElementVNode("image", utsMapOf("src" to _ctx.info.pic, "class" to "image"), null, 8, utsArrayOf(
                                                "src"
                                            ))
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                            createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(_ctx.info.name) + toDisplayString(value), 1)
                                            )),
                                            createElementVNode("view", utsMapOf("class" to "attr"), utsArrayOf(
                                                createElementVNode("text", utsMapOf("class" to "text"), "颜色,黑色")
                                            )),
                                            createElementVNode("view", utsMapOf("class" to "price-number"), utsArrayOf(
                                                createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                                    createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                                    createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.info.price), 1)
                                                )),
                                                createElementVNode("view", utsMapOf("class" to "number"), utsArrayOf(
                                                    createElementVNode("text", utsMapOf("class" to "text"), "x1")
                                                ))
                                            ))
                                        ))
                                    ));
                                }
                                ), 64)
                            )),
                            createElementVNode("view", utsMapOf("class" to "shop-select"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "配送方式")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "select"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "快递运输")
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                                        ))
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "优惠券")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "select"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text pl"), "暂无优惠券")
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                                        ))
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "留言")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "txt"), utsArrayOf(
                                            createElementVNode("input", utsMapOf("type" to "text", "placeholder" to "建议留言前先与商家沟通", "class" to "input"))
                                        ))
                                    ))
                                ))
                            ))
                        ));
                    }
                    ), 64)
                )),
                createElementVNode("view", utsMapOf("class" to "order-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "order-item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "商品总额")
                            )),
                            createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(_ctx.info.price), 1)
                                ))
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "运费")
                            )),
                            createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "￥6.00")
                                ))
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "优惠券")
                            )),
                            createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "select"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text pl"), "暂无优惠券")
                                )),
                                createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                                ))
                            ))
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "total-price"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "tit"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "合计：")
                        )),
                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(_ctx.info.price), 1)
                        ))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "footer-info", "style" to normalizeStyle(utsMapOf("paddingBottom" to (_ctx.statusBarBottom + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "实付款：")
                        )),
                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                            createElementVNode("text", utsMapOf("class" to "max"), "99"),
                            createElementVNode("text", utsMapOf("class" to "min"), ".00")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "btn-info"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "btn", "onClick" to _ctx.onSubmit), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "提交订单")
                        ), 8, utsArrayOf(
                            "onClick"
                        ))
                    ))
                ), 4)
            ), 4)
        ), 4);
    }
    open var statusBarBottom: Number by `$data`;
    open var goodsId: Number by `$data`;
    open var info: goodsItems1 by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarBottom" to 0, "goodsId" to 0, "info" to goodsItems1());
    }
    override fun `$initMethods`() {
        this.getData = fun() {
            goodsData.forEach(fun(item){
                if (item.id === this.goodsId) {
                    this.info = item;
                }
            }
            );
        }
        ;
        this.onAddress = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/AddressList/AddressList"));
        }
        ;
        this.onSubmit = fun() {
            uni_showToast(ShowToastOptions(title = "提交成功", success = fun(_){
                uni_navigateTo(NavigateToOptions(url = "/pages/OrderList/OrderList"));
            }
            ));
        }
        ;
    }
    open lateinit var getData: () -> Unit;
    open lateinit var onAddress: () -> Unit;
    open lateinit var onSubmit: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "address-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "backgroundColor" to "#ffffff", "marginTop" to "30rpx", "marginRight" to "30rpx", "marginBottom" to "30rpx", "marginLeft" to "30rpx", "paddingTop" to "30rpx", "paddingRight" to "30rpx", "paddingBottom" to "30rpx", "paddingLeft" to "30rpx", "borderRadius" to "20rpx")), "address-data" to utsMapOf(".address-info " to utsMapOf("flex" to 1)), "default-city" to utsMapOf(".address-info .address-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "20rpx")), "tag-item" to utsMapOf(".address-info .address-data .default-city " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "item" to utsMapOf(".address-info .address-data .default-city .tag-item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "2rpx", "paddingRight" to "10rpx", "paddingBottom" to "2rpx", "paddingLeft" to "10rpx", "marginRight" to "10rpx", "backgroundColor" to "#ff6e65", "borderRadius" to "4rpx"), ".shop-goods .shop-list .shop-select " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "80rpx"), ".order-info .order-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "80rpx")), "text" to utsMapOf(".address-info .address-data .default-city .tag-item .item " to utsMapOf("fontSize" to "20rpx", "color" to "#ffffff"), ".address-info .address-data .default-city .address-city " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".address-info .address-data .address " to utsMapOf("fontSize" to "38rpx", "fontWeight" to "bold", "color" to "#333333"), ".address-info .address-data .name-phone .name " to utsMapOf("fontSize" to "28rpx", "color" to "#aaaaaa"), ".shop-goods .shop-list .shop-name .name " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".shop-goods .shop-list .goods-list .list .info .name " to utsMapOf("width" to "100%", "height" to "60rpx", "whiteSpace" to "nowrap", "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#333333"), ".shop-goods .shop-list .goods-list .list .info .attr " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".shop-goods .shop-list .goods-list .list .info .price-number .number " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".shop-goods .shop-list .shop-select .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".shop-goods .shop-list .shop-select .item .content .select " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-info .order-item .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-info .order-item .item .content .select " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-info .order-item .item .content .price " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".order-info .total-price .tit " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-info .total-price .price " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".footer-info .price-info .title " to utsMapOf("fontSize" to "28rpx", "color" to "#aaaaaa"), ".footer-info .btn-info .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "blue" to utsMapOf(".address-info .address-data .default-city .tag-item " to utsMapOf("backgroundColor" to "#ffdb53")), "address-city" to utsMapOf(".address-info .address-data .default-city " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "address" to utsMapOf(".address-info .address-data " to utsMapOf("width" to "100%", "marginBottom" to "20rpx")), "name-phone" to utsMapOf(".address-info .address-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%")), "name" to utsMapOf(".address-info .address-data .name-phone " to utsMapOf("display" to "flex", "marginRight" to "10rpx"), ".shop-goods .shop-list .shop-name " to utsMapOf("display" to "flex", "alignItems" to "center"), ".shop-goods .shop-list .goods-list .list .info " to utsMapOf("width" to "100%", "height" to "40rpx", "overflow" to "hidden")), "more" to utsMapOf(".address-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center"), ".shop-goods .shop-list .shop-select .item .content " to utsMapOf("display" to "flex", "alignItems" to "center"), ".order-info .order-item .item .content " to utsMapOf("display" to "flex", "alignItems" to "center")), "image" to utsMapOf(".address-info .more " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".shop-goods .shop-list .shop-name .icon " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".shop-goods .shop-list .goods-list .list .pic " to utsMapOf("width" to "160rpx", "height" to "160rpx", "borderRadius" to "10rpx"), ".shop-goods .shop-list .shop-select .item .content .more " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".order-info .order-item .item .content .more " to utsMapOf("width" to "38rpx", "height" to "38rpx")), "shop-goods" to padStyleMapOf(utsMapOf("marginLeft" to "30rpx", "marginRight" to "30rpx")), "shop-list" to utsMapOf(".shop-goods " to utsMapOf("width" to "100%", "marginBottom" to "30rpx", "paddingTop" to "30rpx", "paddingRight" to "30rpx", "paddingBottom" to "30rpx", "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "shop-name" to utsMapOf(".shop-goods .shop-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "20rpx")), "icon" to utsMapOf(".shop-goods .shop-list .shop-name " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "10rpx")), "goods-list" to utsMapOf(".shop-goods .shop-list " to utsMapOf("width" to "100%", "marginBottom" to 10)), "list" to utsMapOf(".shop-goods .shop-list .goods-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "160rpx", "marginBottom" to "20rpx")), "pic" to utsMapOf(".shop-goods .shop-list .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "info" to utsMapOf(".shop-goods .shop-list .goods-list .list " to utsMapOf("flex" to 1, "height" to "100%")), "attr" to utsMapOf(".shop-goods .shop-list .goods-list .list .info " to utsMapOf("width" to "100%", "marginBottom" to "10rpx")), "price-number" to utsMapOf(".shop-goods .shop-list .goods-list .list .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "marginTop" to "20rpx")), "price" to utsMapOf(".shop-goods .shop-list .goods-list .list .info .price-number " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end"), ".order-info .order-item .item .content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".order-info .total-price " to utsMapOf("display" to "flex", "alignItems" to "center"), ".footer-info .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "min" to utsMapOf(".shop-goods .shop-list .goods-list .list .info .price-number .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".footer-info .price-info .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#fe9a5c", "marginBottom" to "4rpx")), "max" to utsMapOf(".shop-goods .shop-list .goods-list .list .info .price-number .price " to utsMapOf("fontSize" to "34rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".footer-info .price-info .price " to utsMapOf("fontWeight" to "bold", "fontSize" to "48rpx", "color" to "#fe9a5c")), "number" to utsMapOf(".shop-goods .shop-list .goods-list .list .info .price-number " to utsMapOf("display" to "flex", "alignItems" to "center")), "shop-select" to utsMapOf(".shop-goods .shop-list " to utsMapOf("width" to "100%")), "title" to utsMapOf(".shop-goods .shop-list .shop-select .item " to utsMapOf("display" to "flex", "alignItems" to "center"), ".order-info .order-item .item " to utsMapOf("display" to "flex", "alignItems" to "center"), ".footer-info .price-info " to utsMapOf("display" to "flex", "alignItems" to "center")), "content" to utsMapOf(".shop-goods .shop-list .shop-select .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".order-info .order-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "select" to utsMapOf(".shop-goods .shop-list .shop-select .item .content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".order-info .order-item .item .content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "pl" to utsMapOf(".shop-goods .shop-list .shop-select .item .content .select " to utsMapOf("color" to "#aaaaaa"), ".order-info .order-item .item .content .select " to utsMapOf("color" to "#aaaaaa")), "txt" to utsMapOf(".shop-goods .shop-list .shop-select .item .content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".order-info .order-item .item .content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "input" to utsMapOf(".shop-goods .shop-list .shop-select .item .content .txt " to utsMapOf("width" to "320rpx", "fontSize" to "28rpx", "color" to "#333333", "textAlign" to "right"), ".order-info .order-item .item .content .txt " to utsMapOf("width" to "320rpx", "fontSize" to "28rpx", "color" to "#333333", "textAlign" to "right")), "order-info" to padStyleMapOf(utsMapOf("paddingTop" to "20rpx", "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx", "marginLeft" to "30rpx", "marginRight" to "30rpx", "marginBottom" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "order-item" to utsMapOf(".order-info " to utsMapOf("width" to "100%")), "total-price" to utsMapOf(".order-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "flex-end", "width" to "100%", "height" to "80rpx", "borderTopWidth" to 1, "borderTopStyle" to "solid", "borderTopColor" to "#f8f8f8")), "tit" to utsMapOf(".order-info .total-price " to utsMapOf("display" to "flex", "alignItems" to "center")), "footer-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "bottom" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "minHeight" to "100rpx", "paddingLeft" to "30rpx", "paddingRight" to "30rpx", "backgroundColor" to "#ffffff")), "price-info" to utsMapOf(".footer-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "btn-info" to utsMapOf(".footer-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "btn" to utsMapOf(".footer-info .btn-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "230rpx", "height" to "70rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "40rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
