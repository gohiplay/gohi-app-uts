@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
open class GenPagesMemberCenterMemberCenter : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page", "onScroll" to _ctx.onPageScroll), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "head-back", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px"), "backgroundColor" to ("rgba(255,255,255," + _ctx.pageScrollTop + ")")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "会员中心")
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_icon1.png", "class" to "image"))
                    ))
                ), 4),
                createElementVNode("view", utsMapOf("class" to "card-info", "style" to normalizeStyle(utsMapOf("marginTop" to ((_ctx.statusBarHeight + 50) + "px")))), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/hyzx_bg.png", "mode" to "scaleToFill", "class" to "bg")),
                    createElementVNode("view", utsMapOf("class" to "card"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title-btn"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "title-desc"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "title"), "优享会员"),
                                createElementVNode("text", utsMapOf("class" to "desc"), "尊享12大特权，省心又省钱")
                            )),
                            createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "去续费")
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "economize-price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "economize"), "已为您节省"),
                            createElementVNode("text", utsMapOf("class" to "price"), "￥999")
                        ))
                    ))
                ), 4)
            )),
            createElementVNode("view", utsMapOf("class" to "rights-interests"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title-more"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "line")),
                        createElementVNode("text", utsMapOf("class" to "text"), "我的权益")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "rights-item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hzx_icon1.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "商城折扣")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hzx_icon2.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "定期领券")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hzx_icon3.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "购物返现")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hzx_icon4.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "活动预告")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hzx_icon5.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "专属客服")
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "tile-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-desc"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "title"), "省钱套餐"),
                        createElementVNode("text", utsMapOf("class" to "desc"), "优惠商城")
                    )),
                    createElementVNode("view", utsMapOf("class" to "icon"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hyzx_sq.png", "class" to "image"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "item", "style" to normalizeStyle(utsMapOf("border-right" to "none"))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-desc"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "title"), "好物专享"),
                        createElementVNode("text", utsMapOf("class" to "desc"), "甄选好物")
                    )),
                    createElementVNode("view", utsMapOf("class" to "icon"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hyzx_hw.png", "class" to "image"))
                    ))
                ), 4),
                createElementVNode("view", utsMapOf("class" to "item", "style" to normalizeStyle(utsMapOf("border-bottom" to "none"))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-desc"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "title"), "福利中心"),
                        createElementVNode("text", utsMapOf("class" to "desc"), "每月福利")
                    )),
                    createElementVNode("view", utsMapOf("class" to "icon"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hyzx_fl.png", "class" to "image"))
                    ))
                ), 4),
                createElementVNode("view", utsMapOf("class" to "item", "style" to normalizeStyle(utsMapOf("border-right" to "none", "border-bottom" to "none"))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-desc"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "title"), "生日礼物"),
                        createElementVNode("text", utsMapOf("class" to "desc"), "专属红包")
                    )),
                    createElementVNode("view", utsMapOf("class" to "icon"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/hyzx_srlb.png", "class" to "image"))
                    ))
                ), 4)
            )),
            createElementVNode("view", utsMapOf("class" to "recommend-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "info-title"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "line")),
                        createElementVNode("text", utsMapOf("class" to "text"), "精选推荐")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.goodsList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                            )),
                            createElementVNode("view", utsMapOf("class" to "economize"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "预计可省20元")
                            )),
                            createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                    createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                } else {
                                    createCommentVNode("v-if", true);
                                }
                            ))
                        ));
                    }
                    ), 128)
                ))
            ))
        ), 44, utsArrayOf(
            "onScroll"
        ));
    }
    open var statusBarHeight: Number by `$data`;
    open var pageScrollTop: Number by `$data`;
    open var goodsList: UTSArray<goodsItems1> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "pageScrollTop" to 0, "goodsList" to goodsData as UTSArray<goodsItems1>);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.onPageScroll = fun(e: ScrollEvent) {
            var scrollTop: Number = e.detail.scrollTop;
            var top = scrollTop / 100;
            this.pageScrollTop = top;
            if (top >= 1) {
                this.pageScrollTop = 1;
            }
        }
        ;
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var onPageScroll: (e: ScrollEvent) -> Unit;
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "head-info" to padStyleMapOf(utsMapOf("width" to "100%", "backgroundImage" to "linear-gradient(to bottom, #fee9c4, #f8f8f8)")), "head-back" to utsMapOf(".head-info " to utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignContent" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "back" to utsMapOf(".head-info .head-back " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "image" to utsMapOf(".head-info .head-back .back " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".head-info .head-back .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".rights-interests .rights-item .item " to utsMapOf("width" to "70rpx", "height" to "70rpx", "marginBottom" to "10rpx"), ".tile-info .item .icon " to utsMapOf("width" to "68rpx", "height" to "72rpx"), ".recommend-info .goods-list .list .pic " to utsMapOf("width" to "334rpx", "height" to "334rpx", "borderTopLeftRadius" to "10rpx", "borderTopRightRadius" to "10rpx", "borderBottomRightRadius" to 0, "borderBottomLeftRadius" to 0)), "title" to utsMapOf(".head-info .head-back " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center"), ".head-info .card-info .card .title-btn .title-desc " to utsMapOf("fontSize" to "38rpx", "fontWeight" to "bold", "color" to "#ffffff"), ".rights-interests .title-more " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".tile-info .item .title-desc " to utsMapOf("fontSize" to "34rpx", "fontWeight" to "bold", "color" to "#333333"), ".recommend-info .info-title " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".recommend-info .goods-list .list " to utsMapOf("display" to "flex", "marginTop" to "10rpx")), "text" to utsMapOf(".head-info .head-back .title " to utsMapOf("fontSize" to "32rpx", "color" to "#333333"), ".head-info .card-info .card .title-btn .btn " to utsMapOf("fontSize" to "26rpx", "color" to "#fe9a5c"), ".rights-interests .title-more .title " to utsMapOf("fontWeight" to "bold", "fontSize" to "32rpx"), ".rights-interests .rights-item .item " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".recommend-info .info-title .title " to utsMapOf("fontWeight" to "bold", "fontSize" to "32rpx"), ".recommend-info .goods-list .list .economize " to utsMapOf("fontSize" to "24rpx", "color" to "#fe9a5c"), ".recommend-info .goods-list .list .title " to utsMapOf("lines" to 2, "textOverflow" to "ellipsis", "width" to "334rpx", "height" to "70rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "fontSize" to "28rpx", "color" to "#333333")), "more" to utsMapOf(".head-info .head-back " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "card-info" to utsMapOf(".head-info " to utsMapOf("position" to "relative", "marginLeft" to "30rpx", "marginRight" to "30rpx", "height" to "300rpx")), "bg" to utsMapOf(".head-info .card-info " to utsMapOf("width" to "100%", "height" to "300rpx")), "card" to utsMapOf(".head-info .card-info " to utsMapOf("position" to "absolute", "left" to 0, "top" to 0, "width" to "100%", "height" to "100%")), "title-btn" to utsMapOf(".head-info .card-info .card " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "20rpx")), "title-desc" to utsMapOf(".head-info .card-info .card .title-btn " to utsMapOf("display" to "flex", "flexDirection" to "column")), "desc" to utsMapOf(".head-info .card-info .card .title-btn .title-desc " to utsMapOf("marginTop" to "10rpx", "fontSize" to "24rpx", "color" to "#ffffff"), ".tile-info .item .title-desc " to utsMapOf("marginTop" to "10rpx", "fontSize" to "26rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "btn" to utsMapOf(".head-info .card-info .card .title-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "160rpx", "height" to "60rpx", "backgroundColor" to "#ffffff", "borderRadius" to "40rpx")), "economize-price" to utsMapOf(".head-info .card-info .card " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "60rpx")), "economize" to utsMapOf(".head-info .card-info .card .economize-price " to utsMapOf("fontSize" to "24rpx", "color" to "#ffffff"), ".recommend-info .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "price" to utsMapOf(".head-info .card-info .card .economize-price " to utsMapOf("fontSize" to "48rpx", "fontWeight" to "bold", "color" to "#ffffff"), ".recommend-info .goods-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "rights-interests" to padStyleMapOf(utsMapOf("marginLeft" to "30rpx", "marginRight" to "30rpx", "marginTop" to "30rpx", "height" to "240rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "title-more" to utsMapOf(".rights-interests " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "line" to utsMapOf(".rights-interests .title-more .title " to utsMapOf("width" to "12rpx", "height" to "30rpx", "marginRight" to "10rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "20rpx"), ".recommend-info .info-title .title " to utsMapOf("width" to "12rpx", "height" to "30rpx", "marginRight" to "10rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "20rpx")), "rights-item" to utsMapOf(".rights-interests " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "140rpx")), "item" to utsMapOf(".rights-interests .rights-item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "width" to "20%", "height" to "140rpx"), ".tile-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "45%", "height" to "140rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx", "borderRightWidth" to 1, "borderRightStyle" to "solid", "borderRightColor" to "#f8f8f8", "borderBottomWidth" to 1, "borderBottomStyle" to "solid", "borderBottomColor" to "#f8f8f8")), "tile-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "marginLeft" to "30rpx", "marginRight" to "30rpx", "marginTop" to "30rpx", "paddingTop" to "30rpx", "paddingRight" to "30rpx", "paddingBottom" to "30rpx", "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "icon" to utsMapOf(".tile-info .item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "recommend-info" to padStyleMapOf(utsMapOf("marginLeft" to "30rpx", "marginRight" to "30rpx", "marginTop" to "30rpx")), "info-title" to utsMapOf(".recommend-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx")), "goods-list" to utsMapOf(".recommend-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "justifyContent" to "space-between", "width" to "100%")), "list" to utsMapOf(".recommend-info .goods-list " to utsMapOf("width" to "334rpx", "marginBottom" to "20rpx", "paddingBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "10rpx")), "pic" to utsMapOf(".recommend-info .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "334rpx")), "min" to utsMapOf(".recommend-info .goods-list .list .price " to utsMapOf("fontSize" to "22rpx", "color" to "#ff6e65", "marginBottom" to "4rpx")), "max" to utsMapOf(".recommend-info .goods-list .list .price " to utsMapOf("fontSize" to "36rpx", "fontWeight" to "bold", "color" to "#ff6e65")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
