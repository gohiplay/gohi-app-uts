@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
import io.dcloud.uniapp.extapi.setStorageSync as uni_setStorageSync;
open class GenPagesSettingSetting : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {}
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "setting-item"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "账户安全")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "账户安全保证"),
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "more"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "支付设置")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "more"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "关于")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "more"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "隐私设置")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "more"))
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "login-out", "onClick" to _ctx.onLoginOut), utsArrayOf(
                createElementVNode("text", utsMapOf("class" to "text"), "退出登录")
            ), 8, utsArrayOf(
                "onClick"
            ))
        ), 4);
    }
    override fun `$initMethods`() {
        this.onLoginOut = fun() {
            uni_setStorageSync("isLogin", false);
            uni_navigateBack(null);
        }
        ;
    }
    open lateinit var onLoginOut: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "setting-item" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "item" to utsMapOf(".setting-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "80rpx", "borderBottomWidth" to 1, "borderBottomStyle" to "solid", "borderBottomColor" to "#f8f8f8")), "title" to utsMapOf(".setting-item .item " to utsMapOf("display" to "flex", "alignItems" to "center")), "text" to utsMapOf(".setting-item .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".setting-item .item .content " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".login-out " to utsMapOf("fontSize" to "29rpx", "color" to "#333333")), "content" to utsMapOf(".setting-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "more" to utsMapOf(".setting-item .item .content " to utsMapOf("width" to "38rpx", "height" to "38rpx")), "login-out" to padStyleMapOf(utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "100rpx", "marginTop" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
