@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenPagesAddressAddEditAddressAddEdit : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {}
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_switch = resolveComponent("switch");
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "address-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "input-text"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "姓名")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("input", utsMapOf("type" to "text", "placeholder" to "请输入姓名", "class" to "input"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "input-text"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "手机号")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("input", utsMapOf("type" to "tel", "maxlength" to 11, "placeholder" to "请输入手机号", "class" to "input"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "input-text"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "所在地区")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("input", utsMapOf("type" to "tel", "maxlength" to 11, "placeholder" to "请输入所在地区", "class" to "input"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "input-area-text"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "详细地址")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("textarea", utsMapOf("placeholder" to "请输入详细地址", "class" to "textarea"))
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "address-tag"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "tag-item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "标签")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "家")
                        )),
                        createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "公司")
                        )),
                        createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "学校")
                        ))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "default-address"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title-desc"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "title"), "设置默认地址"),
                        createElementVNode("text", utsMapOf("class" to "desc"), "提醒：下单会优先使用该地址")
                    )),
                    createElementVNode("view", utsMapOf("class" to "check"), utsArrayOf(
                        createVNode(_component_switch, utsMapOf("class" to "switch-checked", "checked" to _ctx.checked, "style" to normalizeStyle(utsMapOf("transform" to "scale(0.7)"))), null, 8, utsArrayOf(
                            "checked",
                            "style"
                        ))
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "footer-btn"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "text"), "保存")
                ))
            ))
        ), 4);
    }
    open var checked: Boolean by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("checked" to false);
    }
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "address-info" to padStyleMapOf(utsMapOf("marginTop" to "30rpx", "marginRight" to "30rpx", "marginBottom" to "30rpx", "marginLeft" to "30rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "input-text" to utsMapOf(".address-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "borderBottomWidth" to 1, "borderBottomStyle" to "solid", "borderBottomColor" to "#f8f8f8")), "title" to utsMapOf(".address-info .input-text " to utsMapOf("display" to "flex", "alignItems" to "center"), ".address-info .input-area-text " to utsMapOf("display" to "flex", "marginRight" to "20rpx"), ".address-tag .tag-item " to utsMapOf("display" to "flex", "width" to "200rpx"), ".address-tag .default-address .title-desc " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333", "marginBottom" to "20rpx")), "text" to utsMapOf(".address-info .input-text .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".address-info .input-area-text .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".address-tag .tag-item .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".address-tag .tag-item .item .tag " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".footer-btn .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "content" to utsMapOf(".address-info .input-text " to utsMapOf("display" to "flex", "alignItems" to "center"), ".address-info .input-area-text " to utsMapOf("flex" to 1, "height" to "100%")), "input" to utsMapOf(".address-info .input-text .content " to utsMapOf("width" to "320rpx", "fontSize" to "28rpx", "color" to "#333333", "textAlign" to "right")), "input-area-text" to utsMapOf(".address-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "justifyContent" to "space-between", "width" to "100%", "height" to "180rpx", "borderBottomWidth" to 1, "borderBottomStyle" to "solid", "borderBottomColor" to "#f8f8f8", "paddingTop" to "20rpx", "paddingRight" to 0, "paddingBottom" to "20rpx", "paddingLeft" to 0)), "textarea" to utsMapOf(".address-info .input-area-text .content " to utsMapOf("width" to "100%", "height" to "100%", "fontSize" to "28rpx", "color" to "#333333")), "address-tag" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginRight" to "30rpx", "marginBottom" to 0, "marginLeft" to "30rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "tag-item" to utsMapOf(".address-tag " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "borderBottomWidth" to 1, "borderBottomStyle" to "solid", "borderBottomColor" to "#f8f8f8")), "item" to utsMapOf(".address-tag .tag-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "alignItems" to "center", "flex" to 1, "height" to "100%")), "tag" to utsMapOf(".address-tag .tag-item .item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "120rpx", "height" to "50rpx", "marginRight" to "20rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#f8f8f8", "borderRadius" to "60rpx")), "default-address" to utsMapOf(".address-tag " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "140rpx")), "title-desc" to utsMapOf(".address-tag .default-address " to utsMapOf("flex" to 1)), "desc" to utsMapOf(".address-tag .default-address .title-desc " to utsMapOf("fontSize" to "24rpx", "color" to "#333333")), "check" to utsMapOf(".address-tag .default-address " to utsMapOf("display" to "flex", "alignItems" to "center")), "footer-btn" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "bottom" to 0, "zIndex" to 100, "display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "minHeight" to "120rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "btn" to utsMapOf(".footer-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "80rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "40rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
