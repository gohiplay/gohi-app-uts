@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenPagesMessageMessage : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {}
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "tab-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(1);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.messageCut === 1))
                    ))), "交易物流", 2),
                    withDirectives(createElementVNode("text", utsMapOf("class" to "active-line"), null, 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.messageCut === 1
                        )
                    ))
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "tab", "onClick" to fun(){
                    _ctx.onTab(2);
                }
                ), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "text",
                        utsMapOf("active" to (_ctx.messageCut === 2))
                    ))), "服务通知", 2),
                    withDirectives(createElementVNode("text", utsMapOf("class" to "active-line"), null, 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.messageCut === 2
                        )
                    ))
                ), 8, utsArrayOf(
                    "onClick"
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "message-info"), utsArrayOf(
                createElementVNode(Fragment, null, RenderHelpers.renderList(10, fun(item, index, _, _): VNode {
                    return createElementVNode("view", utsMapOf("class" to "message-list", "key" to index), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "date"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "2024年01月02日 19:2" + toDisplayString(item), 1)
                        )),
                        createElementVNode("view", utsMapOf("class" to "list"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "您的订单已完成")
                            )),
                            createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", "class" to "image"))
                                )),
                                createElementVNode("view", utsMapOf("class" to "message"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "订单[华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青]已完成，期待您的分享商品和购物心得")
                                ))
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "list"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "满意度评价")
                            )),
                            createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "message"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "您于12-29进行在线咨询，邀请您对我们的服务进行评价，一个月内完成问卷将有机会获得优惠券发送到您的账户中")
                                ))
                            ))
                        ))
                    ));
                }
                ), 64)
            ))
        ), 4);
    }
    open var messageCut: Number by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("messageCut" to 1);
    }
    override fun `$initMethods`() {
        this.onTab = fun(type: Number) {
            this.messageCut = type;
        }
        ;
    }
    open lateinit var onTab: (type: Number) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "tab-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff")), "tab" to utsMapOf(".tab-info " to utsMapOf("position" to "relative", "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "flex" to 1, "height" to "100%")), "text" to utsMapOf(".tab-info .tab " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".message-info .message-list .date " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "4rpx", "paddingRight" to "20rpx", "paddingBottom" to "4rpx", "paddingLeft" to "20rpx", "backgroundColor" to "rgba(0,0,0,0.3)", "borderRadius" to "40rpx", "fontSize" to "24rpx", "color" to "#ffffff"), ".message-info .message-list .list .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".message-info .message-list .list .info .message " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa", "lineHeight" to "40rpx")), "active" to utsMapOf(".tab-info .tab " to utsMapOf("fontSize" to "30rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "active-line" to utsMapOf(".tab-info .tab " to utsMapOf("position" to "absolute", "bottom" to "16rpx", "width" to "20%", "height" to "8rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "10rpx")), "message-info" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "120rpx")), "message-list" to utsMapOf(".message-info " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "date" to utsMapOf(".message-info .message-list " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginBottom" to "20rpx")), "list" to utsMapOf(".message-info .message-list " to utsMapOf("width" to "100%", "height" to "240rpx", "paddingTop" to "20rpx", "paddingRight" to "20rpx", "paddingBottom" to "20rpx", "paddingLeft" to "20rpx", "marginBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "10rpx")), "title" to utsMapOf(".message-info .message-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "60rpx")), "info" to utsMapOf(".message-info .message-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%")), "pic" to utsMapOf(".message-info .message-list .list .info " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "20rpx")), "image" to utsMapOf(".message-info .message-list .list .info .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx")), "message" to utsMapOf(".message-info .message-list .list .info " to utsMapOf("display" to "flex", "alignItems" to "center", "flex" to 1)));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
