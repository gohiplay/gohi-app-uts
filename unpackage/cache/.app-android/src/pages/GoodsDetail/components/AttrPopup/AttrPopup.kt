@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesGoodsDetailComponentsAttrPopupAttrPopup : VueComponent {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onMounted(fun() {
            val windowInfo = uni_getWindowInfo();
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_cat_popup = resolveComponent("cat-popup");
        return createVNode(_component_cat_popup, utsMapOf("ref" to "CatPopup", "mode" to "bottom"), utsMapOf("default" to withSlotCtx(fun(): UTSArray<Any> {
            return utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "attr-info", "style" to normalizeStyle(utsMapOf("paddingBottom" to ((_ctx.statusBarBottom + 20) + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "close", "onClick" to _ctx.close), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/close_icon.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "goods-data"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to _ctx.info.pic, "class" to "image"), null, 8, utsArrayOf(
                                "src"
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "data"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(_ctx.info.name), 1)
                            )),
                            createElementVNode("view", utsMapOf("class" to "attr"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "已选："),
                                createElementVNode("text", utsMapOf("class" to "text"), "颜色，白色")
                            )),
                            createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "selling-price"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                    createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.info.price), 1)
                                ))
                            ))
                        ))
                    )),
                    createElementVNode("scroll-view", utsMapOf("class" to "attr-select"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(6, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "attr-list", "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "attr-key"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "颜色" + toDisplayString(item), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "attr-value"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "item active"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "黑色")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "黑色")
                                    ))
                                ))
                            ));
                        }
                        ), 64)
                    )),
                    createElementVNode("view", utsMapOf("class" to "goods-number"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "数量")
                        )),
                        createElementVNode("view", utsMapOf("class" to "number"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "sub"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/sub_icon2.png", "class" to "image"))
                            )),
                            createElementVNode("view", utsMapOf("class" to "num"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "1")
                            )),
                            createElementVNode("view", utsMapOf("class" to "add"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/add_icon2.png", "class" to "image"))
                            ))
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "attr-btn"), utsArrayOf(
                        if (_ctx.btnType === "cart") {
                            createElementVNode("view", utsMapOf("key" to 0, "class" to "btn"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "加入购物车")
                            ));
                        } else {
                            createCommentVNode("v-if", true);
                        }
                        ,
                        if (_ctx.btnType === "buy") {
                            createElementVNode("view", utsMapOf("key" to 1, "class" to "btn", "onClick" to _ctx.onBuy), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "立即购买")
                            ), 8, utsArrayOf(
                                "onClick"
                            ));
                        } else {
                            createCommentVNode("v-if", true);
                        }
                    ))
                ), 4)
            );
        }
        ), "_" to 1), 512);
    }
    open var info: goodsItems1 by `$props`;
    open var statusBarBottom: Number by `$data`;
    open var btnType: String by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarBottom" to 0, "btnType" to "");
    }
    override fun `$initMethods`() {
        this.open = fun(type: String) {
            this.btnType = type;
            (this.`$refs`["CatPopup"] as ComponentPublicInstance).`$callMethod`("open");
        }
        ;
        this.close = fun() {
            (this.`$refs`["CatPopup"] as ComponentPublicInstance).`$callMethod`("close");
        }
        ;
        this.onBuy = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/ConfirmOrder/ConfirmOrder?goodsId=" + this.info.id));
        }
        ;
    }
    open lateinit var open: (type: String) -> Unit;
    open lateinit var close: () -> Unit;
    open lateinit var onBuy: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("attr-info" to padStyleMapOf(utsMapOf("width" to "750rpx", "backgroundColor" to "#ffffff", "borderTopLeftRadius" to "20rpx", "borderTopRightRadius" to "20rpx", "borderBottomRightRadius" to 0, "borderBottomLeftRadius" to 0, "paddingBottom" to "40rpx")), "close" to utsMapOf(".attr-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "flex-end", "width" to "100%", "height" to "80rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "image" to utsMapOf(".attr-info .close " to utsMapOf("width" to "32rpx", "height" to "32rpx"), ".attr-info .goods-data .pic " to utsMapOf("width" to "200rpx", "height" to "200rpx", "borderRadius" to "10rpx"), ".attr-info .goods-number .number .sub " to utsMapOf("width" to "28rpx", "height" to "28rpx"), ".attr-info .goods-number .number .add " to utsMapOf("width" to "28rpx", "height" to "28rpx")), "goods-data" to utsMapOf(".attr-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "200rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginBottom" to "20rpx")), "pic" to utsMapOf(".attr-info .goods-data " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "data" to utsMapOf(".attr-info .goods-data " to utsMapOf("flex" to 1, "height" to "100%")), "title" to utsMapOf(".attr-info .goods-data .data " to utsMapOf("width" to "100%"), ".attr-info .goods-number " to utsMapOf("display" to "flex", "alignItems" to "center")), "text" to utsMapOf(".attr-info .goods-data .data .title " to utsMapOf("width" to "100%", "height" to "80rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "28rpx", "lineHeight" to "40rpx", "color" to "#333333"), ".attr-info .goods-data .data .attr " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".attr-info .attr-select .attr-list .attr-key " to utsMapOf("fontWeight" to "bold", "fontSize" to "28rpx", "color" to "#333333"), ".attr-info .attr-select .attr-list .attr-value .item " to utsMapOf("fontSize" to "28rpx", "color" to "#777777"), ".attr-info .attr-select .attr-list .attr-value .active " to utsMapOf("color" to "#fe9a5c"), ".attr-info .goods-number .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".attr-info .goods-number .number .num " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".attr-info .attr-btn .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "attr" to utsMapOf(".attr-info .goods-data .data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "30rpx")), "price" to utsMapOf(".attr-info .goods-data .data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "selling-price" to utsMapOf(".attr-info .goods-data .data .price " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "min" to utsMapOf(".attr-info .goods-data .data .price .selling-price " to utsMapOf("fontWeight" to "bold", "fontSize" to "24rpx", "color" to "#ff6e65")), "max" to utsMapOf(".attr-info .goods-data .data .price .selling-price " to utsMapOf("fontWeight" to "bold", "fontSize" to "38rpx", "color" to "#ff6e65")), "attr-select" to utsMapOf(".attr-info " to utsMapOf("width" to "100%", "height" to "600rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "attr-list" to utsMapOf(".attr-info .attr-select " to utsMapOf("width" to "100%", "marginBottom" to "20rpx")), "attr-key" to utsMapOf(".attr-info .attr-select .attr-list " to utsMapOf("width" to "100%", "marginBottom" to "20rpx")), "attr-value" to utsMapOf(".attr-info .attr-select .attr-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "width" to "100%")), "item" to utsMapOf(".attr-info .attr-select .attr-list .attr-value " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "12rpx", "paddingRight" to "40rpx", "paddingBottom" to "12rpx", "paddingLeft" to "40rpx", "marginRight" to "20rpx", "marginBottom" to "20rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "rgba(0,0,0,0)", "backgroundColor" to "#f8f8f8", "borderRadius" to "80rpx")), "active" to utsMapOf(".attr-info .attr-select .attr-list .attr-value " to utsMapOf("backgroundColor" to "rgba(254,154,92,0.3)", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#fe9a5c")), "goods-number" to utsMapOf(".attr-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "number" to utsMapOf(".attr-info .goods-number " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "200rpx", "height" to "50rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#aaaaaa", "borderRadius" to "60rpx")), "sub" to utsMapOf(".attr-info .goods-number .number " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "60rpx", "height" to "100%", "borderRightWidth" to 1, "borderRightStyle" to "solid", "borderRightColor" to "#aaaaaa")), "add" to utsMapOf(".attr-info .goods-number .number " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "60rpx", "height" to "100%", "borderLeftWidth" to 1, "borderLeftStyle" to "solid", "borderLeftColor" to "#aaaaaa")), "num" to utsMapOf(".attr-info .goods-number .number " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "flex" to 1, "height" to "100%")), "attr-btn" to utsMapOf(".attr-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginTop" to "20rpx")), "btn" to utsMapOf(".attr-info .attr-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "90rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "45rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf("info" to utsMapOf("type" to "Object", "default" to UTSJSONObject())));
        var propsNeedCastKeys = utsArrayOf(
            "info"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf("CatPopup" to GenComponentsCatPopupCatPopupClass);
    }
}
