@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesGoodsDetailGoodsDetail : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onReady(fun() {
            this.shopInfoEl = this.`$refs`["shopInfo"] as Element;
            this.evaluateInfoEl = this.`$refs`["evaluateInfo"] as Element;
            this.goodsDetailEl = this.`$refs`["goodsDetail"] as Element;
        }
        , instance);
        onLoad(fun(options: OnLoadOptions) {
            val windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.screenWidth = windowInfo.screenWidth;
            val goodsId = options.get("goodsId") as String;
            this.goodsId = goodsId.toInt();
            this.getData();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        val _component_attr_popup = resolveComponent("attr-popup");
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1", "margin-bottom" to "60px")), "onScroll" to _ctx.onPageScroll, "scroll-top" to _ctx.scrollTop, "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "head-info-white", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px"), "opacity" to _ctx.pageScrollTop))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon1.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "head-item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onScrollView(0);
                        }
                        ), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("active" to (_ctx.headCut === 0))
                            ))), "商品", 2)
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onScrollView(1);
                        }
                        ), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("active" to (_ctx.headCut === 1))
                            ))), "店铺", 2)
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onScrollView(2);
                        }
                        ), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("active" to (_ctx.headCut === 2))
                            ))), "评价", 2)
                        ), 8, utsArrayOf(
                            "onClick"
                        )),
                        createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onScrollView(3);
                        }
                        ), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("active" to (_ctx.headCut === 3))
                            ))), "详情", 2)
                        ), 8, utsArrayOf(
                            "onClick"
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "operation-btn"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/fx_icon1.png", "class" to "image"))
                        )),
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_icon1.png", "class" to "image"))
                        ))
                    ))
                ), 4),
                createElementVNode("view", utsMapOf("class" to "head-info-transparen", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                    withDirectives(createElementVNode("view", utsMapOf("class" to "back"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon2.png", "class" to "image"))
                    ), 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.pageScrollTop <= 0
                        )
                    )),
                    withDirectives(createElementVNode("view", utsMapOf("class" to "operation-btn"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/fx_icon2.png", "class" to "image"))
                        )),
                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_icon2.png", "class" to "image"))
                        ))
                    ), 512), utsArrayOf(
                        utsArrayOf(
                            vShow,
                            _ctx.pageScrollTop <= 0
                        )
                    ))
                ), 4)
            )),
            createElementVNode("view", utsMapOf("class" to "banner-info", "style" to normalizeStyle(utsMapOf("height" to (_ctx.screenWidth + "px")))), utsArrayOf(
                createElementVNode("swiper", utsMapOf("class" to "swiper", "autoplay" to true, "circular" to true, "indicator-dots" to false, "onChange" to _ctx.changeBanner), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(3, fun(item, index, _, _): VNode {
                        return createElementVNode("swiper-item", utsMapOf("item-id" to index), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to _ctx.info.pic, "class" to "image"), toDisplayString(item), 9, utsArrayOf(
                                "src"
                            ))
                        ), 8, utsArrayOf(
                            "item-id"
                        ));
                    }
                    ), 64)
                ), 40, utsArrayOf(
                    "onChange"
                )),
                createElementVNode("view", utsMapOf("class" to "indicator-dot"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(3, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                            "dots",
                            utsMapOf("active" to (_ctx.bannerCurrent === index))
                        )), "data-item" to item, "key" to index), null, 10, utsArrayOf(
                            "data-item"
                        ));
                    }
                    ), 64)
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "goods-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                        createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.info.price), 1)
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "goods-name"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(_ctx.info.name), 1)
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "goods-tags"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/dht_icon.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "超时赔付")
                    )),
                    createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/dht_icon.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "无忧退款")
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "goods-attr-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "选择")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "请选择规格属性")
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "发货")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "浙江杭州 免邮费 付款后3天内发货")
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "参数")
                    )),
                    createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "品牌 材质")
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "evaluate-info", "ref" to "evaluateInfo"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "evaluate-title", "onClick" to _ctx.onEvaluate), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "评价"),
                        createElementVNode("text", utsMapOf("class" to "num"), "(10万+)")
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                    ))
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "evaluate-item"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(3, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "item", "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "user-star"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/ni_user_pic.png", "class" to "image"))
                                )),
                                createElementVNode("view", utsMapOf("class" to "name-star"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "哇**哈" + toDisplayString(item), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "star"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_off.png", "class" to "image"))
                                    ))
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "evaluate-content"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "货很好，是正版，用着非常好丝滑，遥遥领先货很好，是正版，用着非常好丝滑，遥遥领先货很好，是正版，用着非常好丝滑，遥遥领先货很好，是正版，用着非常好丝滑，遥遥领先")
                                )),
                                createElementVNode("view", utsMapOf("class" to "img-item"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "img"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to _ctx.info.pic, "class" to "image"), null, 8, utsArrayOf(
                                            "src"
                                        ))
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "img"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to _ctx.info.pic, "class" to "image"), null, 8, utsArrayOf(
                                            "src"
                                        ))
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "img"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to _ctx.info.pic, "class" to "image"), null, 8, utsArrayOf(
                                            "src"
                                        ))
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "attr"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "黑色,帆布空心")
                                ))
                            ))
                        ));
                    }
                    ), 64)
                ))
            ), 512),
            createElementVNode("view", utsMapOf("class" to "shop-info", "ref" to "shopInfo", "onClick" to _ctx.onToShop), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "shop-logo"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/shop_default.png", "class" to "image"))
                )),
                createElementVNode("view", utsMapOf("class" to "shop-data"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "shop-name"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(_ctx.info.shopName), 1)
                    )),
                    createElementVNode("view", utsMapOf("class" to "star-info"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "star"), utsArrayOf(
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_off.png", "class" to "image")),
                            createElementVNode("text", utsMapOf("class" to "text"), "4.0")
                        )),
                        createElementVNode("view", utsMapOf("class" to "tags"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "自营")
                            ))
                        ))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "shop-more"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                ))
            ), 8, utsArrayOf(
                "onClick"
            )),
            createElementVNode("view", utsMapOf("class" to "goods-param"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "line")),
                        createElementVNode("text", utsMapOf("class" to "tit"), "商品参数")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "param-item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "规格")
                        )),
                        createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "大型")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "适用对象")
                        )),
                        createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "成人")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "材质")
                        )),
                        createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "棉花")
                        ))
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "goods-detail", "ref" to "goodsDetail"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "line")),
                        createElementVNode("text", utsMapOf("class" to "tit"), "图文信息")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to _ctx.info.pic), null, 8, utsArrayOf(
                        "src"
                    ))
                ))
            ), 512),
            createElementVNode("view", utsMapOf("class" to "footer-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "footer-btn"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "btn", "onClick" to _ctx.onToShop), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/dpt_icon.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "店铺")
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/kft_icon.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "客服")
                    )),
                    createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/sct_off.png", "class" to "image")),
                        createElementVNode("text", utsMapOf("class" to "text"), "收藏")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "buy-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "add-cart-btn", "onClick" to fun(){
                        _ctx.onAddBuy("cart");
                    }
                    ), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "加入购物车")
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "buy-btn", "onClick" to fun(){
                        _ctx.onAddBuy("buy");
                    }
                    ), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "立即购买")
                    ), 8, utsArrayOf(
                        "onClick"
                    ))
                ))
            )),
            createVNode(_component_attr_popup, utsMapOf("ref" to "AttrPopup", "info" to _ctx.info), null, 8, utsArrayOf(
                "info"
            ))
        ), 44, utsArrayOf(
            "onScroll",
            "scroll-top"
        ));
    }
    open var statusBarHeight: Number by `$data`;
    open var screenWidth: Number by `$data`;
    open var bannerCurrent: Number by `$data`;
    open var pageScrollTop: Number by `$data`;
    open var scrollTop: Number by `$data`;
    open var headCut: Number by `$data`;
    open var shopInfoEl: Element? by `$data`;
    open var evaluateInfoEl: Element? by `$data`;
    open var goodsDetailEl: Element? by `$data`;
    open var goodsId: Number by `$data`;
    open var info: goodsItems1 by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "screenWidth" to 0, "bannerCurrent" to 0, "pageScrollTop" to 0, "scrollTop" to 0, "headCut" to 0, "shopInfoEl" to null as Element?, "evaluateInfoEl" to null as Element?, "goodsDetailEl" to null as Element?, "goodsId" to 0, "info" to goodsItems1());
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.getData = fun() {
            goodsData.forEach(fun(item){
                if (item.id === this.goodsId) {
                    this.info = item;
                }
            }
            );
        }
        ;
        this.onPageScroll = fun(e: ScrollEvent) {
            var scrollTop: Number = e.detail.scrollTop;
            var top = scrollTop / 100;
            var statusBarHeight: Number = this.statusBarHeight + 50;
            var shopTop: Number = this.shopInfoEl?.offsetTop as Number - statusBarHeight;
            var evaluateTop: Number = this.evaluateInfoEl?.offsetTop as Number - statusBarHeight;
            var goodsDetailTop: Number = this.goodsDetailEl?.offsetTop as Number - statusBarHeight;
            this.pageScrollTop = top;
            if (top >= 1) {
                this.pageScrollTop = 1;
            }
            if (scrollTop >= shopTop) {
                this.headCut = 1;
            } else if (scrollTop >= evaluateTop) {
                this.headCut = 2;
            } else if (scrollTop >= goodsDetailTop) {
                this.headCut = 3;
            } else if (scrollTop <= 10) {
                this.headCut = 0;
            }
        }
        ;
        this.onScrollView = fun(type: Number) {
            this.headCut = type;
            var statusBarHeight: Number = this.statusBarHeight + 50;
            var shopTop: Number = this.shopInfoEl?.offsetTop as Number - statusBarHeight;
            var evaluateTop: Number = this.evaluateInfoEl?.offsetTop as Number - statusBarHeight;
            var goodsDetailTop: Number = this.goodsDetailEl?.offsetTop as Number - statusBarHeight;
            var scrollTop: Number = 0;
            when (type) {
                0 -> 
                    {
                        scrollTop = 0;
                        this.pageScrollTop = 0;
                    }
                1 -> 
                    scrollTop = shopTop;
                2 -> 
                    scrollTop = evaluateTop;
                3 -> 
                    scrollTop = goodsDetailTop;
            }
            setTimeout(fun(){
                this.scrollTop = scrollTop;
            }
            , 100);
        }
        ;
        this.changeBanner = fun(e: SwiperChangeEvent) {
            var index = e.detail.current;
            this.bannerCurrent = index;
        }
        ;
        this.onAddBuy = fun(type: String) {
            (this.`$refs`["AttrPopup"] as ComponentPublicInstance).`$callMethod`("open", type);
            console.log(type, " at pages/GoodsDetail/GoodsDetail.uvue:418");
        }
        ;
        this.onToShop = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/ShopDetails/ShopDetails?name=" + this.info.shopName));
        }
        ;
        this.onEvaluate = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsEvaluate/GoodsEvaluate"));
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var getData: () -> Unit;
    open lateinit var onPageScroll: (e: ScrollEvent) -> Unit;
    open lateinit var onScrollView: (type: Number) -> Unit;
    open lateinit var changeBanner: (e: SwiperChangeEvent) -> Unit;
    open lateinit var onAddBuy: (type: String) -> Unit;
    open lateinit var onToShop: () -> Unit;
    open lateinit var onEvaluate: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "head-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "width" to "100%")), "head-info-white" to utsMapOf(".head-info " to utsMapOf("position" to "absolute", "zIndex" to 12, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "back" to utsMapOf(".head-info .head-info-white " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center"), ".head-info .head-info-transparen " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "backgroundColor" to "rgba(0,0,0,0.5)", "borderRadius" to "80rpx")), "image" to utsMapOf(".head-info .head-info-white .back " to utsMapOf("width" to "58rpx", "height" to "58rpx"), ".head-info .head-info-white .operation-btn .btn " to utsMapOf("width" to "54rpx", "height" to "54rpx"), ".head-info .head-info-transparen .back " to utsMapOf("width" to "58rpx", "height" to "58rpx"), ".head-info .head-info-transparen .operation-btn .btn " to utsMapOf("width" to "54rpx", "height" to "54rpx"), ".banner-info .swiper " to utsMapOf("width" to "100%", "height" to "100%"), ".goods-info .goods-tags .tag " to utsMapOf("width" to "28rpx", "height" to "28rpx", "marginRight" to "10rpx"), ".goods-attr-info .item .more " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".evaluate-info .evaluate-title .more " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".evaluate-info .evaluate-item .item .user-star .pic " to utsMapOf("width" to "60rpx", "height" to "60rpx", "borderRadius" to "30rpx"), ".evaluate-info .evaluate-item .item .user-star .name-star .star " to utsMapOf("width" to "24rpx", "height" to "24rpx", "marginRight" to "4rpx"), ".evaluate-info .evaluate-item .item .evaluate-content .img-item .img " to utsMapOf("width" to "180rpx", "height" to "180rpx"), ".shop-info .shop-logo " to utsMapOf("width" to "120rpx", "height" to "120rpx", "borderRadius" to "60rpx"), ".shop-info .shop-data .star-info .star " to utsMapOf("width" to "28rpx", "height" to "28rpx", "marginRight" to "4rpx"), ".shop-info .shop-more " to utsMapOf("width" to "42rpx", "height" to "42rpx"), ".goods-param .title-info .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".goods-detail .title-info .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".footer-info .footer-btn .btn " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginBottom" to "6rpx")), "head-item" to utsMapOf(".head-info .head-info-white " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1, "height" to "100%")), "item" to utsMapOf(".head-info .head-info-white .head-item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "25%"), ".goods-attr-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx"), ".evaluate-info .evaluate-item " to utsMapOf("width" to "100%", "marginBottom" to "20rpx"), ".goods-param .param-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "60rpx")), "text" to utsMapOf(".head-info .head-info-white .head-item .item " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".goods-info .goods-name .name " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#333333"), ".goods-info .goods-tags .tag " to utsMapOf("fontSize" to "24rpx", "color" to "#777777"), ".goods-attr-info .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#777777"), ".goods-attr-info .item .content " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".evaluate-info .evaluate-title .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".evaluate-info .evaluate-item .item .user-star .name-star .name " to utsMapOf("fontSize" to "24rpx", "color" to "#333333"), ".evaluate-info .evaluate-item .item .evaluate-content .content " to utsMapOf("width" to "100%", "height" to "60rpx", "fontSize" to "24rpx", "color" to "#333333", "lines" to 2, "textOverflow" to "ellipsis"), ".evaluate-info .evaluate-item .item .evaluate-content .attr " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".shop-info .shop-data .shop-name " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".shop-info .shop-data .star-info .star " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".shop-info .shop-data .star-info .tags .tag " to utsMapOf("fontSize" to "20rpx", "color" to "#ffffff"), ".goods-param .param-item .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#777777"), ".goods-param .param-item .item .content " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".footer-info .footer-btn .btn " to utsMapOf("fontSize" to "24rpx", "color" to "#333333"), ".footer-info .buy-info .add-cart-btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff"), ".footer-info .buy-info .buy-btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "active" to utsMapOf(".head-info .head-info-white .head-item .item " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".banner-info .indicator-dot " to utsMapOf("width" to "34rpx", "height" to "16rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "16rpx")), "operation-btn" to utsMapOf(".head-info .head-info-white " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".head-info .head-info-transparen " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "btn" to utsMapOf(".head-info .head-info-white .operation-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginLeft" to "20rpx"), ".head-info .head-info-transparen .operation-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginLeft" to "20rpx", "paddingTop" to "4rpx", "paddingRight" to "4rpx", "paddingBottom" to "4rpx", "paddingLeft" to "4rpx", "backgroundColor" to "rgba(0,0,0,0.5)", "borderRadius" to "80rpx"), ".footer-info .footer-btn " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "33%", "height" to "100%")), "head-info-transparen" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "rgba(0,0,0,0)")), "banner-info" to padStyleMapOf(utsMapOf("position" to "relative", "zIndex" to 10, "width" to "100%", "height" to "750rpx", "backgroundColor" to "#ffffff")), "swiper" to utsMapOf(".banner-info " to utsMapOf("width" to "100%", "height" to "100%")), "indicator-dot" to utsMapOf(".banner-info " to utsMapOf("position" to "absolute", "left" to 0, "bottom" to "20rpx", "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "100%")), "dots" to utsMapOf(".banner-info .indicator-dot " to utsMapOf("width" to "16rpx", "height" to "16rpx", "backgroundColor" to "rgba(254,154,92,0.3)", "borderRadius" to "10rpx", "marginTop" to 0, "marginRight" to "6rpx", "marginBottom" to 0, "marginLeft" to "6rpx")), "goods-info" to padStyleMapOf(utsMapOf("marginTop" to "20rpx", "marginRight" to "30rpx", "marginBottom" to "20rpx", "marginLeft" to "30rpx", "paddingTop" to "20rpx", "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "price-info" to utsMapOf(".goods-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginBottom" to "10rpx")), "price" to utsMapOf(".goods-info .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "min" to utsMapOf(".goods-info .price-info .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#fe9a5c", "marginBottom" to "4rpx")), "max" to utsMapOf(".goods-info .price-info .price " to utsMapOf("fontSize" to "38rpx", "fontWeight" to "bold", "color" to "#fe9a5c")), "goods-name" to utsMapOf(".goods-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "width" to "100%", "marginBottom" to "10rpx")), "name" to utsMapOf(".goods-info .goods-name " to utsMapOf("width" to "100%"), ".evaluate-info .evaluate-item .item .user-star .name-star " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "goods-tags" to utsMapOf(".goods-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginTop" to "20rpx")), "tag" to utsMapOf(".goods-info .goods-tags " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginRight" to "20rpx"), ".shop-info .shop-data .star-info .tags " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "4rpx", "paddingRight" to "10rpx", "paddingBottom" to "4rpx", "paddingLeft" to "10rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "4rpx")), "goods-attr-info" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginRight" to "30rpx", "marginBottom" to "20rpx", "marginLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "title" to utsMapOf(".goods-attr-info .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100rpx"), ".evaluate-info .evaluate-title " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".goods-param .title-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".goods-param .param-item .item " to utsMapOf("flexDirection" to "row", "display" to "flex", "alignItems" to "center", "width" to "120rpx"), ".goods-detail .title-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "content" to utsMapOf(".goods-attr-info .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1), ".evaluate-info .evaluate-item .item .evaluate-content " to utsMapOf("flex" to 1, "marginBottom" to "10rpx"), ".goods-param .param-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1)), "more" to utsMapOf(".goods-attr-info .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".evaluate-info .evaluate-title " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".goods-param .title-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".goods-detail .title-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "evaluate-info" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginRight" to "30rpx", "marginBottom" to "20rpx", "marginLeft" to "30rpx", "paddingTop" to "20rpx", "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "evaluate-title" to utsMapOf(".evaluate-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "marginBottom" to "20rpx")), "num" to utsMapOf(".evaluate-info .evaluate-title .title " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa")), "evaluate-item" to utsMapOf(".evaluate-info " to utsMapOf("width" to "100%")), "user-star" to utsMapOf(".evaluate-info .evaluate-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "70rpx")), "pic" to utsMapOf(".evaluate-info .evaluate-item .item .user-star " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "star" to utsMapOf(".evaluate-info .evaluate-item .item .user-star .name-star " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center"), ".shop-info .shop-data .star-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "evaluate-content" to utsMapOf(".evaluate-info .evaluate-item .item " to utsMapOf("width" to "100%", "marginTop" to "10rpx")), "img-item" to utsMapOf(".evaluate-info .evaluate-item .item .evaluate-content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "200rpx", "marginBottom" to "10rpx")), "img" to utsMapOf(".evaluate-info .evaluate-item .item .evaluate-content .img-item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "attr" to utsMapOf(".evaluate-info .evaluate-item .item .evaluate-content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "10rpx")), "shop-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "height" to "200rpx", "marginTop" to 0, "marginRight" to "30rpx", "marginBottom" to "20rpx", "marginLeft" to "30rpx", "paddingTop" to "20rpx", "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "shop-logo" to utsMapOf(".shop-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "shop-data" to utsMapOf(".shop-info " to utsMapOf("flex" to 1)), "shop-name" to utsMapOf(".shop-info .shop-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "10rpx")), "star-info" to utsMapOf(".shop-info .shop-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%")), "tags" to utsMapOf(".shop-info .shop-data .star-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginLeft" to "20rpx")), "shop-more" to utsMapOf(".shop-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "goods-param" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginRight" to "30rpx", "marginBottom" to "20rpx", "marginLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "title-info" to utsMapOf(".goods-param " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx"), ".goods-detail " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "20rpx", "paddingBottom" to 0, "paddingLeft" to "20rpx")), "line" to utsMapOf(".goods-param .title-info .title " to utsMapOf("width" to "14rpx", "height" to "30rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "10rpx", "marginRight" to "10rpx"), ".goods-detail .title-info .title " to utsMapOf("width" to "14rpx", "height" to "30rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "10rpx", "marginRight" to "10rpx")), "tit" to utsMapOf(".goods-param .title-info .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".goods-detail .title-info .title " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333")), "param-item" to utsMapOf(".goods-param " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "goods-detail" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginRight" to "30rpx", "marginBottom" to "20rpx", "marginLeft" to "30rpx", "paddingBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "footer-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "bottom" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "120rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "boxSizing" to "border-box", "backgroundColor" to "#ffffff")), "footer-btn" to utsMapOf(".footer-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "35%", "height" to "100%")), "buy-info" to utsMapOf(".footer-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "55%", "height" to "100%")), "add-cart-btn" to utsMapOf(".footer-info .buy-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "48%", "height" to "70rpx", "backgroundColor" to "#ffdb53", "borderRadius" to "45rpx")), "buy-btn" to utsMapOf(".footer-info .buy-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "48%", "height" to "70rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "45rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf("AttrPopup" to GenPagesGoodsDetailComponentsAttrPopupAttrPopupClass);
    }
}
