@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesSearchSearch : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1"))), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "search-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "search"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/fdj_icon.png", "class" to "image")),
                        createElementVNode("input", utsMapOf("type" to "text", "placeholder" to "请输入搜索内容", "modelValue" to _ctx.keyword, "onInput" to fun(`$event`: InputEvent){
                            _ctx.keyword = `$event`.detail.value;
                        }
                        , "class" to "input"), null, 40, utsArrayOf(
                            "modelValue",
                            "onInput"
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "btn", "onClick" to _ctx.onSearch), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "搜索")
                    ), 8, utsArrayOf(
                        "onClick"
                    ))
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "search-history"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "text"), "搜索历史")
                )),
                createElementVNode("view", utsMapOf("class" to "keyword-item"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.historyList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onKeyword(item);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item), 1)
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "search-find"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "title-info"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "text"), "搜索发现")
                )),
                createElementVNode("view", utsMapOf("class" to "search-hot"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "今日热搜")
                    )),
                    createElementVNode("view", utsMapOf("class" to "hot-item"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.hotKeywordList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                                _ctx.onKeyword(item.keyword);
                            }
                            , "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "key"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.sort) + ".", 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "keyword"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.keyword), 1)
                                ))
                            ), 8, utsArrayOf(
                                "onClick"
                            ));
                        }
                        ), 128)
                    ))
                ))
            ))
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var keyword: String by `$data`;
    open var historyList: UTSArray<String> by `$data`;
    open var hotKeywordList: UTSArray<hotKeywordItem> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "keyword" to "", "historyList" to utsArrayOf(
            "华为手机"
        ), "hotKeywordList" to utsArrayOf<hotKeywordItem>(hotKeywordItem(keyword = "空气炸锅", sort = 1), hotKeywordItem(keyword = "录音笔", sort = 2), hotKeywordItem(keyword = "打印机", sort = 3), hotKeywordItem(keyword = "华为手机", sort = 4), hotKeywordItem(keyword = "华为Mate 60 Pro", sort = 5)));
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.onSearch = fun() {
            if (this.keyword === "") {
                toast("请输入搜索内容");
                return;
            }
            uni_navigateTo(NavigateToOptions(url = "/pages/SearchGoods/SearchGoods?keyword=" + this.keyword));
        }
        ;
        this.onKeyword = fun(value: String) {
            uni_navigateTo(NavigateToOptions(url = "/pages/SearchGoods/SearchGoods?keyword=" + value));
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var onSearch: () -> Unit;
    open lateinit var onKeyword: (value: String) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("head-info" to padStyleMapOf(utsMapOf("width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff")), "search-info" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "back" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100rpx", "height" to "100%")), "image" to utsMapOf(".head-info .search-info .back " to utsMapOf("width" to "58rpx", "height" to "58rpx"), ".head-info .search-info .search " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "10rpx")), "search" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "flex" to 1, "height" to "60rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "marginRight" to "20rpx", "backgroundColor" to "#f8f8f8", "borderRadius" to "30rpx")), "input" to utsMapOf(".head-info .search-info .search " to utsMapOf("flex" to 1, "height" to "100%", "fontSize" to "26rpx", "color" to "#333333")), "btn" to utsMapOf(".head-info .search-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100rpx", "height" to "50rpx", "backgroundImage" to "linear-gradient(to right, #fb9e6d, #ff6e37)", "borderRadius" to "30rpx")), "text" to utsMapOf(".head-info .search-info .btn " to utsMapOf("fontSize" to "26rpx", "color" to "#ffffff"), ".title-info " to utsMapOf("fontSize" to "32rpx", "color" to "#333333"), ".search-history .keyword-item .item " to utsMapOf("fontSize" to "24rpx", "color" to "#333333"), ".search-find .search-hot .title " to utsMapOf("fontSize" to "32rpx", "fontWeight" to "bold", "color" to "#fe9a5c"), ".search-find .search-hot .hot-item .item .key " to utsMapOf("fontWeight" to "bold", "fontSize" to "28rpx", "color" to "#333333"), ".search-find .search-hot .hot-item .item .keyword " to utsMapOf("fontSize" to "28rpx", "color" to "#333333")), "title-info" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "20rpx")), "search-history" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to "20rpx", "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx")), "keyword-item" to utsMapOf(".search-history " to utsMapOf("display" to "flex", "flexDirection" to "row", "flexWrap" to "wrap", "width" to "100%")), "item" to utsMapOf(".search-history .keyword-item " to utsMapOf("paddingTop" to "10rpx", "paddingRight" to "20rpx", "paddingBottom" to "10rpx", "paddingLeft" to "20rpx", "marginBottom" to "20rpx", "marginRight" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "40rpx"), ".search-find .search-hot .hot-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "80rpx")), "search-find" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "20rpx", "paddingTop" to "20rpx", "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx")), "search-hot" to utsMapOf(".search-find " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#ffffff", "borderRadius" to "20rpx")), "title" to utsMapOf(".search-find .search-hot " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginTop" to "20rpx")), "hot-item" to utsMapOf(".search-find .search-hot " to utsMapOf("width" to "100%")), "key" to utsMapOf(".search-find .search-hot .hot-item .item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "keyword" to utsMapOf(".search-find .search-hot .hot-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
