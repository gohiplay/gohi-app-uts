@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.previewImage as uni_previewImage;
open class GenPagesGoodsEvaluateGoodsEvaluate : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {}
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "evaluate-tab"), utsArrayOf(
                createElementVNode("scroll-view", utsMapOf("scroll-x" to true, "class" to "tab-item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item active"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text active-text"), "全部")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "有图(37)")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "追加(100)")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "好评(99)")
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "差评(9)")
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "evaluate-item"), utsArrayOf(
                createElementVNode(Fragment, null, RenderHelpers.renderList(3, fun(item, index, _, _): VNode {
                    return createElementVNode("view", utsMapOf("class" to "item", "key" to index), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "user-star"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/ni_user_pic.png", "class" to "image"))
                            )),
                            createElementVNode("view", utsMapOf("class" to "name-star"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "哇**哈" + toDisplayString(item), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "star"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_on.png", "class" to "image")),
                                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/star_off.png", "class" to "image"))
                                ))
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "evaluate-content"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "货很好，是正版，用着非常好丝滑，遥遥领先货很好，是正版，用着非常好丝滑，遥遥领先货很好，是正版，用着非常好丝滑，遥遥领先货很好，是正版，用着非常好丝滑，遥遥领先")
                            )),
                            createElementVNode("view", utsMapOf("class" to "img-item", "onClick" to _ctx.previewImage), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "img"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", "class" to "image"))
                                )),
                                createElementVNode("view", utsMapOf("class" to "img"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", "class" to "image"))
                                )),
                                createElementVNode("view", utsMapOf("class" to "img"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", "class" to "image"))
                                )),
                                createElementVNode("view", utsMapOf("class" to "img"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", "class" to "image"))
                                ))
                            ), 8, utsArrayOf(
                                "onClick"
                            )),
                            createElementVNode("view", utsMapOf("class" to "attr"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "黑色,帆布空心")
                            ))
                        ))
                    ));
                }
                ), 64)
            ))
        ), 4);
    }
    override fun `$initMethods`() {
        this.previewImage = fun() {
            uni_previewImage(PreviewImageOptions(urls = utsArrayOf(
                "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp"
            ), current = 0));
        }
        ;
    }
    open lateinit var previewImage: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "evaluate-tab" to padStyleMapOf(utsMapOf("position" to "fixed", "top" to 0, "left" to 0, "display" to "flex", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff")), "tab-item" to utsMapOf(".evaluate-tab " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "item" to utsMapOf(".evaluate-tab .tab-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "10rpx", "paddingRight" to "20rpx", "paddingBottom" to "10rpx", "paddingLeft" to "20rpx", "marginRight" to "20rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#aaaaaa", "borderRadius" to "50rpx"), ".evaluate-item " to utsMapOf("width" to "100%", "paddingTop" to "20rpx", "paddingRight" to "20rpx", "paddingBottom" to "20rpx", "paddingLeft" to "20rpx", "marginBottom" to "20rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "text" to utsMapOf(".evaluate-tab .tab-item .item " to utsMapOf("fontSize" to "24rpx", "color" to "#333333"), ".evaluate-item .item .user-star .name-star .name " to utsMapOf("fontSize" to "24rpx", "color" to "#333333"), ".evaluate-item .item .evaluate-content .content " to utsMapOf("width" to "100%", "fontSize" to "24rpx", "color" to "#333333", "lineHeight" to "40rpx"), ".evaluate-item .item .evaluate-content .attr " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa")), "active-text" to utsMapOf(".evaluate-tab .tab-item .item " to utsMapOf("color" to "#fe9a5c")), "active" to utsMapOf(".evaluate-tab .tab-item " to utsMapOf("borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#fe9a5c")), "evaluate-item" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "100rpx", "paddingTop" to "20rpx", "paddingRight" to "30rpx", "paddingBottom" to "20rpx", "paddingLeft" to "30rpx")), "user-star" to utsMapOf(".evaluate-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "70rpx")), "pic" to utsMapOf(".evaluate-item .item .user-star " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginRight" to "20rpx")), "image" to utsMapOf(".evaluate-item .item .user-star .pic " to utsMapOf("width" to "60rpx", "height" to "60rpx", "borderRadius" to "30rpx"), ".evaluate-item .item .user-star .name-star .star " to utsMapOf("width" to "24rpx", "height" to "24rpx", "marginRight" to "4rpx"), ".evaluate-item .item .evaluate-content .img-item .img " to utsMapOf("width" to "314rpx", "height" to "314rpx", "borderRadius" to "10rpx")), "name" to utsMapOf(".evaluate-item .item .user-star .name-star " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "star" to utsMapOf(".evaluate-item .item .user-star .name-star " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "evaluate-content" to utsMapOf(".evaluate-item .item " to utsMapOf("width" to "100%", "marginTop" to "10rpx")), "content" to utsMapOf(".evaluate-item .item .evaluate-content " to utsMapOf("width" to "100%", "marginBottom" to "10rpx")), "img-item" to utsMapOf(".evaluate-item .item .evaluate-content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "flexWrap" to "wrap", "width" to "100%", "marginBottom" to "10rpx")), "img" to utsMapOf(".evaluate-item .item .evaluate-content .img-item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "marginBottom" to "20rpx")), "attr" to utsMapOf(".evaluate-item .item .evaluate-content " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "10rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
