@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenPagesAfterSaleAfterSale : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {}
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "tab-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "item active"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "text"), "售后申请")
                )),
                createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "text"), "申请记录")
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "order-list"), utsArrayOf(
                createElementVNode(Fragment, null, RenderHelpers.renderList(4, fun(item, index, _, _): VNode {
                    return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "shop-status"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "shop"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/shop_icon.png", "class" to "icon")),
                                createElementVNode("text", utsMapOf("class" to "text"), "哇哈哈店铺" + toDisplayString(item), 1)
                            )),
                            createElementVNode("view", utsMapOf("class" to "status"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "red-status"), "已申请")
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "goods-item"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "goods"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                        createElementVNode("image", utsMapOf("src" to "/static/images/goods_pic.png", "class" to "image"))
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "商品名称")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "price-number"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                            createElementVNode("text", utsMapOf("class" to "max"), "99"),
                                            createElementVNode("text", utsMapOf("class" to "min"), ".00")
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "number"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "共1件")
                                        ))
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "btn-info"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "btn active"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "申请售后")
                                    ))
                                ))
                            ))
                        ))
                    ));
                }
                ), 64)
            ))
        ), 4);
    }
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "tab-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx", "backgroundColor" to "#ffffff")), "item" to utsMapOf(".tab-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "50%", "height" to "100%", "borderBottomWidth" to 2, "borderBottomStyle" to "solid", "borderBottomColor" to "rgba(0,0,0,0)"), ".order-list .list .goods-item " to utsMapOf("width" to "100%")), "text" to utsMapOf(".tab-info .item " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".tab-info .active " to utsMapOf("fontWeight" to "bold", "color" to "#fe9a5c"), ".order-list .list .shop-status .shop " to utsMapOf("fontSize" to "26rpx", "color" to "#333333"), ".order-list .list .shop-status .status " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".order-list .list .goods-item .item .goods .name " to utsMapOf("width" to "100%", "height" to "80rpx", "lineHeight" to "40rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#333333"), ".order-list .list .goods-item .item .goods .price-number .number " to utsMapOf("fontSize" to "22rpx", "color" to "#333333"), ".order-list .list .goods-item .item .btn-info .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-list .list .goods-item .item .btn-info .active " to utsMapOf("color" to "#fe9a5c")), "active" to utsMapOf(".tab-info " to utsMapOf("borderBottomWidth" to 2, "borderBottomStyle" to "solid", "borderBottomColor" to "#fe9a5c"), ".order-list .list .goods-item .item .btn-info " to utsMapOf("borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#fe9a5c")), "order-list" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to "120rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "boxSizing" to "border-box")), "list" to utsMapOf(".order-list " to utsMapOf("width" to "100%", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx", "marginBottom" to "20rpx")), "shop-status" to utsMapOf(".order-list .list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "80rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "borderBottomWidth" to 1, "borderBottomStyle" to "solid", "borderBottomColor" to "#f8f8f8")), "shop" to utsMapOf(".order-list .list .shop-status " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "icon" to utsMapOf(".order-list .list .shop-status .shop " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "10rpx")), "status" to utsMapOf(".order-list .list .shop-status " to utsMapOf("display" to "flex", "alignItems" to "center")), "success" to utsMapOf(".order-list .list .shop-status .status " to utsMapOf("fontSize" to "26rpx", "color" to "#fe9a5c")), "green-status" to utsMapOf(".order-list .list .shop-status .status " to utsMapOf("fontSize" to "26rpx", "color" to "#158c15")), "red-status" to utsMapOf(".order-list .list .shop-status .status " to utsMapOf("fontSize" to "26rpx", "color" to "#FF0000")), "goods-item" to utsMapOf(".order-list .list " to utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "goods" to utsMapOf(".order-list .list .goods-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "200rpx")), "pic" to utsMapOf(".order-list .list .goods-item .item .goods " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "20rpx")), "image" to utsMapOf(".order-list .list .goods-item .item .goods .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx")), "name" to utsMapOf(".order-list .list .goods-item .item .goods " to utsMapOf("flex" to 1)), "price-number" to utsMapOf(".order-list .list .goods-item .item .goods " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "marginLeft" to "10rpx")), "price" to utsMapOf(".order-list .list .goods-item .item .goods .price-number " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "marginBottom" to "10rpx")), "min" to utsMapOf(".order-list .list .goods-item .item .goods .price-number .price " to utsMapOf("fontSize" to "22rpx", "fontWeight" to "bold", "color" to "#333333")), "max" to utsMapOf(".order-list .list .goods-item .item .goods .price-number .price " to utsMapOf("fontWeight" to "bold", "fontSize" to "34rpx", "color" to "#333333")), "number" to utsMapOf(".order-list .list .goods-item .item .goods .price-number " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "btn-info" to utsMapOf(".order-list .list .goods-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "flex-end", "width" to "100%", "height" to "100rpx")), "btn" to utsMapOf(".order-list .list .goods-item .item .btn-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "160rpx", "height" to "60rpx", "marginLeft" to "20rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#333333", "borderRadius" to "60rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
