@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesAddressListAddressList : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            val windowInfo = uni_getWindowInfo();
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("style" to normalizeStyle(utsMapOf("paddingBottom" to ((_ctx.statusBarBottom + 60) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "address-info"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(3, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "item", "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "address-data"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "address-city"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "浙江杭州市滨江区")
                                )),
                                createElementVNode("view", utsMapOf("class" to "address"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), "长河街道华洲中心中南一街123号")
                                )),
                                createElementVNode("view", utsMapOf("class" to "name-phone"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "哇哈哈" + toDisplayString(item), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "name"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "17888888888")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "tag-item"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "默认")
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "tag blue"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "公司")
                                        ))
                                    ))
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                            ))
                        ));
                    }
                    ), 64)
                )),
                createElementVNode("view", utsMapOf("class" to "footer-btn", "style" to normalizeStyle(utsMapOf("paddingBottom" to (_ctx.statusBarBottom + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "btn", "onClick" to _ctx.onAdd), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "新建地址")
                    ), 8, utsArrayOf(
                        "onClick"
                    ))
                ), 4)
            ), 4)
        ), 4);
    }
    open var statusBarBottom: Number by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarBottom" to 0);
    }
    override fun `$initMethods`() {
        this.onAdd = fun() {
            uni_navigateTo(NavigateToOptions(url = "/pages/AddressAddEdit/AddressAddEdit"));
        }
        ;
    }
    open lateinit var onAdd: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "address-info" to padStyleMapOf(utsMapOf("marginTop" to "30rpx", "marginRight" to "30rpx", "marginBottom" to 0, "marginLeft" to "30rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "item" to utsMapOf(".address-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "paddingTop" to "30rpx", "paddingRight" to 0, "paddingBottom" to "30rpx", "paddingLeft" to 0, "borderBottomWidth" to 1, "borderBottomStyle" to "solid", "borderBottomColor" to "#f8f8f8", "borderBottomWidth:last-child" to "medium", "borderBottomStyle:last-child" to "none", "borderBottomColor:last-child" to "#000000")), "address-data" to utsMapOf(".address-info .item " to utsMapOf("flex" to 1)), "address-city" to utsMapOf(".address-info .item .address-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginBottom" to "10rpx")), "text" to utsMapOf(".address-info .item .address-data .address-city " to utsMapOf("fontSize" to "28rpx", "color" to "#aaaaaa"), ".address-info .item .address-data .address " to utsMapOf("fontSize" to "38rpx", "fontWeight" to "bold", "color" to "#333333"), ".address-info .item .address-data .name-phone .name " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".address-info .item .address-data .name-phone .tag-item .tag " to utsMapOf("fontSize" to "20rpx", "color" to "#ffffff"), ".footer-btn .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "address" to utsMapOf(".address-info .item .address-data " to utsMapOf("width" to "100%", "marginBottom" to "10rpx")), "name-phone" to utsMapOf(".address-info .item .address-data " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%")), "name" to utsMapOf(".address-info .item .address-data .name-phone " to utsMapOf("display" to "flex", "marginRight" to "10rpx")), "tag-item" to utsMapOf(".address-info .item .address-data .name-phone " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "tag" to utsMapOf(".address-info .item .address-data .name-phone .tag-item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "2rpx", "paddingRight" to "10rpx", "paddingBottom" to "2rpx", "paddingLeft" to "10rpx", "marginRight" to "10rpx", "backgroundColor" to "#ff6e65", "borderRadius" to "4rpx")), "blue" to utsMapOf(".address-info .item .address-data .name-phone .tag-item " to utsMapOf("backgroundColor" to "#ffdb53")), "more" to utsMapOf(".address-info .item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "image" to utsMapOf(".address-info .item .more " to utsMapOf("width" to "38rpx", "height" to "38rpx")), "footer-btn" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "bottom" to 0, "zIndex" to 100, "display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "minHeight" to "120rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "btn" to utsMapOf(".footer-btn " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "80rpx", "backgroundColor" to "#fe9a5c", "borderRadius" to "40rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
