@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
import io.dcloud.uniapp.extapi.navigateTo as uni_navigateTo;
open class GenPagesSeckillSeckill : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(_: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "onScroll" to _ctx.onPageScroll, "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "head-back", "style" to normalizeStyle(utsMapOf("height" to ((_ctx.statusBarHeight + 50) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px"), "backgroundColor" to ("rgba(231,45,30," + _ctx.pageScrollTop + ")")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon2.png", "class" to "image"))
                    ), 8, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/xsms_tit.png", "class" to "image"))
                    )),
                    createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_icon2.png", "class" to "image"))
                    ))
                ), 4),
                createElementVNode("view", utsMapOf("class" to "head-banner", "style" to normalizeStyle(utsMapOf("marginTop" to ((_ctx.statusBarHeight + 60) + "px")))), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "banner"), utsArrayOf(
                        createElementVNode("swiper", utsMapOf("class" to "swiper", "autoplay" to false, "circular" to true, "indicator-dots" to true, "indicator-active-color" to "#ffffff", "indicator-color" to "rgba(255,255,255,0.5)"), utsArrayOf(
                            createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.bannerList, fun(item, index, _, _): VNode {
                                return createElementVNode("swiper-item", utsMapOf("key" to index), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.pic, "mode" to "aspectFit", "class" to "image"), null, 8, utsArrayOf(
                                        "src"
                                    ))
                                ));
                            }
                            ), 128)
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "brand-item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", "class" to "image"))
                            )),
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "华为专区")
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", "class" to "image"))
                            )),
                            createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "荣耀专区")
                            ))
                        ))
                    ))
                ), 4)
            )),
            createElementVNode("view", utsMapOf("class" to "session-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "session-item"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "item",
                        utsMapOf("active" to (_ctx.sessionCut === 0))
                    )), "onClick" to fun(){
                        _ctx.onSessionCut(0);
                    }
                    ), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "time"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("text-active" to (_ctx.sessionCut === 0))
                            ))), "18:00", 2)
                        )),
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("text-active" to (_ctx.sessionCut === 0))
                            ))), "已开抢", 2)
                        ))
                    ), 10, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "item",
                        utsMapOf("active" to (_ctx.sessionCut === 1))
                    )), "onClick" to fun(){
                        _ctx.onSessionCut(1);
                    }
                    ), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "time"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("text-active" to (_ctx.sessionCut === 1))
                            ))), "20:00", 2)
                        )),
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("text-active" to (_ctx.sessionCut === 1))
                            ))), "抢购中", 2)
                        ))
                    ), 10, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "item",
                        utsMapOf("active" to (_ctx.sessionCut === 2))
                    )), "onClick" to fun(){
                        _ctx.onSessionCut(2);
                    }
                    ), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "time"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("text-active" to (_ctx.sessionCut === 2))
                            ))), "22:00", 2)
                        )),
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("text-active" to (_ctx.sessionCut === 2))
                            ))), "即将开抢", 2)
                        ))
                    ), 10, utsArrayOf(
                        "onClick"
                    )),
                    createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                        "item",
                        utsMapOf("active" to (_ctx.sessionCut === 3))
                    )), "onClick" to fun(){
                        _ctx.onSessionCut(3);
                    }
                    ), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "time"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("text-active" to (_ctx.sessionCut === 3))
                            ))), "00:00", 2)
                        )),
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to normalizeClass(utsArrayOf(
                                "text",
                                utsMapOf("text-active" to (_ctx.sessionCut === 3))
                            ))), "即将开抢", 2)
                        ))
                    ), 10, utsArrayOf(
                        "onClick"
                    ))
                ))
            )),
            createElementVNode("view", utsMapOf("class" to "seckill-goods"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "goods-item"), utsArrayOf(
                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.seckillList, fun(item, index, _, _): VNode {
                        return createElementVNode("view", utsMapOf("class" to "item", "onClick" to fun(){
                            _ctx.onGoodsItem(item);
                        }
                        , "key" to index), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                    "src"
                                ))
                            )),
                            createElementVNode("view", utsMapOf("class" to "info"), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "goods-name"), utsArrayOf(
                                    createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(item.name), 1)
                                )),
                                createElementVNode("view", utsMapOf("class" to "progress"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "bar", "style" to normalizeStyle(utsMapOf("width" to (_ctx.calculateNumber(item.inventory as Number, item.salesVolume as Number, 1) + "%")))), utsArrayOf(
                                        if (_ctx.calculateNumber(item.inventory as Number, item.salesVolume as Number, 1) >= 40) {
                                            createElementVNode("text", utsMapOf("key" to 0, "class" to "text"), "剩余" + toDisplayString(_ctx.calculateNumber(item.inventory as Number, item.salesVolume as Number, 0)) + "件商品", 1);
                                        } else {
                                            createCommentVNode("v-if", true);
                                        }
                                    ), 4)
                                )),
                                createElementVNode("view", utsMapOf("class" to "price-btn"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "price-info"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                                            createElementVNode("text", utsMapOf("class" to "max"), toDisplayString(_ctx.formatPrice(item.price as Number, 0)), 1),
                                            if (isTrue(_ctx.formatPrice(item.price as Number, 1))) {
                                                createElementVNode("text", utsMapOf("key" to 0, "class" to "min"), "." + toDisplayString(_ctx.formatPrice(item.price as Number, 1)), 1);
                                            } else {
                                                createCommentVNode("v-if", true);
                                            }
                                        )),
                                        createElementVNode("view", utsMapOf("class" to "old-price"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "￥" + toDisplayString(item.originalPrice), 1)
                                        ))
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "btn-info"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "抢购")
                                        ))
                                    ))
                                ))
                            ))
                        ), 8, utsArrayOf(
                            "onClick"
                        ));
                    }
                    ), 128)
                ))
            ))
        ), 44, utsArrayOf(
            "onScroll"
        ));
    }
    open var statusBarHeight: Number by `$data`;
    open var pageScrollTop: Number by `$data`;
    open var sessionCut: Number by `$data`;
    open var seckillList: UTSArray<goodsItems1> by `$data`;
    open var bannerList: UTSArray<bannerItem1> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "pageScrollTop" to 0, "sessionCut" to 0, "seckillList" to seckillGoodsData as UTSArray<goodsItems1>, "bannerList" to BannerData as UTSArray<bannerItem1>);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.onPageScroll = fun(e: ScrollEvent) {
            var scrollTop: Number = e.detail.scrollTop;
            var top = scrollTop / 100;
            this.pageScrollTop = top;
            if (top >= 1) {
                this.pageScrollTop = 1;
            }
        }
        ;
        this.calculateNumber = fun(count: Number, useCount: Number, type: Number): Number {
            var sum = count - useCount;
            var bar = (useCount / count) * 100;
            return if (type === 0) {
                sum;
            } else {
                Math.round(bar * 100) / 100;
            }
            ;
        }
        ;
        this.formatPrice = fun(price: Number, type: Number): String {
            var strPrice = price.toString();
            var start: String = strPrice.split(".")[0];
            var end: String = "";
            try {
                end = strPrice.split(".")[1];
            }
             catch (e: Throwable) {
                end = "";
            }
            return if (type === 0) {
                start;
            } else {
                end;
            }
            ;
        }
        ;
        this.onGoodsItem = fun(item: goodsItems1) {
            uni_navigateTo(NavigateToOptions(url = "/pages/GoodsDetail/GoodsDetail?goodsId=" + item.id));
        }
        ;
        this.onSessionCut = fun(type: Number) {
            this.sessionCut = type;
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var onPageScroll: (e: ScrollEvent) -> Unit;
    open lateinit var calculateNumber: (count: Number, useCount: Number, type: Number) -> Number;
    open lateinit var formatPrice: (price: Number, type: Number) -> String;
    open lateinit var onGoodsItem: (item: goodsItems1) -> Unit;
    open lateinit var onSessionCut: (type: Number) -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#ffffff")), "head-info" to padStyleMapOf(utsMapOf("width" to "100%", "height" to "400rpx", "backgroundImage" to "linear-gradient(to bottom, #e72d1e, #ffffff)")), "head-back" to utsMapOf(".head-info " to utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "back" to utsMapOf(".head-info .head-back " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "image" to utsMapOf(".head-info .head-back .back " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".head-info .head-back .title " to utsMapOf("width" to "288rpx", "height" to "58rpx"), ".head-info .head-back .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".head-info .head-banner .banner .swiper " to utsMapOf("width" to "100%", "height" to "100%", "borderRadius" to "20rpx"), ".head-info .head-banner .brand-item .item .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx", "borderRadius" to "30rpx"), ".seckill-goods .goods-item .item .pic " to utsMapOf("width" to "220rpx", "height" to "220rpx", "borderRadius" to "10rpx")), "title" to utsMapOf(".head-info .head-back " to utsMapOf("display" to "flex", "alignItems" to "center"), ".head-info .head-banner .brand-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "height" to "40rpx"), ".session-info .session-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center")), "more" to utsMapOf(".head-info .head-back " to utsMapOf("display" to "flex", "alignItems" to "center")), "head-banner" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "180rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "banner" to utsMapOf(".head-info .head-banner " to utsMapOf("width" to "340rpx", "height" to "180rpx")), "swiper" to utsMapOf(".head-info .head-banner .banner " to utsMapOf("width" to "340rpx", "height" to "180rpx")), "brand-item" to utsMapOf(".head-info .head-banner " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginLeft" to "20rpx", "flex" to 1)), "item" to utsMapOf(".head-info .head-banner .brand-item " to utsMapOf("width" to "140rpx", "height" to "180rpx", "backgroundColor" to "#ff7d65", "borderRadius" to "30rpx", "marginRight" to "20rpx"), ".session-info .session-item " to utsMapOf("display" to "flex", "flexDirection" to "column", "alignItems" to "center", "justifyContent" to "center", "width" to "170rpx", "height" to "120rpx", "boxShadow" to "0 0 20rpx rgba(0, 0, 0, 0)", "borderRadius" to "20rpx"), ".seckill-goods .goods-item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "paddingTop" to "30rpx", "paddingRight" to "30rpx", "paddingBottom" to "30rpx", "paddingLeft" to "30rpx", "marginBottom" to "30rpx", "backgroundColor" to "#ffffff", "boxShadow" to "0 0 30rpx rgba(0, 0, 0, 0.1)", "borderRadius" to "30rpx")), "pic" to utsMapOf(".head-info .head-banner .brand-item .item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "140rpx", "height" to "140rpx", "backgroundColor" to "#ffffff", "borderRadius" to "30rpx"), ".seckill-goods .goods-item .item " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "20rpx")), "text" to utsMapOf(".head-info .head-banner .brand-item .item .title " to utsMapOf("fontSize" to "24rpx", "color" to "#ffffff"), ".session-info .session-item .item .time " to utsMapOf("fontSize" to "38rpx", "fontWeight" to "bold", "color" to "#333333"), ".session-info .session-item .item .title " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#333333"), ".seckill-goods .goods-item .item .info .goods-name " to utsMapOf("width" to "100%", "height" to "80rpx", "lineHeight" to "40rpx", "lines" to 2, "textOverflow" to "ellipsis", "fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".seckill-goods .goods-item .item .info .progress .bar " to utsMapOf("fontSize" to "22rpx", "color" to "#ffffff"), ".seckill-goods .goods-item .item .info .price-btn .price-info .old-price " to utsMapOf("fontSize" to "24rpx", "color" to "#aaaaaa", "textDecorationLine" to "line-through"), ".seckill-goods .goods-item .item .info .price-btn .btn-info .btn " to utsMapOf("fontSize" to "32rpx", "color" to "#ffffff")), "session-info" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx")), "session-item" to utsMapOf(".session-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "140rpx", "paddingTop" to 0, "paddingRight" to "10rpx", "paddingBottom" to 0, "paddingLeft" to "10rpx")), "time" to utsMapOf(".session-info .session-item .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center")), "text-active" to utsMapOf(".session-info .session-item .item .time " to utsMapOf("color" to "#e72d1e"), ".session-info .session-item .item .title " to utsMapOf("color" to "#e72d1e")), "active" to utsMapOf(".session-info .session-item " to utsMapOf("backgroundColor" to "#ffffff", "boxShadow" to "0 0 20rpx rgba(0, 0, 0, 0.1)", "borderRadius" to "20rpx")), "seckill-goods" to padStyleMapOf(utsMapOf("width" to "100%")), "goods-item" to utsMapOf(".seckill-goods " to utsMapOf("width" to "100%", "paddingTop" to "30rpx", "paddingRight" to "30rpx", "paddingBottom" to "30rpx", "paddingLeft" to "30rpx")), "info" to utsMapOf(".seckill-goods .goods-item .item " to utsMapOf("flex" to 1, "height" to "100%")), "goods-name" to utsMapOf(".seckill-goods .goods-item .item .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "marginBottom" to "20rpx")), "progress" to utsMapOf(".seckill-goods .goods-item .item .info " to utsMapOf("width" to "100%", "height" to "30rpx", "backgroundColor" to "#fbeae5", "borderRadius" to "30rpx", "overflow" to "hidden")), "bar" to utsMapOf(".seckill-goods .goods-item .item .info .progress " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "50%", "height" to "100%", "backgroundColor" to "#ff583c", "borderRadius" to "30rpx")), "price-btn" to utsMapOf(".seckill-goods .goods-item .item .info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "marginTop" to "40rpx")), "price-info" to utsMapOf(".seckill-goods .goods-item .item .info .price-btn " to utsMapOf("display" to "flex", "flexDirection" to "row")), "price" to utsMapOf(".seckill-goods .goods-item .item .info .price-btn .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "min" to utsMapOf(".seckill-goods .goods-item .item .info .price-btn .price-info .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#ff583c", "marginBottom" to "4rpx")), "max" to utsMapOf(".seckill-goods .goods-item .item .info .price-btn .price-info .price " to utsMapOf("fontSize" to "42rpx", "fontWeight" to "bold", "color" to "#ff583c")), "old-price" to utsMapOf(".seckill-goods .goods-item .item .info .price-btn .price-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "marginLeft" to "10rpx", "marginBottom" to "4rpx")), "btn-info" to utsMapOf(".seckill-goods .goods-item .item .info .price-btn " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "btn" to utsMapOf(".seckill-goods .goods-item .item .info .price-btn .btn-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "120rpx", "height" to "60rpx", "backgroundImage" to "linear-gradient(to right, #fd975a, #ff4d29)", "borderRadius" to "40rpx")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
