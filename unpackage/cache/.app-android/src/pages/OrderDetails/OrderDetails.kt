@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getWindowInfo as uni_getWindowInfo;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
open class GenPagesOrderDetailsOrderDetails : BasePage {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLoad(fun(options: OnLoadOptions) {
            var windowInfo = uni_getWindowInfo();
            this.statusBarHeight = windowInfo.statusBarHeight;
            this.statusBarBottom = windowInfo.safeAreaInsets.bottom;
            val orderId = options.get("orderId") as String;
            this.orderId = orderId.toInt();
            this.getData();
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("scroll-view", utsMapOf("style" to normalizeStyle(utsMapOf("flex" to "1")), "class" to "page"), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to "head-info", "style" to normalizeStyle(utsMapOf("height" to ((50 + _ctx.statusBarHeight) + "px"), "paddingTop" to (_ctx.statusBarHeight + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "back", "onClick" to _ctx.onBack), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/back_icon.png", "class" to "image"))
                ), 8, utsArrayOf(
                    "onClick"
                )),
                createElementVNode("view", utsMapOf("class" to "order-status"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "status"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/order_df.png", "class" to "image")),
                        if (_ctx.info.status === 0) {
                            createElementVNode("text", utsMapOf("key" to 0, "class" to "text"), "待付款");
                        } else {
                            if (_ctx.info.status === 1) {
                                createElementVNode("text", utsMapOf("key" to 1, "class" to "text"), "待发货");
                            } else {
                                if (_ctx.info.status === 2) {
                                    createElementVNode("text", utsMapOf("key" to 2, "class" to "text"), "待收货");
                                } else {
                                    if (_ctx.info.status === 3) {
                                        createElementVNode("text", utsMapOf("key" to 3, "class" to "text"), "已完成");
                                    } else {
                                        if (_ctx.info.status === 4) {
                                            createElementVNode("text", utsMapOf("key" to 4, "class" to "text"), "已取消");
                                        } else {
                                            createCommentVNode("v-if", true);
                                        }
                                        ;
                                    }
                                    ;
                                }
                                ;
                            }
                            ;
                        }
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                    createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_icon1.png", "class" to "image"))
                ))
            ), 4),
            createElementVNode("view", utsMapOf("style" to normalizeStyle(utsMapOf("paddingBottom" to ((_ctx.statusBarBottom + 50) + "px"), "marginTop" to ((_ctx.statusBarHeight + 60) + "px")))), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "address-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "name-phone"), utsArrayOf(
                        createElementVNode("image", utsMapOf("src" to "/static/images/icon/dw_icon.png", "class" to "icon")),
                        createElementVNode("text", utsMapOf("class" to "name"), "哇哈哈"),
                        createElementVNode("text", utsMapOf("class" to "phone"), "1788****8888")
                    )),
                    createElementVNode("view", utsMapOf("class" to "address"), utsArrayOf(
                        createElementVNode("text", utsMapOf("class" to "text"), "浙江省杭州市滨江区江安单号9999999号")
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "shop-goods"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "shop-info"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "tag-item"), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "tag"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text"), "自营")
                            ))
                        )),
                        createElementVNode("view", utsMapOf("class" to "shop-name"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(_ctx.info.shopName), 1),
                            createElementVNode("image", utsMapOf("src" to "/static/images/icon/more_right.png", "class" to "image"))
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "goods-list"), utsArrayOf(
                        createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.info.goodsList, fun(item, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "list", "key" to index), utsArrayOf(
                                createElementVNode("view", utsMapOf("class" to "pic"), utsArrayOf(
                                    createElementVNode("image", utsMapOf("src" to item.pic, "class" to "image"), null, 8, utsArrayOf(
                                        "src"
                                    ))
                                )),
                                createElementVNode("view", utsMapOf("class" to "goods"), utsArrayOf(
                                    createElementVNode("view", utsMapOf("class" to "name-price"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "name"), toDisplayString(item.name), 1),
                                        createElementVNode("text", utsMapOf("class" to "price"), "￥" + toDisplayString(item.price), 1)
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "attr-number"), utsArrayOf(
                                        createElementVNode("text", utsMapOf("class" to "text"), "颜色,黑色"),
                                        createElementVNode("text", utsMapOf("class" to "text"), "x1")
                                    )),
                                    createElementVNode("view", utsMapOf("class" to "btn-info"), utsArrayOf(
                                        createElementVNode("view", utsMapOf("class" to "btn"), utsArrayOf(
                                            createElementVNode("text", utsMapOf("class" to "text"), "申请售后")
                                        ))
                                    ))
                                ))
                            ));
                        }
                        ), 128)
                    )),
                    createElementVNode("view", utsMapOf("class" to "shop-service"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "service"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "联系客服")
                        ))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "order-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "商品总价")
                        )),
                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "￥99.00")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "运费")
                        )),
                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "￥0")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "title"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "优惠券")
                        )),
                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text ac"), "-￥10")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "total-price"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "sub-price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "共减"),
                            createElementVNode("text", utsMapOf("class" to "text"), "￥10")
                        )),
                        createElementVNode("view", utsMapOf("class" to "price"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "tit"), "实付款："),
                            createElementVNode("text", utsMapOf("class" to "min"), "￥"),
                            createElementVNode("text", utsMapOf("class" to "max"), "99"),
                            createElementVNode("text", utsMapOf("class" to "min"), ".00")
                        ))
                    ))
                )),
                createElementVNode("view", utsMapOf("class" to "order-info"), utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "assist"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "订单编号")
                        )),
                        createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), toDisplayString(_ctx.info.id), 1)
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "assist"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "支付方式")
                        )),
                        createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "微信支付")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "assist"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "下单时间")
                        )),
                        createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "2023-12-12 12:12:12")
                        ))
                    )),
                    createElementVNode("view", utsMapOf("class" to "item"), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "assist"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "配送方式")
                        )),
                        createElementVNode("view", utsMapOf("class" to "content"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "快递")
                        ))
                    ))
                ))
            ), 4),
            createElementVNode("view", utsMapOf("class" to "footer-info"), utsArrayOf(
                createElementVNode("view", utsMapOf("class" to "more"), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "text"), "更多")
                )),
                createElementVNode("view", utsMapOf("class" to "order-btn"), utsArrayOf(
                    if (isTrue(_ctx.info.status >= 2 && _ctx.info.status < 4)) {
                        createElementVNode("view", utsMapOf("key" to 0, "class" to "btn"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "查看物流")
                        ));
                    } else {
                        createCommentVNode("v-if", true);
                    }
                    ,
                    if (_ctx.info.status === 2) {
                        createElementVNode("view", utsMapOf("key" to 1, "class" to "btn active"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "确认收货")
                        ));
                    } else {
                        createCommentVNode("v-if", true);
                    }
                    ,
                    if (_ctx.info.status === 3) {
                        createElementVNode("view", utsMapOf("key" to 2, "class" to "btn active"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "评价")
                        ));
                    } else {
                        createCommentVNode("v-if", true);
                    }
                    ,
                    if (_ctx.info.status === 0) {
                        createElementVNode("view", utsMapOf("key" to 3, "class" to "btn active"), utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "text"), "继续付款")
                        ));
                    } else {
                        createCommentVNode("v-if", true);
                    }
                ))
            ))
        ), 4);
    }
    open var statusBarHeight: Number by `$data`;
    open var statusBarBottom: Number by `$data`;
    open var orderId: Number by `$data`;
    open var info: OrderItem by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("statusBarHeight" to 0, "statusBarBottom" to 0, "orderId" to 0, "info" to OrderData[0] as OrderItem);
    }
    override fun `$initMethods`() {
        this.onBack = fun() {
            uni_navigateBack(null);
        }
        ;
        this.getData = fun() {
            OrderData.forEach(fun(item){
                if (item.id === this.orderId) {
                    this.info = item;
                }
            }
            );
        }
        ;
    }
    open lateinit var onBack: () -> Unit;
    open lateinit var getData: () -> Unit;
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ), utsArrayOf(
                    GenApp.styles
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f8f8")), "head-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "top" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#f8f8f8")), "back" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "image" to utsMapOf(".head-info .back " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".head-info .order-status .status " to utsMapOf("width" to "38rpx", "height" to "38rpx", "marginRight" to "10rpx"), ".head-info .more " to utsMapOf("width" to "48rpx", "height" to "48rpx"), ".shop-goods .shop-info .shop-name " to utsMapOf("width" to "38rpx", "height" to "38rpx"), ".shop-goods .goods-list .list .pic " to utsMapOf("width" to "140rpx", "height" to "140rpx")), "order-status" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "status" to utsMapOf(".head-info .order-status " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "text" to utsMapOf(".head-info .order-status .status " to utsMapOf("fontSize" to "38rpx", "fontWeight" to "bold", "color" to "#333333"), ".address-info .address " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".shop-goods .shop-info .tag-item .tag " to utsMapOf("fontSize" to "22rpx", "color" to "#ffffff"), ".shop-goods .shop-info .shop-name " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".shop-goods .goods-list .list .goods .attr-number " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa"), ".shop-goods .goods-list .list .goods .btn-info .btn " to utsMapOf("fontSize" to "24rpx", "color" to "#333333"), ".shop-goods .shop-service .service " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-info .item .title " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-info .item .assist " to utsMapOf("fontSize" to "28rpx", "color" to "#aaaaaa"), ".order-info .item .price " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".order-info .item .content " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".order-info .total-price .sub-price " to utsMapOf("fontSize" to "26rpx", "fontWeight" to "bold", "color" to "#FF0000"), ".footer-info .more " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".footer-info .order-btn .btn " to utsMapOf("fontSize" to "28rpx", "color" to "#333333"), ".footer-info .order-btn .active " to utsMapOf("fontSize" to "28rpx", "color" to "#ffffff")), "more" to utsMapOf(".head-info " to utsMapOf("display" to "flex", "alignItems" to "center"), ".footer-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "address-info" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginRight" to "30rpx", "marginBottom" to "30rpx", "marginLeft" to "30rpx", "paddingTop" to "30rpx", "paddingRight" to "30rpx", "paddingBottom" to "30rpx", "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "name-phone" to utsMapOf(".address-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginBottom" to "10rpx")), "icon" to utsMapOf(".address-info .name-phone " to utsMapOf("width" to "28rpx", "height" to "28rpx", "marginRight" to "10rpx")), "name" to utsMapOf(".address-info .name-phone " to utsMapOf("fontWeight" to "bold", "fontSize" to "28rpx", "color" to "#333333", "marginRight" to "10rpx"), ".shop-goods .goods-list .list .goods .name-price " to utsMapOf("flex" to 1, "height" to "40rpx", "whiteSpace" to "nowrap", "textOverflow" to "ellipsis", "fontSize" to "28rpx", "color" to "#333333")), "phone" to utsMapOf(".address-info .name-phone " to utsMapOf("fontSize" to "26rpx", "color" to "#aaaaaa")), "address" to utsMapOf(".address-info " to utsMapOf("display" to "flex", "flexDirection" to "row")), "shop-goods" to padStyleMapOf(utsMapOf("marginLeft" to "30rpx", "marginRight" to "30rpx", "marginBottom" to "20rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "shop-info" to utsMapOf(".shop-goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "100rpx")), "tag-item" to utsMapOf(".shop-goods .shop-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "marginRight" to "10rpx")), "tag" to utsMapOf(".shop-goods .shop-info .tag-item " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "paddingTop" to "2rpx", "paddingRight" to "10rpx", "paddingBottom" to "2rpx", "paddingLeft" to "10rpx", "marginTop" to 2, "backgroundColor" to "#fe9a5c", "borderRadius" to "6rpx")), "shop-name" to utsMapOf(".shop-goods .shop-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "goods-list" to utsMapOf(".shop-goods " to utsMapOf("width" to "100%")), "list" to utsMapOf(".shop-goods .goods-list " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "width" to "100%", "height" to "140rpx", "marginBottom" to "20rpx")), "pic" to utsMapOf(".shop-goods .goods-list .list " to utsMapOf("display" to "flex", "alignItems" to "center", "marginRight" to "20rpx")), "goods" to utsMapOf(".shop-goods .goods-list .list " to utsMapOf("flex" to 1, "height" to "100%")), "name-price" to utsMapOf(".shop-goods .goods-list .list .goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "price" to utsMapOf(".shop-goods .goods-list .list .goods .name-price " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#333333"), ".order-info .item " to utsMapOf("display" to "flex", "alignItems" to "center"), ".order-info .total-price " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end")), "attr-number" to utsMapOf(".shop-goods .goods-list .list .goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%")), "btn-info" to utsMapOf(".shop-goods .goods-list .list .goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "flex-end", "width" to "100%", "marginTop" to "10rpx")), "btn" to utsMapOf(".shop-goods .goods-list .list .goods .btn-info " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center", "width" to "140rpx", "height" to "50rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#aaaaaa", "borderRadius" to "40rpx"), ".footer-info .order-btn " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "160rpx", "height" to "60rpx", "marginLeft" to "20rpx", "borderWidth" to 1, "borderStyle" to "solid", "borderColor" to "#aaaaaa", "borderRadius" to "40rpx")), "shop-service" to utsMapOf(".shop-goods " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "center", "width" to "100%", "height" to "100rpx", "borderTopWidth" to 1, "borderTopStyle" to "solid", "borderTopColor" to "#f8f8f8")), "service" to utsMapOf(".shop-goods .shop-service " to utsMapOf("display" to "flex", "alignItems" to "center", "justifyContent" to "center")), "order-info" to padStyleMapOf(utsMapOf("marginLeft" to "30rpx", "marginRight" to "30rpx", "marginBottom" to "20rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff", "borderRadius" to "20rpx")), "item" to utsMapOf(".order-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "80rpx")), "title" to utsMapOf(".order-info .item " to utsMapOf("display" to "flex", "alignItems" to "center")), "assist" to utsMapOf(".order-info .item " to utsMapOf("display" to "flex", "alignItems" to "center")), "ac" to utsMapOf(".order-info .item .price " to utsMapOf("fontSize" to "28rpx", "fontWeight" to "bold", "color" to "#FF0000")), "content" to utsMapOf(".order-info .item " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "total-price" to utsMapOf(".order-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "flex-end", "width" to "100%", "height" to "100rpx")), "sub-price" to utsMapOf(".order-info .total-price " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "flex-end", "marginTop" to "10rpx", "marginRight" to "10rpx")), "tit" to utsMapOf(".order-info .total-price .price " to utsMapOf("fontSize" to "28rpx", "color" to "#333333")), "min" to utsMapOf(".order-info .total-price .price " to utsMapOf("fontSize" to "24rpx", "fontWeight" to "bold", "color" to "#333333")), "max" to utsMapOf(".order-info .total-price .price " to utsMapOf("fontSize" to "38rpx", "fontWeight" to "bold", "color" to "#333333")), "footer-info" to padStyleMapOf(utsMapOf("position" to "fixed", "left" to 0, "bottom" to 0, "zIndex" to 100, "display" to "flex", "flexDirection" to "row", "alignItems" to "center", "justifyContent" to "space-between", "width" to "100%", "height" to "100rpx", "paddingTop" to 0, "paddingRight" to "30rpx", "paddingBottom" to 0, "paddingLeft" to "30rpx", "backgroundColor" to "#ffffff")), "order-btn" to utsMapOf(".footer-info " to utsMapOf("display" to "flex", "flexDirection" to "row", "alignItems" to "center")), "active" to utsMapOf(".footer-info .order-btn " to utsMapOf("backgroundColor" to "#fe9a5c", "borderWidth" to "medium", "borderStyle" to "none", "borderColor" to "#000000")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
