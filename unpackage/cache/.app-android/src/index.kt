@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.appframe.AppConfig;
import uts.sdk.modules.utsEncrypt.encryptRSA;
import io.dcloud.uniapp.extapi.exit as uni_exit;
import io.dcloud.uniapp.extapi.getStorageSync as uni_getStorageSync;
import io.dcloud.uniapp.extapi.request as uni_request;
import io.dcloud.uniapp.extapi.setStorageSync as uni_setStorageSync;
import io.dcloud.uniapp.extapi.showToast as uni_showToast;
var firstBackTime: Number = 0;
open class GenApp : BaseApp {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onLaunch(fun(_: OnLaunchOptions) {
            console.log("App Launch", " at App.uvue:5");
        }
        , instance);
        onAppShow(fun(_: OnShowOptions) {
            console.log("App Show", " at App.uvue:8");
        }
        , instance);
        onHide(fun() {
            console.log("App Hide", " at App.uvue:11");
        }
        , instance);
        onLastPageBackPress(fun() {
            console.log("App LastPageBackPress", " at App.uvue:14");
            if (firstBackTime == 0) {
                uni_showToast(ShowToastOptions(title = "再按一次退出应用", position = "bottom"));
                firstBackTime = Date.now();
                setTimeout(fun(){
                    firstBackTime = 0;
                }, 2000);
            } else if (Date.now() - firstBackTime < 2000) {
                firstBackTime = Date.now();
                uni_exit(null);
            }
        }
        , instance);
        onExit(fun() {
            console.log("App Exit", " at App.uvue:30");
        }
        , instance);
    }
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("uni-row" to padStyleMapOf(utsMapOf("flexDirection" to "row")), "uni-column" to padStyleMapOf(utsMapOf("flexDirection" to "column")));
            }
    }
}
val GenAppClass = CreateVueAppComponent(GenApp::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "app", name = "", inheritAttrs = true, inject = Map(), props = Map(), propsNeedCastKeys = utsArrayOf(), emits = Map(), components = Map(), styles = GenApp.styles);
}
, fun(instance): GenApp {
    return GenApp(instance);
}
);
val locale = ref<String>((uni_getStorageSync("limeI18nLocale") as String?) ?: "");
val messages = ref(Map<String, UTSJSONObject>());
var fallbackLocale: String? = null;
val runBlock1 = run {
    watch(locale, fun(value: String){
        uni_setStorageSync("limeI18nLocale", value);
    }
    );
}
open class Composer : IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("Composer", "uni_modules/lime-i18n/index.uts", 20, 7);
    }
    constructor(){}
    open fun setLocaleMessage(locale: String, message: UTSJSONObject) {
        val map = Map<String, UTSJSONObject>();
        messages.value.forEach(fun(value, key){
            map.set(key, value);
        }
        );
        map.set(locale, message);
        messages.value = map;
    }
    open fun getLocaleMessage(locale: String): UTSJSONObject {
        return messages.value.get(locale) ?: UTSJSONObject();
    }
}
open class LimeI18n : IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("LimeI18n", "uni_modules/lime-i18n/index.uts", 34, 7);
    }
    private var composer: Composer;
    constructor(){
        this.composer = Composer();
    }
    open fun t(key: String): String {
        return this.t(key as String, null);
    }
    open fun t(key: String, options: UTSJSONObject? = null): String {
        if (messages.value.has(locale.value)) {
            return messages.value.get(locale.value)?.getString(key) ?: "";
        } else if (fallbackLocale != null) {
            return messages.value.get(fallbackLocale)?.getString(key) ?: "";
        }
        return "";
    }
    open val global: Composer
        get(): Composer {
            return this.composer;
        }
}
val i18n = LimeI18n();
fun setLocaleMessage(locale: String, message: UTSJSONObject) {
    i18n.global.setLocaleMessage(locale, message);
}
fun createI18n(options: UTSJSONObject): VuePlugin {
    val _locale = options.getString("locale");
    fallbackLocale = options.getString("fallbackLocale");
    val _messages = options.getJSON("messages") ?: UTSJSONObject();
    if (_locale != null) {
        locale.value = _locale;
    }
    val map = Map<String, UTSJSONObject>();
    _messages.toMap().forEach(fun(value, key){
        if (value is UTSJSONObject) {
            map.set(key, value);
        }
    }
    );
    messages.value = map;
    return definePlugin(VuePlugin(install = fun(app: VueApp) {
        app.config.globalProperties["\$i18n"] = true;
        app.config.globalProperties["\$t"] = true;
        app.config.globalProperties["\$locale"] = true;
    }
    ));
}
val `default` = object : UTSJSONObject() {
    var common = object : UTSJSONObject() {
        var more = "查看更多"
        var hello = "{msg} 世界"
    }
    var leftMenus = object : UTSJSONObject() {
        var home = "首页"
    }
    var headMenus = object : UTSJSONObject() {
        var subTitle = "组织服务平台"
        var userName = "张三"
    }
    var login = object : UTSJSONObject() {
        var personal_center = "个人中心"
        var sign_out = "退出登录"
    }
    var plurals = object : UTSJSONObject() {
        var car = "车 | 辆车"
        var apple = "没有苹果 | 一个苹果 | {count} 个苹果"
        var format = object : UTSJSONObject() {
            var named = "你好 {name}，你好吗？ | 嗨 {name}，你看起来不错"
            var list = "你好 {0}，你好吗？ | 嗨 {0}，你看起来不错"
        }
        var fallback = "这是备选 | 这是一个复数备选"
    }
    var message = object : UTSJSONObject() {
        var hello = "世界"
        var helloName = "你好，{name}"
        var hoge = "hoge"
        var link = "@:message.hello"
        var linkHelloName = "@:message.helloName"
        var linkLinkHelloName = "@:message.linkHelloName"
        var linkEnd = "这是一个链接翻译到 @:message.hello"
        var linkWithin = "难道我们不是生活在美好的世界里吗？"
        var linkMultiple = "你好，@:message.hoge！难道 @:message.hello 不美好吗？"
        var linkBrackets = "你好，@:(message.hoge)。难道 @:(message.hello) 不美好吗？"
        var linkHyphen = "@:hyphen-hello"
        var linkUnderscore = "@:underscore_hello"
        var linkPipe = "@:pipe|hello"
        var linkColon = "@:(colon:hello)"
        var linkList = "@:message.hello：{0} {1}"
        var linkCaseLower = "请提供 @.lower:message.homeAddress"
        var linkCaseUpper = "@.upper:message.homeAddress"
        var linkCaseCapitalize = "@.capitalize:message.homeAddress"
        var linkCaseUnknown = "@.unknown:message.homeAddress"
        var linkCaseCustom = "@.custom:message.homeAddress"
        var homeAddress = "家庭地址"
        var circular1 = "Foo @:message.circular2"
        var circular2 = "Bar @:message.circular3"
        var circular3 = "Buz @:message.circular1"
        var linkTwice = "@:message.hello：@:message.hello"
    }
};
val default1 = object : UTSJSONObject() {
    var common = object : UTSJSONObject() {
        var more = "Look More"
        var hello = "{msg} world"
    }
    var leftMenus = object : UTSJSONObject() {
        var home = "Home"
    }
    var headMenus = object : UTSJSONObject() {
        var subTitle = "Organization service platform"
        var userName = "ZhangSan"
    }
    var login = object : UTSJSONObject() {
        var personal_center = "personal center"
        var sign_out = "sign out"
    }
    var plurals = object : UTSJSONObject() {
        var car = "car | cars"
        var apple = "no apples | one apple | {count} apples"
        var format = object : UTSJSONObject() {
            var named = "Hello {name}, how are you? | Hi {name}, you look fine"
            var list = "Hello {0}, how are you? | Hi {0}, you look fine"
        }
        var fallback = "this is fallback | this is a plural fallback"
    }
    var message = object : UTSJSONObject() {
        var hello = "the world"
        var helloName = "Hello {name}"
        var hoge = "hoge"
        var link = "@:message.hello"
        var linkHelloName = "@:message.helloName"
        var linkLinkHelloName = "@:message.linkHelloName"
        var linkEnd = "This is a linked translation to @:message.hello"
        var linkWithin = "Isn't @:message.hello we live in great?"
        var linkMultiple = "Hello @:message.hoge!, isn't @:message.hello great?"
        var linkBrackets = "Hello @:(message.hoge). Isn't @:(message.hello) great?"
        var linkHyphen = "@:hyphen-hello"
        var linkUnderscore = "@:underscore_hello"
        var linkPipe = "@:pipe|hello"
        var linkColon = "@:(colon:hello)"
        var linkList = "@:message.hello: {0} {1}"
        var linkCaseLower = "Please provide @.lower:message.homeAddress"
        var linkCaseUpper = "@.upper:message.homeAddress"
        var linkCaseCapitalize = "@.capitalize:message.homeAddress"
        var linkCaseUnknown = "@.unknown:message.homeAddress"
        var linkCaseCustom = "@.custom:message.homeAddress"
        var homeAddress = "home Address"
        var circular1 = "Foo @:message.circular2"
        var circular2 = "Bar @:message.circular3"
        var circular3 = "Buz @:message.circular1"
        var linkTwice = "@:message.hello: @:message.hello"
        var the_world = "the world"
        var dio = "DIO:"
        var linked = "@:message.dio @:message.the_world !!!!"
        var missingHomeAddress = "Please provide @.lower:message.homeAddress"
        var snake = "snake case"
        var custom_modifier = "custom modifiers example: @.snakeCase:{'message.snake'}"
    }
    var address = "{account}{'@'}{domain}"
    var `hyphen-hello` = "hyphen the wolrd"
    var underscore_hello = "underscore the wolrd"
};
val i18n1 = createI18n(object : UTSJSONObject() {
    var locale = "zh-cn"
    var fallbackLocale = "en-us"
    var messages = object : UTSJSONObject() {
        var `zh-cn` = `default`
        var `en-us` = default1
    }
});
val runBlock2 = run {
    setTimeout(fun(){
        setLocaleMessage("zh-cn", `default`);
    }
    , 5000);
}
val GenComponentsPagePageClass = CreateVueComponent(GenComponentsPagePage::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenComponentsPagePage.inheritAttrs, inject = GenComponentsPagePage.inject, props = GenComponentsPagePage.props, propsNeedCastKeys = GenComponentsPagePage.propsNeedCastKeys, emits = GenComponentsPagePage.emits, components = GenComponentsPagePage.components, styles = GenComponentsPagePage.styles);
}
, fun(instance): GenComponentsPagePage {
    return GenComponentsPagePage(instance);
}
);
val EnvEnums = object : UTSJSONObject(UTSSourceMapPosition("EnvEnums", "util/config.uts", 2, 7)) {
    var development = "development"
    var production = "production"
};
val apis = utsArrayOf<UTSJSONObject>(object : UTSJSONObject() {
    var env = "development"
    var baseUrl = "https://www.gohiplay.com/api"
    var wsUrl = "https://www.gohiplay.com/api"
}, object : UTSJSONObject() {
    var env = "production"
    var baseUrl = "https://www.gohiplay.com/api"
    var wsUrl = "ws://www.gohiplay.com/api"
});
val runBlock3 = run {
    console.log("=====process.env.NODE_ENV ====" + "development", " at util/config.uts:26");
}
val env = EnvEnums["development"];
val runBlock4 = run {
    console.log("当前环境", env, " at util/config.uts:29");
}
open class BaseConfig : IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("BaseConfig", "util/config.uts", 33, 7);
    }
    private var languageList = utsArrayOf(
        object : UTSJSONObject() {
            var languageName = "English"
            var languageValue = "en"
            var languageImg = "/static/images/language/en.png"
        },
        object : UTSJSONObject() {
            var languageName = "繁体中文"
            var languageValue = "zh"
            var languageImg = "/static/images/language/zh.png"
        },
        object : UTSJSONObject() {
            var languageName = "Bahasa Indonesia"
            var languageValue = "id"
            var languageImg = "/static/images/language/id.png"
        },
        object : UTSJSONObject() {
            var languageName = "Bahasa Melayu"
            var languageValue = "ms"
            var languageImg = "/static/images/language/ms.png"
        },
        object : UTSJSONObject() {
            var languageName = "ไทย"
            var languageValue = "th"
            var languageImg = "/static/images/language/th.png"
        }
    );
    private var appInfo = object : UTSJSONObject() {
        var name = "GoHi Play"
        var version = "1.1.11"
        var logo = "/static/logo.png"
        var site_url = ""
        var agreements = utsArrayOf(
            object : UTSJSONObject() {
                var title = "隐私政策"
                var url = ""
            },
            object : UTSJSONObject() {
                var title = "用户服务协议"
                var url = ""
            }
        )
    };
    companion object {
        fun getBaseUrl(): String {
            console.log(JSON.stringify(apis), " at util/config.uts:36");
            for(api in apis){
                console.log("======" + JSON.stringify(api), " at util/config.uts:38");
                if (api.get("env") as String == env) {
                    console.log("当前环境== baseUrl" + api.get("baseUrl"), " at util/config.uts:40");
                    return api.get("baseUrl") as String;
                }
            }
            return "";
        }
    }
}
open class UxRequestOptions (
    @JsonNotNull
    open var url: String,
    @JsonNotNull
    open var baseURL: String,
    @JsonNotNull
    open var api: String,
    @JsonNotNull
    open var method: RequestMethod,
    open var data: UTSJSONObject? = null,
    @JsonNotNull
    open var header: UTSJSONObject,
    @JsonNotNull
    open var timeout: Number,
) : UTSObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("UxRequestOptions", "uni_modules/ux-request/js_sdk/types.uts", 4, 13)
    }
}
open class UxResponseFail (
    @JsonNotNull
    open var errCode: RequestErrorCode,
    @JsonNotNull
    open var errMsg: String,
) : UTSObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("UxResponseFail", "uni_modules/ux-request/js_sdk/types.uts", 17, 13)
    }
}
open class UxParam (
    @JsonNotNull
    open var api: String,
    @JsonNotNull
    open var method: RequestMethod,
    open var data: UTSJSONObject? = null,
) : UTSObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("UxParam", "uni_modules/ux-request/js_sdk/types.uts", 25, 13)
    }
}
open class UxInterceptor : IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("UxInterceptor", "uni_modules/ux-request/js_sdk/interceptors.uts", 6, 7);
    }
    open fun beforeRequest(config: UxRequestOptions): UxRequestOptions {
        return config;
    }
    open fun afterResponseSucc(response: UTSJSONObject): UTSJSONObject {
        return response;
    }
    open fun afterResponseFail(error: UxResponseFail): UxResponseFail {
        return error;
    }
}
open class UxRequest : IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("UxRequest", "uni_modules/ux-request/js_sdk/request.uts", 7, 7);
    }
    private var options: UxRequestOptions = UxRequestOptions(url = "", baseURL = "", api = "", method = "GET", data = UTSJSONObject(), header = UTSJSONObject(), timeout = 60000);
    private var interceptors: UTSArray<UxInterceptor> = utsArrayOf();
    open var requestTask: RequestTask? = null;
    private constructor(){}
    open fun addInterceptor(interceptor: UxInterceptor): Unit {
        this.interceptors.push(interceptor);
    }
    open fun request(param: UxParam): UTSPromise<UTSJSONObject> {
        this.options.api = param.api;
        this.options.method = param.method;
        this.options.data = param.data;
        this.interceptors.forEach(fun(interceptor){
            this.options = interceptor.beforeRequest(this.options);
        }
        );
        return UTSPromise(fun(resolve, reject){
            this.requestTask = uni_request<UTSJSONObject>(RequestOptions(url = this.options.url as String, method = this.options.method, data = this.options.data, header = this.options.header, timeout = this.options.timeout, success = fun(res){
                if (res.statusCode == 200) {
                    var response = res.data as UTSJSONObject;
                    this.interceptors.forEach(fun(interceptor){
                        response = interceptor.afterResponseSucc(response);
                    });
                    resolve(response);
                } else {
                    uni_showToast(ShowToastOptions(title = "服务器状态码：" + res.statusCode, icon = "none"));
                }
            }
            , fail = fun(err){
                var error = UxResponseFail(errCode = err.errCode, errMsg = err.errMsg);
                this.interceptors.forEach(fun(interceptor){
                    error = interceptor.afterResponseFail(error);
                }
                );
                reject(err);
            }
            )) as RequestTask;
        }
        );
    }
    open fun post(api: String, data: UTSJSONObject?): UTSPromise<UTSJSONObject> {
        return this.request(UxParam(api = api, method = "POST", data = data));
    }
    open fun getData(api: String, data: UTSJSONObject?): UTSPromise<UTSJSONObject> {
        return this.request(UxParam(api = api, method = "GET", data = data));
    }
    open fun abort() {
        if (this.requestTask != null) {
            val task = this.requestTask as RequestTask;
            task.abort();
        }
    }
    companion object {
        private var instance: UxRequest? = null;
        fun create(): UxRequest {
            val instance = UxRequest.instance;
            if (instance == null) {
                UxRequest.instance = UxRequest();
            }
            return UxRequest.instance as UxRequest;
        }
    }
}
var timeout: Number = 50000;
val baseUrl: String = BaseConfig.getBaseUrl();
open class HttpInterceptor : UxInterceptor, IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("HttpInterceptor", "util/request.uts", 14, 7);
    }
    constructor() : super() {}
    override fun beforeRequest(config: UxRequestOptions): UxRequestOptions {
        config.baseURL = baseUrl;
        config.url = config.baseURL + config.api;
        config.timeout = timeout;
        config.header["Content-Type"] = "application/json";
        return config;
    }
    override fun afterResponseSucc(response: UTSJSONObject): UTSJSONObject {
        console.log(JSON.stringify(response), " at util/request.uts:28");
        return response;
    }
    override fun afterResponseFail(error: UxResponseFail): UxResponseFail {
        uni_showToast(ShowToastOptions(title = "错误码: " + error.errCode + ", 错误信息: " + error.errMsg, icon = "none"));
        return error;
    }
}
open class LogInterceptor : UxInterceptor, IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("LogInterceptor", "util/request.uts", 41, 7);
    }
    constructor() : super() {}
    override fun beforeRequest(config: UxRequestOptions): UxRequestOptions {
        console.log("请求前拦截器日志：", config, " at util/request.uts:46");
        return config;
    }
    override fun afterResponseFail(error: UxResponseFail): UxResponseFail {
        console.log("请求失败拦截器日志：", error, " at util/request.uts:50");
        return error;
    }
}
val ins = UxRequest.create();
val runBlock5 = run {
    ins.addInterceptor(HttpInterceptor());
    ins.addInterceptor(LogInterceptor());
}
open class goodsItems (
    open var id: String? = null,
    open var title: String? = null,
    open var coverUrl: String? = null,
    open var nums: Number? = null,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("goodsItems", "apis/episode.uts", 5, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return goodsItemsReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class goodsItemsReactiveObject : goodsItems, IUTSReactive<goodsItems> {
    override var __v_raw: goodsItems;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: goodsItems, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(id = __v_raw.id, title = __v_raw.title, coverUrl = __v_raw.coverUrl, nums = __v_raw.nums) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): goodsItemsReactiveObject {
        return goodsItemsReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var id: String?
        get() {
            return trackReactiveGet(__v_raw, "id", __v_raw.id, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("id")) {
                return;
            }
            val oldValue = __v_raw.id;
            __v_raw.id = value;
            triggerReactiveSet(__v_raw, "id", oldValue, value);
        }
    override var title: String?
        get() {
            return trackReactiveGet(__v_raw, "title", __v_raw.title, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("title")) {
                return;
            }
            val oldValue = __v_raw.title;
            __v_raw.title = value;
            triggerReactiveSet(__v_raw, "title", oldValue, value);
        }
    override var coverUrl: String?
        get() {
            return trackReactiveGet(__v_raw, "coverUrl", __v_raw.coverUrl, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("coverUrl")) {
                return;
            }
            val oldValue = __v_raw.coverUrl;
            __v_raw.coverUrl = value;
            triggerReactiveSet(__v_raw, "coverUrl", oldValue, value);
        }
    override var nums: Number?
        get() {
            return trackReactiveGet(__v_raw, "nums", __v_raw.nums, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("nums")) {
                return;
            }
            val oldValue = __v_raw.nums;
            __v_raw.nums = value;
            triggerReactiveSet(__v_raw, "nums", oldValue, value);
        }
}
open class episodeItem (
    open var id: String? = null,
    open var title: String? = null,
    open var shortTitle: String? = null,
    open var description: String? = null,
    open var coverId: String? = null,
    open var episodeDetails: UTSArray<episodeDetailItem>? = null,
) : UTSObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("episodeItem", "apis/episode.uts", 15, 13)
    }
}
open class episodeDetailItem (
    open var id: String? = null,
    open var title: String? = null,
    open var sort: Number? = null,
    open var lockFlag: Boolean? = null,
    open var unLockFlag: Boolean? = null,
    open var fileId: String? = null,
    open var fileUrl: String? = null,
    open var poster_src: String? = null,
    open var url: String? = null,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("episodeDetailItem", "apis/episode.uts", 28, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return episodeDetailItemReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class episodeDetailItemReactiveObject : episodeDetailItem, IUTSReactive<episodeDetailItem> {
    override var __v_raw: episodeDetailItem;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: episodeDetailItem, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(id = __v_raw.id, title = __v_raw.title, sort = __v_raw.sort, lockFlag = __v_raw.lockFlag, unLockFlag = __v_raw.unLockFlag, fileId = __v_raw.fileId, fileUrl = __v_raw.fileUrl, poster_src = __v_raw.poster_src, url = __v_raw.url) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): episodeDetailItemReactiveObject {
        return episodeDetailItemReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var id: String?
        get() {
            return trackReactiveGet(__v_raw, "id", __v_raw.id, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("id")) {
                return;
            }
            val oldValue = __v_raw.id;
            __v_raw.id = value;
            triggerReactiveSet(__v_raw, "id", oldValue, value);
        }
    override var title: String?
        get() {
            return trackReactiveGet(__v_raw, "title", __v_raw.title, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("title")) {
                return;
            }
            val oldValue = __v_raw.title;
            __v_raw.title = value;
            triggerReactiveSet(__v_raw, "title", oldValue, value);
        }
    override var sort: Number?
        get() {
            return trackReactiveGet(__v_raw, "sort", __v_raw.sort, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("sort")) {
                return;
            }
            val oldValue = __v_raw.sort;
            __v_raw.sort = value;
            triggerReactiveSet(__v_raw, "sort", oldValue, value);
        }
    override var lockFlag: Boolean?
        get() {
            return trackReactiveGet(__v_raw, "lockFlag", __v_raw.lockFlag, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("lockFlag")) {
                return;
            }
            val oldValue = __v_raw.lockFlag;
            __v_raw.lockFlag = value;
            triggerReactiveSet(__v_raw, "lockFlag", oldValue, value);
        }
    override var unLockFlag: Boolean?
        get() {
            return trackReactiveGet(__v_raw, "unLockFlag", __v_raw.unLockFlag, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("unLockFlag")) {
                return;
            }
            val oldValue = __v_raw.unLockFlag;
            __v_raw.unLockFlag = value;
            triggerReactiveSet(__v_raw, "unLockFlag", oldValue, value);
        }
    override var fileId: String?
        get() {
            return trackReactiveGet(__v_raw, "fileId", __v_raw.fileId, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("fileId")) {
                return;
            }
            val oldValue = __v_raw.fileId;
            __v_raw.fileId = value;
            triggerReactiveSet(__v_raw, "fileId", oldValue, value);
        }
    override var fileUrl: String?
        get() {
            return trackReactiveGet(__v_raw, "fileUrl", __v_raw.fileUrl, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("fileUrl")) {
                return;
            }
            val oldValue = __v_raw.fileUrl;
            __v_raw.fileUrl = value;
            triggerReactiveSet(__v_raw, "fileUrl", oldValue, value);
        }
    override var poster_src: String?
        get() {
            return trackReactiveGet(__v_raw, "poster_src", __v_raw.poster_src, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("poster_src")) {
                return;
            }
            val oldValue = __v_raw.poster_src;
            __v_raw.poster_src = value;
            triggerReactiveSet(__v_raw, "poster_src", oldValue, value);
        }
    override var url: String?
        get() {
            return trackReactiveGet(__v_raw, "url", __v_raw.url, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("url")) {
                return;
            }
            val oldValue = __v_raw.url;
            __v_raw.url = value;
            triggerReactiveSet(__v_raw, "url", oldValue, value);
        }
}
fun listEpisode(data: UTSJSONObject): UTSPromise<UTSJSONObject> {
    return ins.post("/video/app/episode/list", data);
}
fun getEpisode(id: String): UTSPromise<UTSJSONObject> {
    return ins.getData("/video/app/episode/" + id, null);
}
open class bannerItem (
    @JsonNotNull
    open var id: String,
    @JsonNotNull
    open var coverUrl: String,
    open var title: String? = null,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("bannerItem", "apis/banner.uts", 5, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return bannerItemReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class bannerItemReactiveObject : bannerItem, IUTSReactive<bannerItem> {
    override var __v_raw: bannerItem;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: bannerItem, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(id = __v_raw.id, coverUrl = __v_raw.coverUrl, title = __v_raw.title) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): bannerItemReactiveObject {
        return bannerItemReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var id: String
        get() {
            return trackReactiveGet(__v_raw, "id", __v_raw.id, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("id")) {
                return;
            }
            val oldValue = __v_raw.id;
            __v_raw.id = value;
            triggerReactiveSet(__v_raw, "id", oldValue, value);
        }
    override var coverUrl: String
        get() {
            return trackReactiveGet(__v_raw, "coverUrl", __v_raw.coverUrl, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("coverUrl")) {
                return;
            }
            val oldValue = __v_raw.coverUrl;
            __v_raw.coverUrl = value;
            triggerReactiveSet(__v_raw, "coverUrl", oldValue, value);
        }
    override var title: String?
        get() {
            return trackReactiveGet(__v_raw, "title", __v_raw.title, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("title")) {
                return;
            }
            val oldValue = __v_raw.title;
            __v_raw.title = value;
            triggerReactiveSet(__v_raw, "title", oldValue, value);
        }
}
fun listBanner(data: UTSJSONObject): UTSPromise<UTSJSONObject> {
    return ins.post("/video/app/banner/list", data);
}
val DEFAULT_ROW_WIDTH: String = "100%";
val DEFAULT_LAST_ROW_WIDTH: String = "60%";
val GenComponentsSkeletonSkeletonClass = CreateVueComponent(GenComponentsSkeletonSkeleton::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenComponentsSkeletonSkeleton.inheritAttrs, inject = GenComponentsSkeletonSkeleton.inject, props = GenComponentsSkeletonSkeleton.props, propsNeedCastKeys = GenComponentsSkeletonSkeleton.propsNeedCastKeys, emits = GenComponentsSkeletonSkeleton.emits, components = GenComponentsSkeletonSkeleton.components, styles = GenComponentsSkeletonSkeleton.styles);
}
, fun(instance): GenComponentsSkeletonSkeleton {
    return GenComponentsSkeletonSkeleton(instance);
}
);
open class memuItem (
    @JsonNotNull
    open var title: String,
    @JsonNotNull
    open var path: String,
) : UTSObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("memuItem", "pages/home/home.uvue", 130, 8)
    }
}
val GenPagesHomeHomeClass = CreateVueComponent(GenPagesHomeHome::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesHomeHome.inheritAttrs, inject = GenPagesHomeHome.inject, props = GenPagesHomeHome.props, propsNeedCastKeys = GenPagesHomeHome.propsNeedCastKeys, emits = GenPagesHomeHome.emits, components = GenPagesHomeHome.components, styles = GenPagesHomeHome.styles);
}
, fun(instance): GenPagesHomeHome {
    return GenPagesHomeHome(instance);
}
);
open class classifyItem (
    @JsonNotNull
    open var id: Number,
    @JsonNotNull
    open var parent: Number,
    @JsonNotNull
    open var name: String,
    open var pic: String? = null,
    @JsonNotNull
    open var children: UTSArray<classifyItem>,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("classifyItem", "mock/classifyData.uts", 5, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return classifyItemReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class classifyItemReactiveObject : classifyItem, IUTSReactive<classifyItem> {
    override var __v_raw: classifyItem;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: classifyItem, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(id = __v_raw.id, parent = __v_raw.parent, name = __v_raw.name, pic = __v_raw.pic, children = __v_raw.children) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): classifyItemReactiveObject {
        return classifyItemReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var id: Number
        get() {
            return trackReactiveGet(__v_raw, "id", __v_raw.id, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("id")) {
                return;
            }
            val oldValue = __v_raw.id;
            __v_raw.id = value;
            triggerReactiveSet(__v_raw, "id", oldValue, value);
        }
    override var parent: Number
        get() {
            return trackReactiveGet(__v_raw, "parent", __v_raw.parent, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("parent")) {
                return;
            }
            val oldValue = __v_raw.parent;
            __v_raw.parent = value;
            triggerReactiveSet(__v_raw, "parent", oldValue, value);
        }
    override var name: String
        get() {
            return trackReactiveGet(__v_raw, "name", __v_raw.name, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("name")) {
                return;
            }
            val oldValue = __v_raw.name;
            __v_raw.name = value;
            triggerReactiveSet(__v_raw, "name", oldValue, value);
        }
    override var pic: String?
        get() {
            return trackReactiveGet(__v_raw, "pic", __v_raw.pic, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("pic")) {
                return;
            }
            val oldValue = __v_raw.pic;
            __v_raw.pic = value;
            triggerReactiveSet(__v_raw, "pic", oldValue, value);
        }
    override var children: UTSArray<classifyItem>
        get() {
            return trackReactiveGet(__v_raw, "children", __v_raw.children, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("children")) {
                return;
            }
            val oldValue = __v_raw.children;
            __v_raw.children = value;
            triggerReactiveSet(__v_raw, "children", oldValue, value);
        }
}
val ClassifyData = utsArrayOf<classifyItem>(classifyItem(id = 1, parent = 0, name = "手机", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 2, parent = 1, name = "手机通讯", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 3, parent = 2, name = "游戏手机", pic = "https://img11.360buyimg.com/focus/s140x140_jfs/t11470/45/2362968077/2689/fb36d9a0/5a169238Nc8f0882b.jpg", children = utsArrayOf()), classifyItem(id = 4, parent = 2, name = "手机", pic = "https://img10.360buyimg.com/focus/s140x140_jfs/t11503/241/2246064496/4783/cea2850e/5a169216N0701c7f1.jpg", children = utsArrayOf()), classifyItem(id = 5, parent = 2, name = "全面屏手机", pic = "https://img30.360buyimg.com/focus/s140x140_jfs/t18955/187/1309277884/11517/fe100782/5ac48d27N3f5bb821.jpg", children = utsArrayOf()))), classifyItem(id = 24, parent = 1, name = "品牌", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 25, parent = 24, name = "华为", pic = "https://img14.360buyimg.com/focus/s140x140_jfs/t11929/135/2372293765/1396/e103ec31/5a1692e2Nbea6e136.jpg", children = utsArrayOf()), classifyItem(id = 26, parent = 24, name = "小米", pic = "https://img30.360buyimg.com/focus/s140x140_jfs/t13411/188/926813276/3945/a4f47292/5a1692eeN105a64b4.png", children = utsArrayOf()), classifyItem(id = 27, parent = 24, name = "荣耀", pic = "https://img10.360buyimg.com/focus/s140x140_jfs/t12178/348/911080073/4732/db0ad9c7/5a1692e2N6df7c609.jpg", children = utsArrayOf()), classifyItem(id = 28, parent = 24, name = "苹果", pic = "https://img20.360buyimg.com/focus/s140x140_jfs/t13759/194/897734755/2493/1305d4c4/5a1692ebN8ae73077.jpg", children = utsArrayOf()))))), classifyItem(id = 6, parent = 0, name = "电脑办公", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 7, parent = 6, name = "笔记本", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 8, parent = 7, name = "轻薄本", pic = "https://img13.360buyimg.com/focus/s140x140_jfs/t11071/195/2462134264/9117/cd0688bf/5a17ba79N18b9f3d4.png", children = utsArrayOf()), classifyItem(id = 9, parent = 7, name = "游戏本", pic = "https://img30.360buyimg.com/focus/s140x140_jfs/t11155/36/2330310765/10690/eb6754c3/5a17ba96N49561fea.png", children = utsArrayOf()), classifyItem(id = 10, parent = 7, name = "办公本", pic = "https://img20.360buyimg.com/focus/s140x140_jfs/t12499/273/957225674/6892/8281d4a7/5a17b962Nf77d9f6c.jpg", children = utsArrayOf()))))), classifyItem(id = 11, parent = 0, name = "服装", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 12, parent = 11, name = "男装", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 13, parent = 12, name = "风衣", pic = "https://img20.360buyimg.com/focus/s140x140_jfs/t17890/31/1269777779/2792/917e13d0/5ac47830N63e76af2.jpg", children = utsArrayOf()), classifyItem(id = 14, parent = 12, name = "T恤", pic = "https://img12.360buyimg.com/focus/s140x140_jfs/t17641/277/1305218449/8776/e5182bbe/5ac47ffaN8a7b2e14.png", children = utsArrayOf()))), classifyItem(id = 15, parent = 11, name = "女装", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 16, parent = 15, name = "连衣裙", pic = "https://img30.360buyimg.com/focus/s140x140_jfs/t16891/72/715748110/3080/182127b5/5a9fb67aN37c4848f.jpg", children = utsArrayOf()))))), classifyItem(id = 17, parent = 0, name = "美妆护肤", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 18, parent = 17, name = "拔草推荐", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 19, parent = 18, name = "显白口红", pic = "https://img10.360buyimg.com/focus/s140x140_jfs/t1/95022/3/13977/20829/5e5f2636E20222316/bbc6e2cf5b10669e.jpg", children = utsArrayOf()), classifyItem(id = 20, parent = 18, name = "明星同款面膜", pic = "https://img14.360buyimg.com/focus/s140x140_jfs/t1/91206/20/13565/9379/5e5f262bE45790537/0373287c48fa2317.jpg", children = utsArrayOf()))), classifyItem(id = 21, parent = 17, name = "彩妆", pic = "", children = utsArrayOf<classifyItem>(classifyItem(id = 22, parent = 21, name = "美甲", pic = "https://img11.360buyimg.com/focus/s140x140_jfs/t18340/344/2560965947/8933/468d229f/5afd3c2aN62a8f842.jpg", children = utsArrayOf()), classifyItem(id = 23, parent = 21, name = "粉底液", pic = "https://img20.360buyimg.com/focus/s140x140_jfs/t20692/251/127894832/28255/9c74e1cd/5afd3c1eN4eb4f341.jpg", children = utsArrayOf()))))));
val GenPagesClassifyClassifyClass = CreateVueComponent(GenPagesClassifyClassify::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesClassifyClassify.inheritAttrs, inject = GenPagesClassifyClassify.inject, props = GenPagesClassifyClassify.props, propsNeedCastKeys = GenPagesClassifyClassify.propsNeedCastKeys, emits = GenPagesClassifyClassify.emits, components = GenPagesClassifyClassify.components, styles = GenPagesClassifyClassify.styles);
}
, fun(instance): GenPagesClassifyClassify {
    return GenPagesClassifyClassify(instance);
}
);
open class goodsItems1 (
    open var id: Number? = null,
    open var name: String? = null,
    open var price: Number? = null,
    open var originalPrice: Number? = null,
    open var salesVolume: Number? = null,
    open var pic: String? = null,
    open var shopName: String? = null,
    open var inventory: Number? = null,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("goodsItems", "mock/goodsData.uts", 5, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return goodsItems1ReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class goodsItems1ReactiveObject : goodsItems1, IUTSReactive<goodsItems1> {
    override var __v_raw: goodsItems1;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: goodsItems1, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(id = __v_raw.id, name = __v_raw.name, price = __v_raw.price, originalPrice = __v_raw.originalPrice, salesVolume = __v_raw.salesVolume, pic = __v_raw.pic, shopName = __v_raw.shopName, inventory = __v_raw.inventory) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): goodsItems1ReactiveObject {
        return goodsItems1ReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var id: Number?
        get() {
            return trackReactiveGet(__v_raw, "id", __v_raw.id, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("id")) {
                return;
            }
            val oldValue = __v_raw.id;
            __v_raw.id = value;
            triggerReactiveSet(__v_raw, "id", oldValue, value);
        }
    override var name: String?
        get() {
            return trackReactiveGet(__v_raw, "name", __v_raw.name, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("name")) {
                return;
            }
            val oldValue = __v_raw.name;
            __v_raw.name = value;
            triggerReactiveSet(__v_raw, "name", oldValue, value);
        }
    override var price: Number?
        get() {
            return trackReactiveGet(__v_raw, "price", __v_raw.price, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("price")) {
                return;
            }
            val oldValue = __v_raw.price;
            __v_raw.price = value;
            triggerReactiveSet(__v_raw, "price", oldValue, value);
        }
    override var originalPrice: Number?
        get() {
            return trackReactiveGet(__v_raw, "originalPrice", __v_raw.originalPrice, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("originalPrice")) {
                return;
            }
            val oldValue = __v_raw.originalPrice;
            __v_raw.originalPrice = value;
            triggerReactiveSet(__v_raw, "originalPrice", oldValue, value);
        }
    override var salesVolume: Number?
        get() {
            return trackReactiveGet(__v_raw, "salesVolume", __v_raw.salesVolume, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("salesVolume")) {
                return;
            }
            val oldValue = __v_raw.salesVolume;
            __v_raw.salesVolume = value;
            triggerReactiveSet(__v_raw, "salesVolume", oldValue, value);
        }
    override var pic: String?
        get() {
            return trackReactiveGet(__v_raw, "pic", __v_raw.pic, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("pic")) {
                return;
            }
            val oldValue = __v_raw.pic;
            __v_raw.pic = value;
            triggerReactiveSet(__v_raw, "pic", oldValue, value);
        }
    override var shopName: String?
        get() {
            return trackReactiveGet(__v_raw, "shopName", __v_raw.shopName, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("shopName")) {
                return;
            }
            val oldValue = __v_raw.shopName;
            __v_raw.shopName = value;
            triggerReactiveSet(__v_raw, "shopName", oldValue, value);
        }
    override var inventory: Number?
        get() {
            return trackReactiveGet(__v_raw, "inventory", __v_raw.inventory, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("inventory")) {
                return;
            }
            val oldValue = __v_raw.inventory;
            __v_raw.inventory = value;
            triggerReactiveSet(__v_raw, "inventory", oldValue, value);
        }
}
open class breadGoodsItem (
    @JsonNotNull
    open var id: Number,
    @JsonNotNull
    open var name: String,
    @JsonNotNull
    open var logo: String,
    @JsonNotNull
    open var bgPic: String,
    @JsonNotNull
    open var goodsList: UTSArray<goodsItems1>,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("breadGoodsItem", "mock/goodsData.uts", 18, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return breadGoodsItemReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class breadGoodsItemReactiveObject : breadGoodsItem, IUTSReactive<breadGoodsItem> {
    override var __v_raw: breadGoodsItem;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: breadGoodsItem, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(id = __v_raw.id, name = __v_raw.name, logo = __v_raw.logo, bgPic = __v_raw.bgPic, goodsList = __v_raw.goodsList) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): breadGoodsItemReactiveObject {
        return breadGoodsItemReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var id: Number
        get() {
            return trackReactiveGet(__v_raw, "id", __v_raw.id, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("id")) {
                return;
            }
            val oldValue = __v_raw.id;
            __v_raw.id = value;
            triggerReactiveSet(__v_raw, "id", oldValue, value);
        }
    override var name: String
        get() {
            return trackReactiveGet(__v_raw, "name", __v_raw.name, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("name")) {
                return;
            }
            val oldValue = __v_raw.name;
            __v_raw.name = value;
            triggerReactiveSet(__v_raw, "name", oldValue, value);
        }
    override var logo: String
        get() {
            return trackReactiveGet(__v_raw, "logo", __v_raw.logo, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("logo")) {
                return;
            }
            val oldValue = __v_raw.logo;
            __v_raw.logo = value;
            triggerReactiveSet(__v_raw, "logo", oldValue, value);
        }
    override var bgPic: String
        get() {
            return trackReactiveGet(__v_raw, "bgPic", __v_raw.bgPic, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("bgPic")) {
                return;
            }
            val oldValue = __v_raw.bgPic;
            __v_raw.bgPic = value;
            triggerReactiveSet(__v_raw, "bgPic", oldValue, value);
        }
    override var goodsList: UTSArray<goodsItems1>
        get() {
            return trackReactiveGet(__v_raw, "goodsList", __v_raw.goodsList, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("goodsList")) {
                return;
            }
            val oldValue = __v_raw.goodsList;
            __v_raw.goodsList = value;
            triggerReactiveSet(__v_raw, "goodsList", oldValue, value);
        }
}
val goodsData = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店", inventory = 10000), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店", inventory = 3000), goodsItems1(id = 4, name = "罗蒙羊毛双面呢大衣男士秋冬中长款风衣外套休闲保暖呢子大衣上衣男装", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", price = 272.34, originalPrice = 299, salesVolume = 235, shopName = "罗蒙（ROMON）自营专区", inventory = 500), goodsItems1(id = 5, name = "尚都比拉秋季复古气质女神范针织连衣裙中长款显瘦a字裙 黑色 L ", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228639/27/5562/25890/6569ab29Ff3a9d0fe/91d6428be15da41c.jpg!q80.dpg.webp", price = 169, originalPrice = 199, salesVolume = 235, shopName = "尚都比拉自营旗舰店", inventory = 6000), goodsItems1(id = 6, name = "资生堂（Shiseido）悦薇水乳小样护肤品化妆品旅行套装 滋润滋养 新款中样4件：洁面+水+乳液+精华", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/121610/13/41125/70882/64ffdc37F4afe4517/775a22b274bddbc8.jpg!q80.dpg.webp", price = 276, originalPrice = 299.67, salesVolume = 235, shopName = "慕雪倾城美妆专营店", inventory = 300), goodsItems1(id = 7, name = "olayks电压力锅 高压锅 家用多功能高压电饭锅快煮智能预约小压力锅 3升适用3-5人用", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/218941/9/35528/25457/65755e41F35e7888c/fef994d37d44ecca.jpg!q70.dpg.webp", price = 299, originalPrice = 329, salesVolume = 97, shopName = "olayks自营旗舰店", inventory = 100), goodsItems1(id = 8, name = "乐芬兰（LeFenLan）短外套女2023秋冬新款宽松气质羊羔毛小个子复古皮毛一体夹克上衣 卡其色 S", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/104119/12/39372/58031/6523addaF4a77719f/8b7fa71598a9e566.jpg!q80.dpg.webp", price = 158, originalPrice = 199, salesVolume = 999, shopName = "乐芬兰女装旗舰店", inventory = 10002));
val dayData = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店"), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店"), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店"));
val cartData = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店"), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店"), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店"));
val seckillGoodsData = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 27, inventory = 100), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, inventory = 200), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, inventory = 1000), goodsItems1(id = 4, name = "罗蒙羊毛双面呢大衣男士秋冬中长款风衣外套休闲保暖呢子大衣上衣男装", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", price = 272.34, originalPrice = 299, salesVolume = 235, inventory = 300), goodsItems1(id = 5, name = "尚都比拉秋季复古气质女神范针织连衣裙中长款显瘦a字裙 黑色 L ", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228639/27/5562/25890/6569ab29Ff3a9d0fe/91d6428be15da41c.jpg!q80.dpg.webp", price = 169, originalPrice = 199, salesVolume = 235, inventory = 300), goodsItems1(id = 6, name = "资生堂（Shiseido）悦薇水乳小样护肤品化妆品旅行套装 滋润滋养 新款中样4件：洁面+水+乳液+精华", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/121610/13/41125/70882/64ffdc37F4afe4517/775a22b274bddbc8.jpg!q80.dpg.webp", price = 276, originalPrice = 299.67, salesVolume = 235, inventory = 300), goodsItems1(id = 7, name = "olayks电压力锅 高压锅 家用多功能高压电饭锅快煮智能预约小压力锅 3升适用3-5人用", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/218941/9/35528/25457/65755e41F35e7888c/fef994d37d44ecca.jpg!q70.dpg.webp", price = 299, originalPrice = 329, salesVolume = 97, inventory = 100), goodsItems1(id = 8, name = "乐芬兰（LeFenLan）短外套女2023秋冬新款宽松气质羊羔毛小个子复古皮毛一体夹克上衣 卡其色 S", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/104119/12/39372/58031/6523addaF4a77719f/8b7fa71598a9e566.jpg!q80.dpg.webp", price = 158, originalPrice = 199, salesVolume = 666, inventory = 1000));
val brandGoodsData = utsArrayOf<breadGoodsItem>(breadGoodsItem(id = 1, name = "华为", logo = "https://m.360buyimg.com/babel/jfs/t1/208085/14/30593/1587/64c214c4F6d62760f/80cfa31c908685aa.png", bgPic = "https://m11.360buyimg.com/babel/s531x768_jfs/t20270108/88577/35/38548/76327/659d12e0Fc109995f/0df46d1eae2f6dba.jpg!q70.dpg.webp", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店", inventory = 10000), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店", inventory = 3000))), breadGoodsItem(id = 2, name = "小米", logo = "https://m.360buyimg.com/babel/jfs/t1/193915/33/35652/871/64c1deeaFdd644add/b12fd01dbc2f8118.png", bgPic = "https://m11.360buyimg.com/babel/s531x768_jfs/t1/246864/15/2746/126292/659cf28dF5d40b3b9/683aa6008f232455.png.webp", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店", inventory = 10000), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店", inventory = 3000))), breadGoodsItem(id = 3, name = "荣耀", logo = "https://m.360buyimg.com/babel/jfs/t20260727/171047/19/39009/4954/64c33864F32d41c91/92ae66a179f96989.png", bgPic = "https://img12.360buyimg.com/n2/s240x240_jfs/t1/227842/26/11786/104155/659cae74F3eeea9dd/c7ed11ed6e989cda.jpg!q70.jpg.webp", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店", inventory = 10000), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店", inventory = 3000))));
val GenPagesCartCartClass = CreateVueComponent(GenPagesCartCart::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesCartCart.inheritAttrs, inject = GenPagesCartCart.inject, props = GenPagesCartCart.props, propsNeedCastKeys = GenPagesCartCart.propsNeedCastKeys, emits = GenPagesCartCart.emits, components = GenPagesCartCart.components, styles = GenPagesCartCart.styles);
}
, fun(instance): GenPagesCartCart {
    return GenPagesCartCart(instance);
}
);
val GenPagesMyMyClass = CreateVueComponent(GenPagesMyMy::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesMyMy.inheritAttrs, inject = GenPagesMyMy.inject, props = GenPagesMyMy.props, propsNeedCastKeys = GenPagesMyMy.propsNeedCastKeys, emits = GenPagesMyMy.emits, components = GenPagesMyMy.components, styles = GenPagesMyMy.styles);
}
, fun(instance): GenPagesMyMy {
    return GenPagesMyMy(instance);
}
);
fun toast(title: String) {
    uni_showToast(ShowToastOptions(title = title, icon = "none", duration = 2000));
}
open class hotKeywordItem (
    @JsonNotNull
    open var keyword: String,
    @JsonNotNull
    open var sort: Number,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("hotKeywordItem", "pages/search/search.uvue", 63, 8)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return hotKeywordItemReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class hotKeywordItemReactiveObject : hotKeywordItem, IUTSReactive<hotKeywordItem> {
    override var __v_raw: hotKeywordItem;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: hotKeywordItem, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(keyword = __v_raw.keyword, sort = __v_raw.sort) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): hotKeywordItemReactiveObject {
        return hotKeywordItemReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var keyword: String
        get() {
            return trackReactiveGet(__v_raw, "keyword", __v_raw.keyword, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("keyword")) {
                return;
            }
            val oldValue = __v_raw.keyword;
            __v_raw.keyword = value;
            triggerReactiveSet(__v_raw, "keyword", oldValue, value);
        }
    override var sort: Number
        get() {
            return trackReactiveGet(__v_raw, "sort", __v_raw.sort, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("sort")) {
                return;
            }
            val oldValue = __v_raw.sort;
            __v_raw.sort = value;
            triggerReactiveSet(__v_raw, "sort", oldValue, value);
        }
}
val GenPagesSearchSearchClass = CreateVueComponent(GenPagesSearchSearch::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesSearchSearch.inheritAttrs, inject = GenPagesSearchSearch.inject, props = GenPagesSearchSearch.props, propsNeedCastKeys = GenPagesSearchSearch.propsNeedCastKeys, emits = GenPagesSearchSearch.emits, components = GenPagesSearchSearch.components, styles = GenPagesSearchSearch.styles);
}
, fun(instance): GenPagesSearchSearch {
    return GenPagesSearchSearch(instance);
}
);
val GenComponentsCatPopupCatPopupClass = CreateVueComponent(GenComponentsCatPopupCatPopup::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = GenComponentsCatPopupCatPopup.name, inheritAttrs = GenComponentsCatPopupCatPopup.inheritAttrs, inject = GenComponentsCatPopupCatPopup.inject, props = GenComponentsCatPopupCatPopup.props, propsNeedCastKeys = GenComponentsCatPopupCatPopup.propsNeedCastKeys, emits = GenComponentsCatPopupCatPopup.emits, components = GenComponentsCatPopupCatPopup.components, styles = GenComponentsCatPopupCatPopup.styles);
}
, fun(instance): GenComponentsCatPopupCatPopup {
    return GenComponentsCatPopupCatPopup(instance);
}
);
val GenPagesSearchGoodsSearchGoodsClass = CreateVueComponent(GenPagesSearchGoodsSearchGoods::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesSearchGoodsSearchGoods.inheritAttrs, inject = GenPagesSearchGoodsSearchGoods.inject, props = GenPagesSearchGoodsSearchGoods.props, propsNeedCastKeys = GenPagesSearchGoodsSearchGoods.propsNeedCastKeys, emits = GenPagesSearchGoodsSearchGoods.emits, components = GenPagesSearchGoodsSearchGoods.components, styles = GenPagesSearchGoodsSearchGoods.styles);
}
, fun(instance): GenPagesSearchGoodsSearchGoods {
    return GenPagesSearchGoodsSearchGoods(instance);
}
);
val GenPagesGoodsDetailComponentsAttrPopupAttrPopupClass = CreateVueComponent(GenPagesGoodsDetailComponentsAttrPopupAttrPopup::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenPagesGoodsDetailComponentsAttrPopupAttrPopup.inheritAttrs, inject = GenPagesGoodsDetailComponentsAttrPopupAttrPopup.inject, props = GenPagesGoodsDetailComponentsAttrPopupAttrPopup.props, propsNeedCastKeys = GenPagesGoodsDetailComponentsAttrPopupAttrPopup.propsNeedCastKeys, emits = GenPagesGoodsDetailComponentsAttrPopupAttrPopup.emits, components = GenPagesGoodsDetailComponentsAttrPopupAttrPopup.components, styles = GenPagesGoodsDetailComponentsAttrPopupAttrPopup.styles);
}
, fun(instance): GenPagesGoodsDetailComponentsAttrPopupAttrPopup {
    return GenPagesGoodsDetailComponentsAttrPopupAttrPopup(instance);
}
);
val GenPagesGoodsDetailGoodsDetailClass = CreateVueComponent(GenPagesGoodsDetailGoodsDetail::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesGoodsDetailGoodsDetail.inheritAttrs, inject = GenPagesGoodsDetailGoodsDetail.inject, props = GenPagesGoodsDetailGoodsDetail.props, propsNeedCastKeys = GenPagesGoodsDetailGoodsDetail.propsNeedCastKeys, emits = GenPagesGoodsDetailGoodsDetail.emits, components = GenPagesGoodsDetailGoodsDetail.components, styles = GenPagesGoodsDetailGoodsDetail.styles);
}
, fun(instance): GenPagesGoodsDetailGoodsDetail {
    return GenPagesGoodsDetailGoodsDetail(instance);
}
);
val GenPagesConfirmOrderConfirmOrderClass = CreateVueComponent(GenPagesConfirmOrderConfirmOrder::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesConfirmOrderConfirmOrder.inheritAttrs, inject = GenPagesConfirmOrderConfirmOrder.inject, props = GenPagesConfirmOrderConfirmOrder.props, propsNeedCastKeys = GenPagesConfirmOrderConfirmOrder.propsNeedCastKeys, emits = GenPagesConfirmOrderConfirmOrder.emits, components = GenPagesConfirmOrderConfirmOrder.components, styles = GenPagesConfirmOrderConfirmOrder.styles);
}
, fun(instance): GenPagesConfirmOrderConfirmOrder {
    return GenPagesConfirmOrderConfirmOrder(instance);
}
);
val GenPagesAddressListAddressListClass = CreateVueComponent(GenPagesAddressListAddressList::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesAddressListAddressList.inheritAttrs, inject = GenPagesAddressListAddressList.inject, props = GenPagesAddressListAddressList.props, propsNeedCastKeys = GenPagesAddressListAddressList.propsNeedCastKeys, emits = GenPagesAddressListAddressList.emits, components = GenPagesAddressListAddressList.components, styles = GenPagesAddressListAddressList.styles);
}
, fun(instance): GenPagesAddressListAddressList {
    return GenPagesAddressListAddressList(instance);
}
);
val GenPagesAddressAddEditAddressAddEditClass = CreateVueComponent(GenPagesAddressAddEditAddressAddEdit::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesAddressAddEditAddressAddEdit.inheritAttrs, inject = GenPagesAddressAddEditAddressAddEdit.inject, props = GenPagesAddressAddEditAddressAddEdit.props, propsNeedCastKeys = GenPagesAddressAddEditAddressAddEdit.propsNeedCastKeys, emits = GenPagesAddressAddEditAddressAddEdit.emits, components = GenPagesAddressAddEditAddressAddEdit.components, styles = GenPagesAddressAddEditAddressAddEdit.styles);
}
, fun(instance): GenPagesAddressAddEditAddressAddEdit {
    return GenPagesAddressAddEditAddressAddEdit(instance);
}
);
val GenPagesGoodsCollectGoodsCollectClass = CreateVueComponent(GenPagesGoodsCollectGoodsCollect::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesGoodsCollectGoodsCollect.inheritAttrs, inject = GenPagesGoodsCollectGoodsCollect.inject, props = GenPagesGoodsCollectGoodsCollect.props, propsNeedCastKeys = GenPagesGoodsCollectGoodsCollect.propsNeedCastKeys, emits = GenPagesGoodsCollectGoodsCollect.emits, components = GenPagesGoodsCollectGoodsCollect.components, styles = GenPagesGoodsCollectGoodsCollect.styles);
}
, fun(instance): GenPagesGoodsCollectGoodsCollect {
    return GenPagesGoodsCollectGoodsCollect(instance);
}
);
val GenPagesShopCollectShopCollectClass = CreateVueComponent(GenPagesShopCollectShopCollect::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesShopCollectShopCollect.inheritAttrs, inject = GenPagesShopCollectShopCollect.inject, props = GenPagesShopCollectShopCollect.props, propsNeedCastKeys = GenPagesShopCollectShopCollect.propsNeedCastKeys, emits = GenPagesShopCollectShopCollect.emits, components = GenPagesShopCollectShopCollect.components, styles = GenPagesShopCollectShopCollect.styles);
}
, fun(instance): GenPagesShopCollectShopCollect {
    return GenPagesShopCollectShopCollect(instance);
}
);
open class OrderItem (
    @JsonNotNull
    open var id: Number,
    @JsonNotNull
    open var shopName: String,
    @JsonNotNull
    open var status: Number,
    @JsonNotNull
    open var goodsList: UTSArray<goodsItems1>,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("OrderItem", "mock/orderData.uts", 7, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return OrderItemReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class OrderItemReactiveObject : OrderItem, IUTSReactive<OrderItem> {
    override var __v_raw: OrderItem;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: OrderItem, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(id = __v_raw.id, shopName = __v_raw.shopName, status = __v_raw.status, goodsList = __v_raw.goodsList) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): OrderItemReactiveObject {
        return OrderItemReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var id: Number
        get() {
            return trackReactiveGet(__v_raw, "id", __v_raw.id, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("id")) {
                return;
            }
            val oldValue = __v_raw.id;
            __v_raw.id = value;
            triggerReactiveSet(__v_raw, "id", oldValue, value);
        }
    override var shopName: String
        get() {
            return trackReactiveGet(__v_raw, "shopName", __v_raw.shopName, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("shopName")) {
                return;
            }
            val oldValue = __v_raw.shopName;
            __v_raw.shopName = value;
            triggerReactiveSet(__v_raw, "shopName", oldValue, value);
        }
    override var status: Number
        get() {
            return trackReactiveGet(__v_raw, "status", __v_raw.status, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("status")) {
                return;
            }
            val oldValue = __v_raw.status;
            __v_raw.status = value;
            triggerReactiveSet(__v_raw, "status", oldValue, value);
        }
    override var goodsList: UTSArray<goodsItems1>
        get() {
            return trackReactiveGet(__v_raw, "goodsList", __v_raw.goodsList, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("goodsList")) {
                return;
            }
            val oldValue = __v_raw.goodsList;
            __v_raw.goodsList = value;
            triggerReactiveSet(__v_raw, "goodsList", oldValue, value);
        }
}
val OrderData = utsArrayOf<OrderItem>(OrderItem(id = 1, shopName = "华为官方自营旗舰店", status = 0, goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店", inventory = 3000))), OrderItem(id = 2, shopName = "荣耀官方自营旗舰店", status = 1, goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店", inventory = 10000))), OrderItem(id = 3, shopName = "小米官方自营旗舰店", status = 2, goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200))), OrderItem(id = 4, shopName = "华为官方自营旗舰店", status = 3, goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店", inventory = 10000), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店", inventory = 3000))), OrderItem(id = 5, shopName = "华为官方自营旗舰店", status = 4, goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店", inventory = 10000), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999, shopName = "华为官方自营旗舰店", inventory = 3000))));
val GenPagesOrderListComponentsOrderPageOrderPageClass = CreateVueComponent(GenPagesOrderListComponentsOrderPageOrderPage::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = GenPagesOrderListComponentsOrderPageOrderPage.name, inheritAttrs = GenPagesOrderListComponentsOrderPageOrderPage.inheritAttrs, inject = GenPagesOrderListComponentsOrderPageOrderPage.inject, props = GenPagesOrderListComponentsOrderPageOrderPage.props, propsNeedCastKeys = GenPagesOrderListComponentsOrderPageOrderPage.propsNeedCastKeys, emits = GenPagesOrderListComponentsOrderPageOrderPage.emits, components = GenPagesOrderListComponentsOrderPageOrderPage.components, styles = GenPagesOrderListComponentsOrderPageOrderPage.styles);
}
, fun(instance): GenPagesOrderListComponentsOrderPageOrderPage {
    return GenPagesOrderListComponentsOrderPageOrderPage(instance);
}
);
open class SwiperViewItem (
    @JsonNotNull
    open var type: String,
    @JsonNotNull
    open var name: String,
    @JsonNotNull
    open var preload: Boolean = false,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("SwiperViewItem", "pages/OrderList/OrderList.uvue", 60, 8)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return SwiperViewItemReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class SwiperViewItemReactiveObject : SwiperViewItem, IUTSReactive<SwiperViewItem> {
    override var __v_raw: SwiperViewItem;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: SwiperViewItem, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(type = __v_raw.type, name = __v_raw.name, preload = __v_raw.preload) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): SwiperViewItemReactiveObject {
        return SwiperViewItemReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var type: String
        get() {
            return trackReactiveGet(__v_raw, "type", __v_raw.type, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("type")) {
                return;
            }
            val oldValue = __v_raw.type;
            __v_raw.type = value;
            triggerReactiveSet(__v_raw, "type", oldValue, value);
        }
    override var name: String
        get() {
            return trackReactiveGet(__v_raw, "name", __v_raw.name, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("name")) {
                return;
            }
            val oldValue = __v_raw.name;
            __v_raw.name = value;
            triggerReactiveSet(__v_raw, "name", oldValue, value);
        }
    override var preload: Boolean
        get() {
            return trackReactiveGet(__v_raw, "preload", __v_raw.preload, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("preload")) {
                return;
            }
            val oldValue = __v_raw.preload;
            __v_raw.preload = value;
            triggerReactiveSet(__v_raw, "preload", oldValue, value);
        }
}
val GenPagesOrderListOrderListClass = CreateVueComponent(GenPagesOrderListOrderList::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesOrderListOrderList.inheritAttrs, inject = GenPagesOrderListOrderList.inject, props = GenPagesOrderListOrderList.props, propsNeedCastKeys = GenPagesOrderListOrderList.propsNeedCastKeys, emits = GenPagesOrderListOrderList.emits, components = GenPagesOrderListOrderList.components, styles = GenPagesOrderListOrderList.styles);
}
, fun(instance): GenPagesOrderListOrderList {
    return GenPagesOrderListOrderList(instance);
}
);
val GenPagesOrderDetailsOrderDetailsClass = CreateVueComponent(GenPagesOrderDetailsOrderDetails::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesOrderDetailsOrderDetails.inheritAttrs, inject = GenPagesOrderDetailsOrderDetails.inject, props = GenPagesOrderDetailsOrderDetails.props, propsNeedCastKeys = GenPagesOrderDetailsOrderDetails.propsNeedCastKeys, emits = GenPagesOrderDetailsOrderDetails.emits, components = GenPagesOrderDetailsOrderDetails.components, styles = GenPagesOrderDetailsOrderDetails.styles);
}
, fun(instance): GenPagesOrderDetailsOrderDetails {
    return GenPagesOrderDetailsOrderDetails(instance);
}
);
val GenPagesSettingSettingClass = CreateVueComponent(GenPagesSettingSetting::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesSettingSetting.inheritAttrs, inject = GenPagesSettingSetting.inject, props = GenPagesSettingSetting.props, propsNeedCastKeys = GenPagesSettingSetting.propsNeedCastKeys, emits = GenPagesSettingSetting.emits, components = GenPagesSettingSetting.components, styles = GenPagesSettingSetting.styles);
}
, fun(instance): GenPagesSettingSetting {
    return GenPagesSettingSetting(instance);
}
);
val GenPagesPersonalInfoPersonalInfoClass = CreateVueComponent(GenPagesPersonalInfoPersonalInfo::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesPersonalInfoPersonalInfo.inheritAttrs, inject = GenPagesPersonalInfoPersonalInfo.inject, props = GenPagesPersonalInfoPersonalInfo.props, propsNeedCastKeys = GenPagesPersonalInfoPersonalInfo.propsNeedCastKeys, emits = GenPagesPersonalInfoPersonalInfo.emits, components = GenPagesPersonalInfoPersonalInfo.components, styles = GenPagesPersonalInfoPersonalInfo.styles);
}
, fun(instance): GenPagesPersonalInfoPersonalInfo {
    return GenPagesPersonalInfoPersonalInfo(instance);
}
);
val GenPagesMemberCenterMemberCenterClass = CreateVueComponent(GenPagesMemberCenterMemberCenter::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesMemberCenterMemberCenter.inheritAttrs, inject = GenPagesMemberCenterMemberCenter.inject, props = GenPagesMemberCenterMemberCenter.props, propsNeedCastKeys = GenPagesMemberCenterMemberCenter.propsNeedCastKeys, emits = GenPagesMemberCenterMemberCenter.emits, components = GenPagesMemberCenterMemberCenter.components, styles = GenPagesMemberCenterMemberCenter.styles);
}
, fun(instance): GenPagesMemberCenterMemberCenter {
    return GenPagesMemberCenterMemberCenter(instance);
}
);
open class bannerItem1 (
    @JsonNotNull
    open var id: Number,
    @JsonNotNull
    open var pic: String,
    open var title: String? = null,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("bannerItem", "mock/bannerData.uts", 6, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return bannerItem1ReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class bannerItem1ReactiveObject : bannerItem1, IUTSReactive<bannerItem1> {
    override var __v_raw: bannerItem1;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: bannerItem1, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(id = __v_raw.id, pic = __v_raw.pic, title = __v_raw.title) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): bannerItem1ReactiveObject {
        return bannerItem1ReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var id: Number
        get() {
            return trackReactiveGet(__v_raw, "id", __v_raw.id, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("id")) {
                return;
            }
            val oldValue = __v_raw.id;
            __v_raw.id = value;
            triggerReactiveSet(__v_raw, "id", oldValue, value);
        }
    override var pic: String
        get() {
            return trackReactiveGet(__v_raw, "pic", __v_raw.pic, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("pic")) {
                return;
            }
            val oldValue = __v_raw.pic;
            __v_raw.pic = value;
            triggerReactiveSet(__v_raw, "pic", oldValue, value);
        }
    override var title: String?
        get() {
            return trackReactiveGet(__v_raw, "title", __v_raw.title, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("title")) {
                return;
            }
            val oldValue = __v_raw.title;
            __v_raw.title = value;
            triggerReactiveSet(__v_raw, "title", oldValue, value);
        }
}
val BannerData = utsArrayOf<bannerItem1>(bannerItem1(id = 1, pic = "https://m.360buyimg.com/babel/s1071x321_jfs/t20270109/235883/3/12114/47052/659e3034F21c8d0b7/7b7abb4f8cc2627c.jpg!q70.dpg", title = ""), bannerItem1(id = 2, pic = "https://m.360buyimg.com/babel/jfs/t1/232236/36/11109/182790/6597bae7Ff23261c1/11f54ec6423324ae.png", title = ""), bannerItem1(id = 3, pic = "https://m.360buyimg.com/babel/jfs/t20270102/245832/19/1737/93109/6594dd77F6bf121b2/8c8bbd22ed90d51f.png", title = ""));
val GenPagesSeckillSeckillClass = CreateVueComponent(GenPagesSeckillSeckill::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesSeckillSeckill.inheritAttrs, inject = GenPagesSeckillSeckill.inject, props = GenPagesSeckillSeckill.props, propsNeedCastKeys = GenPagesSeckillSeckill.propsNeedCastKeys, emits = GenPagesSeckillSeckill.emits, components = GenPagesSeckillSeckill.components, styles = GenPagesSeckillSeckill.styles);
}
, fun(instance): GenPagesSeckillSeckill {
    return GenPagesSeckillSeckill(instance);
}
);
open class CouponItem (
    @JsonNotNull
    open var title: String,
    @JsonNotNull
    open var fullPrice: Number,
    @JsonNotNull
    open var subPrice: Number,
    @JsonNotNull
    open var startDate: String,
    @JsonNotNull
    open var endDate: String,
    @JsonNotNull
    open var goodsList: UTSArray<goodsItems1>,
) : UTSReactiveObject(), IUTSSourceMap {
    override fun `__$getOriginalPosition`(): UTSSourceMapPosition? {
        return UTSSourceMapPosition("CouponItem", "mock/couponData.uts", 4, 13)
    }
    override fun __v_create(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): UTSReactiveObject {
        return CouponItemReactiveObject(this, __v_isReadonly, __v_isShallow, __v_skip)
    }
}
open class CouponItemReactiveObject : CouponItem, IUTSReactive<CouponItem> {
    override var __v_raw: CouponItem;
    override var __v_isReadonly: Boolean;
    override var __v_isShallow: Boolean;
    override var __v_skip: Boolean;
    constructor(__v_raw: CouponItem, __v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean) : super(title = __v_raw.title, fullPrice = __v_raw.fullPrice, subPrice = __v_raw.subPrice, startDate = __v_raw.startDate, endDate = __v_raw.endDate, goodsList = __v_raw.goodsList) {
        this.__v_raw = __v_raw;
        this.__v_isReadonly = __v_isReadonly;
        this.__v_isShallow = __v_isShallow;
        this.__v_skip = __v_skip;
    }
    override fun __v_clone(__v_isReadonly: Boolean, __v_isShallow: Boolean, __v_skip: Boolean): CouponItemReactiveObject {
        return CouponItemReactiveObject(this.__v_raw, __v_isReadonly, __v_isShallow, __v_skip);
    }
    override var title: String
        get() {
            return trackReactiveGet(__v_raw, "title", __v_raw.title, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("title")) {
                return;
            }
            val oldValue = __v_raw.title;
            __v_raw.title = value;
            triggerReactiveSet(__v_raw, "title", oldValue, value);
        }
    override var fullPrice: Number
        get() {
            return trackReactiveGet(__v_raw, "fullPrice", __v_raw.fullPrice, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("fullPrice")) {
                return;
            }
            val oldValue = __v_raw.fullPrice;
            __v_raw.fullPrice = value;
            triggerReactiveSet(__v_raw, "fullPrice", oldValue, value);
        }
    override var subPrice: Number
        get() {
            return trackReactiveGet(__v_raw, "subPrice", __v_raw.subPrice, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("subPrice")) {
                return;
            }
            val oldValue = __v_raw.subPrice;
            __v_raw.subPrice = value;
            triggerReactiveSet(__v_raw, "subPrice", oldValue, value);
        }
    override var startDate: String
        get() {
            return trackReactiveGet(__v_raw, "startDate", __v_raw.startDate, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("startDate")) {
                return;
            }
            val oldValue = __v_raw.startDate;
            __v_raw.startDate = value;
            triggerReactiveSet(__v_raw, "startDate", oldValue, value);
        }
    override var endDate: String
        get() {
            return trackReactiveGet(__v_raw, "endDate", __v_raw.endDate, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("endDate")) {
                return;
            }
            val oldValue = __v_raw.endDate;
            __v_raw.endDate = value;
            triggerReactiveSet(__v_raw, "endDate", oldValue, value);
        }
    override var goodsList: UTSArray<goodsItems1>
        get() {
            return trackReactiveGet(__v_raw, "goodsList", __v_raw.goodsList, this.__v_isReadonly, this.__v_isShallow);
        }
        set(value) {
            if (!this.__v_canSet("goodsList")) {
                return;
            }
            val oldValue = __v_raw.goodsList;
            __v_raw.goodsList = value;
            triggerReactiveSet(__v_raw, "goodsList", oldValue, value);
        }
}
val CouponData = utsArrayOf<CouponItem>(CouponItem(title = "手机3C满减优惠券", fullPrice = 100, subPrice = 10, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店"), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店"), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999))), CouponItem(title = "美妆满减优惠券", fullPrice = 80, subPrice = 5, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 6, name = "资生堂（Shiseido）悦薇水乳小样护肤品化妆品旅行套装 滋润滋养 新款中样4件：洁面+水+乳液+精华", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/121610/13/41125/70882/64ffdc37F4afe4517/775a22b274bddbc8.jpg!q80.dpg.webp", price = 276, originalPrice = 299.67, salesVolume = 235, shopName = "慕雪倾城美妆专营店", inventory = 300), goodsItems1(id = 7, name = "olayks电压力锅 高压锅 家用多功能高压电饭锅快煮智能预约小压力锅 3升适用3-5人用", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/218941/9/35528/25457/65755e41F35e7888c/fef994d37d44ecca.jpg!q70.dpg.webp", price = 299, originalPrice = 329, salesVolume = 97, shopName = "olayks自营旗舰店", inventory = 100), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200))), CouponItem(title = "服装满减优惠券", fullPrice = 600, subPrice = 120, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 8, name = "乐芬兰（LeFenLan）短外套女2023秋冬新款宽松气质羊羔毛小个子复古皮毛一体夹克上衣 卡其色 S", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/104119/12/39372/58031/6523addaF4a77719f/8b7fa71598a9e566.jpg!q80.dpg.webp", price = 158, originalPrice = 199, salesVolume = 999, shopName = "乐芬兰女装旗舰店", inventory = 10002), goodsItems1(id = 5, name = "尚都比拉秋季复古气质女神范针织连衣裙中长款显瘦a字裙 黑色 L ", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228639/27/5562/25890/6569ab29Ff3a9d0fe/91d6428be15da41c.jpg!q80.dpg.webp", price = 169, originalPrice = 199, salesVolume = 235, shopName = "尚都比拉自营旗舰店", inventory = 6000), goodsItems1(id = 4, name = "罗蒙羊毛双面呢大衣男士秋冬中长款风衣外套休闲保暖呢子大衣上衣男装", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", price = 272.34, originalPrice = 299, salesVolume = 235, shopName = "罗蒙（ROMON）自营专区", inventory = 500))), CouponItem(title = "手机3C满减优惠券", fullPrice = 100, subPrice = 10, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店"), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店"), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999))), CouponItem(title = "美妆满减优惠券", fullPrice = 80, subPrice = 5, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 6, name = "资生堂（Shiseido）悦薇水乳小样护肤品化妆品旅行套装 滋润滋养 新款中样4件：洁面+水+乳液+精华", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/121610/13/41125/70882/64ffdc37F4afe4517/775a22b274bddbc8.jpg!q80.dpg.webp", price = 276, originalPrice = 299.67, salesVolume = 235, shopName = "慕雪倾城美妆专营店", inventory = 300), goodsItems1(id = 7, name = "olayks电压力锅 高压锅 家用多功能高压电饭锅快煮智能预约小压力锅 3升适用3-5人用", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/218941/9/35528/25457/65755e41F35e7888c/fef994d37d44ecca.jpg!q70.dpg.webp", price = 299, originalPrice = 329, salesVolume = 97, shopName = "olayks自营旗舰店", inventory = 100), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200))), CouponItem(title = "服装满减优惠券", fullPrice = 600, subPrice = 120, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 8, name = "乐芬兰（LeFenLan）短外套女2023秋冬新款宽松气质羊羔毛小个子复古皮毛一体夹克上衣 卡其色 S", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/104119/12/39372/58031/6523addaF4a77719f/8b7fa71598a9e566.jpg!q80.dpg.webp", price = 158, originalPrice = 199, salesVolume = 999, shopName = "乐芬兰女装旗舰店", inventory = 10002), goodsItems1(id = 5, name = "尚都比拉秋季复古气质女神范针织连衣裙中长款显瘦a字裙 黑色 L ", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228639/27/5562/25890/6569ab29Ff3a9d0fe/91d6428be15da41c.jpg!q80.dpg.webp", price = 169, originalPrice = 199, salesVolume = 235, shopName = "尚都比拉自营旗舰店", inventory = 6000), goodsItems1(id = 4, name = "罗蒙羊毛双面呢大衣男士秋冬中长款风衣外套休闲保暖呢子大衣上衣男装", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", price = 272.34, originalPrice = 299, salesVolume = 235, shopName = "罗蒙（ROMON）自营专区", inventory = 500))), CouponItem(title = "手机3C满减优惠券", fullPrice = 100, subPrice = 10, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 1, name = "荣耀笔记本电脑MagicBook X 14 Pro 锐龙版 2023 R7-7840HS标压处理器 15h长续航 高色域护眼屏 高性能轻薄本", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/110990/16/38425/36858/64b90d71Fcf3e8f7d/598d2cbbbcfa5db9.jpg!q70.dpg.webp", price = 3697.2, originalPrice = 3999, salesVolume = 5000, shopName = "荣耀官方自营旗舰店"), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店"), goodsItems1(id = 3, name = "华为（HUAWEI）旗舰手机 Mate 60 Pro 12GB+512GB 雅川青", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/211556/35/38471/42993/64f1aa51F7ee8e4e5/5f8c4f9c149335ad.jpg!q70.dpg.webp", price = 6999, originalPrice = 7999, salesVolume = 999))), CouponItem(title = "美妆满减优惠券", fullPrice = 80, subPrice = 5, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 6, name = "资生堂（Shiseido）悦薇水乳小样护肤品化妆品旅行套装 滋润滋养 新款中样4件：洁面+水+乳液+精华", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/121610/13/41125/70882/64ffdc37F4afe4517/775a22b274bddbc8.jpg!q80.dpg.webp", price = 276, originalPrice = 299.67, salesVolume = 235, shopName = "慕雪倾城美妆专营店", inventory = 300), goodsItems1(id = 7, name = "olayks电压力锅 高压锅 家用多功能高压电饭锅快煮智能预约小压力锅 3升适用3-5人用", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/218941/9/35528/25457/65755e41F35e7888c/fef994d37d44ecca.jpg!q70.dpg.webp", price = 299, originalPrice = 329, salesVolume = 97, shopName = "olayks自营旗舰店", inventory = 100), goodsItems1(id = 2, name = "小米无线键鼠套装2 轻薄便携 全尺寸104键键盘鼠标套装 2.4G无线传输 电脑笔记本办公套装 键鼠套装", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228249/6/9834/49378/65960c9fF8a394ce7/d5ee187f85c95aef.jpg!q80.dpg.webp", price = 89.23, originalPrice = 99.12, salesVolume = 123, shopName = "小米官方自营旗舰店", inventory = 200))), CouponItem(title = "服装满减优惠券", fullPrice = 600, subPrice = 120, startDate = "2024.01.02", endDate = "2024.12.12", goodsList = utsArrayOf<goodsItems1>(goodsItems1(id = 8, name = "乐芬兰（LeFenLan）短外套女2023秋冬新款宽松气质羊羔毛小个子复古皮毛一体夹克上衣 卡其色 S", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/104119/12/39372/58031/6523addaF4a77719f/8b7fa71598a9e566.jpg!q80.dpg.webp", price = 158, originalPrice = 199, salesVolume = 999, shopName = "乐芬兰女装旗舰店", inventory = 10002), goodsItems1(id = 5, name = "尚都比拉秋季复古气质女神范针织连衣裙中长款显瘦a字裙 黑色 L ", pic = "https://m.360buyimg.com/mobilecms/s750x750_jfs/t1/228639/27/5562/25890/6569ab29Ff3a9d0fe/91d6428be15da41c.jpg!q80.dpg.webp", price = 169, originalPrice = 199, salesVolume = 235, shopName = "尚都比拉自营旗舰店", inventory = 6000), goodsItems1(id = 4, name = "罗蒙羊毛双面呢大衣男士秋冬中长款风衣外套休闲保暖呢子大衣上衣男装", pic = "https://m.360buyimg.com/mobilecms/s1265x1265_jfs/t1/220910/16/35955/54305/64e8bec4F86f0f1e7/8d78e685620a0712.jpg!q70.dpg.webp", price = 272.34, originalPrice = 299, salesVolume = 235, shopName = "罗蒙（ROMON）自营专区", inventory = 500))));
val GenPagesMyCouponMyCouponClass = CreateVueComponent(GenPagesMyCouponMyCoupon::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesMyCouponMyCoupon.inheritAttrs, inject = GenPagesMyCouponMyCoupon.inject, props = GenPagesMyCouponMyCoupon.props, propsNeedCastKeys = GenPagesMyCouponMyCoupon.propsNeedCastKeys, emits = GenPagesMyCouponMyCoupon.emits, components = GenPagesMyCouponMyCoupon.components, styles = GenPagesMyCouponMyCoupon.styles);
}
, fun(instance): GenPagesMyCouponMyCoupon {
    return GenPagesMyCouponMyCoupon(instance);
}
);
val GenPagesCouponUseRecordCouponUseRecordClass = CreateVueComponent(GenPagesCouponUseRecordCouponUseRecord::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesCouponUseRecordCouponUseRecord.inheritAttrs, inject = GenPagesCouponUseRecordCouponUseRecord.inject, props = GenPagesCouponUseRecordCouponUseRecord.props, propsNeedCastKeys = GenPagesCouponUseRecordCouponUseRecord.propsNeedCastKeys, emits = GenPagesCouponUseRecordCouponUseRecord.emits, components = GenPagesCouponUseRecordCouponUseRecord.components, styles = GenPagesCouponUseRecordCouponUseRecord.styles);
}
, fun(instance): GenPagesCouponUseRecordCouponUseRecord {
    return GenPagesCouponUseRecordCouponUseRecord(instance);
}
);
val GenPagesCouponCenterCouponCenterClass = CreateVueComponent(GenPagesCouponCenterCouponCenter::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesCouponCenterCouponCenter.inheritAttrs, inject = GenPagesCouponCenterCouponCenter.inject, props = GenPagesCouponCenterCouponCenter.props, propsNeedCastKeys = GenPagesCouponCenterCouponCenter.propsNeedCastKeys, emits = GenPagesCouponCenterCouponCenter.emits, components = GenPagesCouponCenterCouponCenter.components, styles = GenPagesCouponCenterCouponCenter.styles);
}
, fun(instance): GenPagesCouponCenterCouponCenter {
    return GenPagesCouponCenterCouponCenter(instance);
}
);
val GenPagesShopDetailsComponentsShopTabNarShopTabNarClass = CreateVueComponent(GenPagesShopDetailsComponentsShopTabNarShopTabNar::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenPagesShopDetailsComponentsShopTabNarShopTabNar.inheritAttrs, inject = GenPagesShopDetailsComponentsShopTabNarShopTabNar.inject, props = GenPagesShopDetailsComponentsShopTabNarShopTabNar.props, propsNeedCastKeys = GenPagesShopDetailsComponentsShopTabNarShopTabNar.propsNeedCastKeys, emits = GenPagesShopDetailsComponentsShopTabNarShopTabNar.emits, components = GenPagesShopDetailsComponentsShopTabNarShopTabNar.components, styles = GenPagesShopDetailsComponentsShopTabNarShopTabNar.styles);
}
, fun(instance): GenPagesShopDetailsComponentsShopTabNarShopTabNar {
    return GenPagesShopDetailsComponentsShopTabNarShopTabNar(instance);
}
);
val GenPagesShopDetailsShopDetailsClass = CreateVueComponent(GenPagesShopDetailsShopDetails::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesShopDetailsShopDetails.inheritAttrs, inject = GenPagesShopDetailsShopDetails.inject, props = GenPagesShopDetailsShopDetails.props, propsNeedCastKeys = GenPagesShopDetailsShopDetails.propsNeedCastKeys, emits = GenPagesShopDetailsShopDetails.emits, components = GenPagesShopDetailsShopDetails.components, styles = GenPagesShopDetailsShopDetails.styles);
}
, fun(instance): GenPagesShopDetailsShopDetails {
    return GenPagesShopDetailsShopDetails(instance);
}
);
val GenPagesShopDetailsComponentsGoodsListGoodsListClass = CreateVueComponent(GenPagesShopDetailsComponentsGoodsListGoodsList::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenPagesShopDetailsComponentsGoodsListGoodsList.inheritAttrs, inject = GenPagesShopDetailsComponentsGoodsListGoodsList.inject, props = GenPagesShopDetailsComponentsGoodsListGoodsList.props, propsNeedCastKeys = GenPagesShopDetailsComponentsGoodsListGoodsList.propsNeedCastKeys, emits = GenPagesShopDetailsComponentsGoodsListGoodsList.emits, components = GenPagesShopDetailsComponentsGoodsListGoodsList.components, styles = GenPagesShopDetailsComponentsGoodsListGoodsList.styles);
}
, fun(instance): GenPagesShopDetailsComponentsGoodsListGoodsList {
    return GenPagesShopDetailsComponentsGoodsListGoodsList(instance);
}
);
val GenPagesShopDetailsShopGoodsShopGoodsClass = CreateVueComponent(GenPagesShopDetailsShopGoodsShopGoods::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesShopDetailsShopGoodsShopGoods.inheritAttrs, inject = GenPagesShopDetailsShopGoodsShopGoods.inject, props = GenPagesShopDetailsShopGoodsShopGoods.props, propsNeedCastKeys = GenPagesShopDetailsShopGoodsShopGoods.propsNeedCastKeys, emits = GenPagesShopDetailsShopGoodsShopGoods.emits, components = GenPagesShopDetailsShopGoodsShopGoods.components, styles = GenPagesShopDetailsShopGoodsShopGoods.styles);
}
, fun(instance): GenPagesShopDetailsShopGoodsShopGoods {
    return GenPagesShopDetailsShopGoodsShopGoods(instance);
}
);
val GenPagesShopDetailsShopClassifyShopClassifyClass = CreateVueComponent(GenPagesShopDetailsShopClassifyShopClassify::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesShopDetailsShopClassifyShopClassify.inheritAttrs, inject = GenPagesShopDetailsShopClassifyShopClassify.inject, props = GenPagesShopDetailsShopClassifyShopClassify.props, propsNeedCastKeys = GenPagesShopDetailsShopClassifyShopClassify.propsNeedCastKeys, emits = GenPagesShopDetailsShopClassifyShopClassify.emits, components = GenPagesShopDetailsShopClassifyShopClassify.components, styles = GenPagesShopDetailsShopClassifyShopClassify.styles);
}
, fun(instance): GenPagesShopDetailsShopClassifyShopClassify {
    return GenPagesShopDetailsShopClassifyShopClassify(instance);
}
);
val GenPagesShopDetailsShopGoodsResultShopGoodsResultClass = CreateVueComponent(GenPagesShopDetailsShopGoodsResultShopGoodsResult::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesShopDetailsShopGoodsResultShopGoodsResult.inheritAttrs, inject = GenPagesShopDetailsShopGoodsResultShopGoodsResult.inject, props = GenPagesShopDetailsShopGoodsResultShopGoodsResult.props, propsNeedCastKeys = GenPagesShopDetailsShopGoodsResultShopGoodsResult.propsNeedCastKeys, emits = GenPagesShopDetailsShopGoodsResultShopGoodsResult.emits, components = GenPagesShopDetailsShopGoodsResultShopGoodsResult.components, styles = GenPagesShopDetailsShopGoodsResultShopGoodsResult.styles);
}
, fun(instance): GenPagesShopDetailsShopGoodsResultShopGoodsResult {
    return GenPagesShopDetailsShopGoodsResultShopGoodsResult(instance);
}
);
val GenPagesSpecialOfferSpecialOfferClass = CreateVueComponent(GenPagesSpecialOfferSpecialOffer::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesSpecialOfferSpecialOffer.inheritAttrs, inject = GenPagesSpecialOfferSpecialOffer.inject, props = GenPagesSpecialOfferSpecialOffer.props, propsNeedCastKeys = GenPagesSpecialOfferSpecialOffer.propsNeedCastKeys, emits = GenPagesSpecialOfferSpecialOffer.emits, components = GenPagesSpecialOfferSpecialOffer.components, styles = GenPagesSpecialOfferSpecialOffer.styles);
}
, fun(instance): GenPagesSpecialOfferSpecialOffer {
    return GenPagesSpecialOfferSpecialOffer(instance);
}
);
val GenPagesBrandBrandClass = CreateVueComponent(GenPagesBrandBrand::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesBrandBrand.inheritAttrs, inject = GenPagesBrandBrand.inject, props = GenPagesBrandBrand.props, propsNeedCastKeys = GenPagesBrandBrand.propsNeedCastKeys, emits = GenPagesBrandBrand.emits, components = GenPagesBrandBrand.components, styles = GenPagesBrandBrand.styles);
}
, fun(instance): GenPagesBrandBrand {
    return GenPagesBrandBrand(instance);
}
);
val GenPagesClassifyGoodsClassifyGoodsClass = CreateVueComponent(GenPagesClassifyGoodsClassifyGoods::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesClassifyGoodsClassifyGoods.inheritAttrs, inject = GenPagesClassifyGoodsClassifyGoods.inject, props = GenPagesClassifyGoodsClassifyGoods.props, propsNeedCastKeys = GenPagesClassifyGoodsClassifyGoods.propsNeedCastKeys, emits = GenPagesClassifyGoodsClassifyGoods.emits, components = GenPagesClassifyGoodsClassifyGoods.components, styles = GenPagesClassifyGoodsClassifyGoods.styles);
}
, fun(instance): GenPagesClassifyGoodsClassifyGoods {
    return GenPagesClassifyGoodsClassifyGoods(instance);
}
);
val GenPagesMessageMessageClass = CreateVueComponent(GenPagesMessageMessage::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesMessageMessage.inheritAttrs, inject = GenPagesMessageMessage.inject, props = GenPagesMessageMessage.props, propsNeedCastKeys = GenPagesMessageMessage.propsNeedCastKeys, emits = GenPagesMessageMessage.emits, components = GenPagesMessageMessage.components, styles = GenPagesMessageMessage.styles);
}
, fun(instance): GenPagesMessageMessage {
    return GenPagesMessageMessage(instance);
}
);
val GenPagesGoodsEvaluateGoodsEvaluateClass = CreateVueComponent(GenPagesGoodsEvaluateGoodsEvaluate::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesGoodsEvaluateGoodsEvaluate.inheritAttrs, inject = GenPagesGoodsEvaluateGoodsEvaluate.inject, props = GenPagesGoodsEvaluateGoodsEvaluate.props, propsNeedCastKeys = GenPagesGoodsEvaluateGoodsEvaluate.propsNeedCastKeys, emits = GenPagesGoodsEvaluateGoodsEvaluate.emits, components = GenPagesGoodsEvaluateGoodsEvaluate.components, styles = GenPagesGoodsEvaluateGoodsEvaluate.styles);
}
, fun(instance): GenPagesGoodsEvaluateGoodsEvaluate {
    return GenPagesGoodsEvaluateGoodsEvaluate(instance);
}
);
val GenPagesAfterSaleAfterSaleClass = CreateVueComponent(GenPagesAfterSaleAfterSale::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesAfterSaleAfterSale.inheritAttrs, inject = GenPagesAfterSaleAfterSale.inject, props = GenPagesAfterSaleAfterSale.props, propsNeedCastKeys = GenPagesAfterSaleAfterSale.propsNeedCastKeys, emits = GenPagesAfterSaleAfterSale.emits, components = GenPagesAfterSaleAfterSale.components, styles = GenPagesAfterSaleAfterSale.styles);
}
, fun(instance): GenPagesAfterSaleAfterSale {
    return GenPagesAfterSaleAfterSale(instance);
}
);
fun login(reqParams: UTSJSONObject): UTSPromise<UTSJSONObject> {
    suspend fun async(): Any? {
        var res: UTSJSONObject = await(getKey());
        console.log(JSON.stringify(res), " at apis/login.uts:9");
        var key: String = res.getString("data") as String;
        reqParams["password"] = encryptRSA(reqParams.getString("password")!!, key);
        reqParams["userType"] = "member";
        console.log(JSON.stringify(reqParams), " at apis/login.uts:17");
        return ins.post("/auth/login", reqParams);
    }
    return UTSPromise(fun(resolve, reject) {
        kotlinx.coroutines.CoroutineScope(io.dcloud.uts.UTSAndroid.getDomCoroutineDispatcher()).async {
            try {
                val result = async();
                resolve(if (result is UTSPromise<*>) {
                    @Suppress("UNCHECKED_CAST")
                    `await`(result as UTSPromise<UTSJSONObject>);
                } else {
                    result as UTSJSONObject;
                }
                );
            }
             catch (e: Throwable) {
                reject(e);
            }
        }
        ;
    }
    );
}
fun getKey(): UTSPromise<UTSJSONObject> {
    suspend fun async(): Any? {
        return ins.post("/auth/getKey", UTSJSONObject());
    }
    return UTSPromise(fun(resolve, reject) {
        kotlinx.coroutines.CoroutineScope(io.dcloud.uts.UTSAndroid.getDomCoroutineDispatcher()).async {
            try {
                val result = async();
                resolve(if (result is UTSPromise<*>) {
                    @Suppress("UNCHECKED_CAST")
                    `await`(result as UTSPromise<UTSJSONObject>);
                } else {
                    result as UTSJSONObject;
                }
                );
            }
             catch (e: Throwable) {
                reject(e);
            }
        }
        ;
    }
    );
}
val GenPagesLoginLoginClass = CreateVueComponent(GenPagesLoginLogin::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesLoginLogin.inheritAttrs, inject = GenPagesLoginLogin.inject, props = GenPagesLoginLogin.props, propsNeedCastKeys = GenPagesLoginLogin.propsNeedCastKeys, emits = GenPagesLoginLogin.emits, components = GenPagesLoginLogin.components, styles = GenPagesLoginLogin.styles);
}
, fun(instance): GenPagesLoginLogin {
    return GenPagesLoginLogin(instance);
}
);
var page: Number = 0;
var currentPageIsShow: Boolean = true;
val GenPagesDetailDetailClass = CreateVueComponent(GenPagesDetailDetail::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesDetailDetail.inheritAttrs, inject = GenPagesDetailDetail.inject, props = GenPagesDetailDetail.props, propsNeedCastKeys = GenPagesDetailDetail.propsNeedCastKeys, emits = GenPagesDetailDetail.emits, components = GenPagesDetailDetail.components, styles = GenPagesDetailDetail.styles);
}
, fun(instance): GenPagesDetailDetail {
    return GenPagesDetailDetail(instance);
}
);
fun createApp(): UTSJSONObject {
    val app = createSSRApp(GenAppClass);
    app.use(i18n1);
    app.component("page", GenComponentsPagePageClass);
    return object : UTSJSONObject() {
        var app = app
    };
}
fun main(app: IApp) {
    definePageRoutes();
    defineAppConfig();
    (createApp()["app"] as VueApp).mount(app);
}
open class UniAppConfig : AppConfig {
    override var name: String = "喵购";
    override var appid: String = "__UNI__5E903A2";
    override var versionName: String = "1.0.0";
    override var versionCode: String = "100";
    override var uniCompileVersion: String = "4.15";
    constructor(){}
}
fun definePageRoutes() {
    __uniRoutes.push(PageRoute(path = "pages/home/home", component = GenPagesHomeHomeClass, meta = PageMeta(isQuit = true), style = utsMapOf("navigationBarTitleText" to "首页", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/classify/classify", component = GenPagesClassifyClassifyClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "分类")));
    __uniRoutes.push(PageRoute(path = "pages/cart/cart", component = GenPagesCartCartClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "购物车", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/my/my", component = GenPagesMyMyClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "个人中心", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/search/search", component = GenPagesSearchSearchClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "搜索", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/SearchGoods/SearchGoods", component = GenPagesSearchGoodsSearchGoodsClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "商品列表", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/GoodsDetail/GoodsDetail", component = GenPagesGoodsDetailGoodsDetailClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "商品详情", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/ConfirmOrder/ConfirmOrder", component = GenPagesConfirmOrderConfirmOrderClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "确认订单", "enablePullDownRefresh" to false, "navigationBarBackgroundColor" to "#ffffff")));
    __uniRoutes.push(PageRoute(path = "pages/AddressList/AddressList", component = GenPagesAddressListAddressListClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "我的地址", "enablePullDownRefresh" to false, "navigationBarBackgroundColor" to "#ffffff")));
    __uniRoutes.push(PageRoute(path = "pages/AddressAddEdit/AddressAddEdit", component = GenPagesAddressAddEditAddressAddEditClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "新建地址", "enablePullDownRefresh" to false, "navigationBarBackgroundColor" to "#ffffff")));
    __uniRoutes.push(PageRoute(path = "pages/GoodsCollect/GoodsCollect", component = GenPagesGoodsCollectGoodsCollectClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "商品收藏", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/ShopCollect/ShopCollect", component = GenPagesShopCollectShopCollectClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "店铺收藏", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/OrderList/OrderList", component = GenPagesOrderListOrderListClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "订单列表", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/OrderDetails/OrderDetails", component = GenPagesOrderDetailsOrderDetailsClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "订单详情", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/setting/setting", component = GenPagesSettingSettingClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "设置", "enablePullDownRefresh" to false, "navigationBarBackgroundColor" to "#ffffff")));
    __uniRoutes.push(PageRoute(path = "pages/PersonalInfo/PersonalInfo", component = GenPagesPersonalInfoPersonalInfoClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "个人信息", "enablePullDownRefresh" to false, "navigationBarBackgroundColor" to "#ffffff")));
    __uniRoutes.push(PageRoute(path = "pages/MemberCenter/MemberCenter", component = GenPagesMemberCenterMemberCenterClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "会员中心", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/seckill/seckill", component = GenPagesSeckillSeckillClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "限时秒杀", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/MyCoupon/MyCoupon", component = GenPagesMyCouponMyCouponClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "我的优惠券", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/CouponUseRecord/CouponUseRecord", component = GenPagesCouponUseRecordCouponUseRecordClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "优惠券使用记录", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/CouponCenter/CouponCenter", component = GenPagesCouponCenterCouponCenterClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "领券中心", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/ShopDetails/ShopDetails", component = GenPagesShopDetailsShopDetailsClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "店铺详情", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/ShopDetails/ShopGoods/ShopGoods", component = GenPagesShopDetailsShopGoodsShopGoodsClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "店铺商品", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/ShopDetails/ShopClassify/ShopClassify", component = GenPagesShopDetailsShopClassifyShopClassifyClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "店铺分类", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/ShopDetails/ShopGoodsResult/ShopGoodsResult", component = GenPagesShopDetailsShopGoodsResultShopGoodsResultClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "店铺商品搜索", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/SpecialOffer/SpecialOffer", component = GenPagesSpecialOfferSpecialOfferClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "清仓特价", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/brand/brand", component = GenPagesBrandBrandClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "品牌", "enablePullDownRefresh" to false, "navigationStyle" to "custom")));
    __uniRoutes.push(PageRoute(path = "pages/ClassifyGoods/ClassifyGoods", component = GenPagesClassifyGoodsClassifyGoodsClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "分类商品", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/message/message", component = GenPagesMessageMessageClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "消息", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/GoodsEvaluate/GoodsEvaluate", component = GenPagesGoodsEvaluateGoodsEvaluateClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "评价", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/AfterSale/AfterSale", component = GenPagesAfterSaleAfterSaleClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "售后", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/login/login", component = GenPagesLoginLoginClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "登录", "enablePullDownRefresh" to false)));
    __uniRoutes.push(PageRoute(path = "pages/detail/detail", component = GenPagesDetailDetailClass, meta = PageMeta(isQuit = false), style = utsMapOf("navigationBarTitleText" to "")));
}
val __uniTabBar: Map<String, Any?>? = utsMapOf("color" to "#777777", "selectedColor" to "#fe9a5c", "borderStyle" to "white", "backgroundColor" to "#ffffff", "height" to "54px", "iconWidth" to "26px", "fontSize" to "12px", "custom" to true, "list" to utsArrayOf(
    utsMapOf("pagePath" to "pages/home/home", "iconPath" to "static/images/nav/home.png", "selectedIconPath" to "static/images/nav/home_ac.png", "text" to "首页"),
    utsMapOf("pagePath" to "pages/classify/classify", "iconPath" to "static/images/nav/buy.png", "selectedIconPath" to "static/images/nav/buy_ac.png", "text" to "分类"),
    utsMapOf("pagePath" to "pages/cart/cart", "iconPath" to "static/images/nav/cart.png", "selectedIconPath" to "static/images/nav/cart_ac.png", "text" to "购物车"),
    utsMapOf("pagePath" to "pages/my/my", "iconPath" to "static/images/nav/my.png", "selectedIconPath" to "static/images/nav/my_ac.png", "text" to "我的")
));
val __uniLaunchPage: Map<String, Any?> = utsMapOf("url" to "pages/home/home", "style" to utsMapOf("navigationBarTitleText" to "首页", "enablePullDownRefresh" to false, "navigationStyle" to "custom"));
@Suppress("UNCHECKED_CAST")
fun defineAppConfig() {
    __uniConfig.entryPagePath = "/pages/home/home";
    __uniConfig.globalStyle = utsMapOf("navigationBarTextStyle" to "black", "navigationBarTitleText" to "uni-app", "navigationBarBackgroundColor" to "#FFFFFF", "backgroundColor" to "#F8F8F8");
    __uniConfig.tabBar = __uniTabBar as Map<String, Any>?;
    __uniConfig.conditionUrl = "";
    __uniConfig.uniIdRouter = utsMapOf();
    __uniConfig.ready = true;
}
var `___$i18n` = i18n;
var `___$locale` = locale;
var VueComponent.`$i18n`
    get() = `___$i18n`
    set(value) {
        `___$i18n` = value;
    }
fun VueComponent.`$t`(key: String): String {
    return i18n.t(key);
}
var VueComponent.`$locale`
    get() = `___$locale`
    set(value) {
        `___$locale` = value;
    }
fun getApp(): GenApp {
    return getBaseApp() as GenApp;
}
