@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenComponentsSkeletonSkeleton : VueComponent {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onCreated(fun() {
            var that = this;
            if (that.animate) {
                that.erval = setInterval(fun(){
                    if (that.opacity == 1) {
                        that.opacity = 0.6;
                    } else {
                        that.opacity = 1;
                    }
                    if (!that.loading && that.erval != 0) {
                        clearInterval(that.erval);
                    }
                }
                , 500);
            }
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("view", utsMapOf("style" to normalizeStyle(utsMapOf("width" to "100%"))), utsArrayOf(
            if (isTrue(_ctx.loading)) {
                createElementVNode("view", utsMapOf("key" to 0, "class" to "skeleton", "style" to normalizeStyle(utsMapOf("flexDirection" to _ctx.flexDirection, "justifyContent" to _ctx.flexType, "opacity" to _ctx.opacity))), utsArrayOf(
                    if (isTrue(_ctx.imgTitle)) {
                        createElementVNode("view", utsMapOf("key" to 0, "class" to "skeleton-imgTitle", "style" to normalizeStyle(utsMapOf("width" to "95%", "border-radius" to "10px", "height" to "100px"))), null, 4);
                    } else {
                        createCommentVNode("v-if", true);
                    },
                    if (isTrue(_ctx.showAvatar && !_ctx.imgTitle)) {
                        createElementVNode(Fragment, utsMapOf("key" to 1), RenderHelpers.renderList(_ctx.nameRows, fun(_, index, _, _): VNode {
                            return createElementVNode("view", utsMapOf("class" to "skeleton-avatar", "key" to index, "style" to normalizeStyle(utsMapOf("width" to _ctx.avatarSize, "height" to _ctx.avatarSize, "borderRadius" to _ctx.borderRadius))), null, 4);
                        }), 128);
                    } else {
                        createCommentVNode("v-if", true);
                    },
                    if (isTrue(_ctx.showTitle && !_ctx.imgTitle)) {
                        createElementVNode("view", utsMapOf("key" to 2, "class" to "skeleton-content"), utsArrayOf(
                            if (isTrue(_ctx.titleW)) {
                                createElementVNode("view", utsMapOf("key" to 0, "class" to "skeleton-title", "style" to normalizeStyle(utsMapOf("width" to _ctx.titleWidth))), null, 4);
                            } else {
                                createCommentVNode("v-if", true);
                            },
                            createElementVNode("view", utsMapOf("class" to "skeleton-rows"), utsArrayOf(
                                createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.rowList, fun(item, index, _, _): VNode {
                                    return createElementVNode("view", utsMapOf("class" to "skeleton-row-item", "key" to index, "style" to normalizeStyle(utsMapOf("width" to item))), null, 4);
                                }), 128)
                            ))
                        ));
                    } else {
                        createCommentVNode("v-if", true);
                    },
                    if (isTrue(_ctx.showWaterfall)) {
                        createElementVNode("view", utsMapOf("key" to 3, "class" to "skeleton-waterfall"), utsArrayOf(
                            createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.row, fun(_, index1, _, _): VNode {
                                return createElementVNode("view", utsMapOf("class" to "skeleton-columns"), utsArrayOf(
                                    createElementVNode(Fragment, null, RenderHelpers.renderList(_ctx.column, fun(_, index, _, _): VNode {
                                        return createElementVNode("view", utsMapOf("class" to normalizeClass("skeleton-column-item-" + _ctx.column), "key" to index, "style" to normalizeStyle(utsMapOf("height" to _ctx.imgHeight, "borderRadius" to _ctx.borderRadius))), null, 6);
                                    }), 128)
                                ));
                            }), 256)
                        ));
                    } else {
                        createCommentVNode("v-if", true);
                    }
                ), 4);
            } else {
                createElementVNode("view", utsMapOf("key" to 1), utsArrayOf(
                    renderSlot(_ctx.`$slots`, "default")
                ));
            }
        ), 4);
    }
    open var loading: Boolean by `$props`;
    open var imgTitle: Boolean by `$props`;
    open var nameRow: Number by `$props`;
    open var flexType: String by `$props`;
    open var showAvatar: Boolean by `$props`;
    open var showWaterfall: Boolean by `$props`;
    open var avatarSize: String by `$props`;
    open var borderRadius: String by `$props`;
    open var showTitle: Boolean by `$props`;
    open var titleW: Boolean by `$props`;
    open var titleWidth: String by `$props`;
    open var imgHeight: String by `$props`;
    open var column: Number by `$props`;
    open var row: Number by `$props`;
    open var flexDirection: String by `$props`;
    open var animate: Boolean by `$props`;
    open var opacity: Number by `$data`;
    open var erval: Number by `$data`;
    open var rowList: UTSArray<String> by `$data`;
    open var nameRows: UTSArray<String> by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("opacity" to 1 as Number, "erval" to 0 as Number, "rowList" to computed<UTSArray<String>>(fun(): UTSArray<String> {
            var list: UTSArray<String> = utsArrayOf();
            run {
                var i: Number = 0;
                while(i < this.row){
                    list.push(if (i === this.row - 1 && i != 0) {
                        DEFAULT_LAST_ROW_WIDTH;
                    } else {
                        DEFAULT_ROW_WIDTH;
                    }
                    );
                    i++;
                }
            }
            return list;
        }
        ), "nameRows" to computed<UTSArray<String>>(fun(): UTSArray<String> {
            var list: UTSArray<String> = utsArrayOf();
            run {
                var i: Number = 0;
                while(i < this.nameRow){
                    list.push("1");
                    i++;
                }
            }
            return list;
        }
        ));
    }
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("skeleton" to padStyleMapOf(utsMapOf("width" to "100%", "flexWrap" to "wrap")), "skeleton-imgTitle" to padStyleMapOf(utsMapOf("flexWrap" to "wrap", "backgroundColor" to "#f2f3f5", "marginTop" to 10, "marginRight" to 10, "marginBottom" to 10, "marginLeft" to 10)), "skeleton-avatar" to padStyleMapOf(utsMapOf("flexShrink" to 0, "backgroundColor" to "#f2f3f5", "marginTop" to 10, "marginRight" to 10, "marginBottom" to 0, "marginLeft" to 10)), "skeleton-content" to padStyleMapOf(utsMapOf("width" to "100%", "marginTop" to 10, "marginRight" to 10, "marginBottom" to 10, "marginLeft" to 10)), "skeleton-title" to padStyleMapOf(utsMapOf("backgroundColor" to "#f2f3f5", "height" to 16, "marginTop" to 10)), "skeleton-rows" to padStyleMapOf(utsMapOf("marginTop" to 10, "width" to "100%")), "skeleton-row-item" to padStyleMapOf(utsMapOf("backgroundColor" to "#f2f3f5", "height" to 16, "marginTop" to 4)), "skeleton-waterfall" to padStyleMapOf(utsMapOf("width" to "100%", "paddingTop" to 10, "paddingRight" to 10, "paddingBottom" to 10, "paddingLeft" to 10)), "skeleton-columns" to padStyleMapOf(utsMapOf("marginTop" to 10, "width" to "100%", "display" to "flex", "flexDirection" to "row", "justifyContent" to "space-between", "flexWrap" to "wrap")), "skeleton-column-item" to padStyleMapOf(utsMapOf("backgroundColor" to "#f2f3f5", "height" to 164, "marginTop" to 4, "flex" to 1)), "skeleton-column-item-3" to padStyleMapOf(utsMapOf("backgroundColor" to "#f2f3f5", "width" to "31%", "marginTop" to 4)), "skeleton-column-item-2" to padStyleMapOf(utsMapOf("backgroundColor" to "#f2f3f5", "width" to "47%", "marginTop" to 4)), "skeleton-column-item-4" to padStyleMapOf(utsMapOf("backgroundColor" to "#f2f3f5", "width" to "23%", "marginTop" to 4)));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf("loading" to utsMapOf("type" to "Boolean", "default" to true), "imgTitle" to utsMapOf("type" to "Boolean", "default" to false), "nameRow" to utsMapOf("type" to "Number", "default" to 1), "flexType" to utsMapOf("type" to "String", "default" to "flex-start"), "showAvatar" to utsMapOf("type" to "Boolean", "default" to false), "showWaterfall" to utsMapOf("type" to "Boolean", "default" to false), "avatarSize" to utsMapOf("type" to "String", "default" to "50px"), "borderRadius" to utsMapOf("type" to "String", "default" to "25px"), "showTitle" to utsMapOf("type" to "Boolean", "default" to false), "titleW" to utsMapOf("type" to "Boolean", "default" to true), "titleWidth" to utsMapOf("type" to "String", "default" to "40%"), "imgHeight" to utsMapOf("type" to "String", "default" to "500"), "column" to utsMapOf("type" to "Number", "default" to 1), "row" to utsMapOf("type" to "Number", "default" to 1), "flexDirection" to utsMapOf("type" to "String", "default" to "row"), "animate" to utsMapOf("type" to "Boolean", "default" to true)));
        var propsNeedCastKeys = utsArrayOf(
            "loading",
            "imgTitle",
            "nameRow",
            "flexType",
            "showAvatar",
            "showWaterfall",
            "avatarSize",
            "borderRadius",
            "showTitle",
            "titleW",
            "titleWidth",
            "imgHeight",
            "column",
            "row",
            "flexDirection",
            "animate"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
