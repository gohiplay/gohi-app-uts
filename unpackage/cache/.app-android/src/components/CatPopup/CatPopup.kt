@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uni.UNI5E903A2;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getElementById as uni_getElementById;
open class GenComponentsCatPopupCatPopup : VueComponent {
    constructor(instance: ComponentInternalInstance) : super(instance) {
        onMounted(fun() {
            this.popup = uni_getElementById("popup");
        }
        , instance);
    }
    @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
    override fun `$render`(): Any? {
        val _ctx = this;
        val _cache = this.`$`.renderCache;
        return createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
            "popup-info",
            utsMapOf("show" to _ctx.show)
        )), "onClick" to _ctx.close), utsArrayOf(
            createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                "cat-dialog",
                _ctx.mode
            )), "id" to "popup", "onClick" to withModifiers(fun(){}, utsArrayOf(
                "stop"
            ))), utsArrayOf(
                renderSlot(_ctx.`$slots`, "default")
            ), 10, utsArrayOf(
                "onClick"
            ))
        ), 10, utsArrayOf(
            "onClick"
        ));
    }
    open var mode: String by `$props`;
    open var show: Boolean by `$data`;
    open var popup: Element? by `$data`;
    @Suppress("USELESS_CAST")
    override fun data(): Map<String, Any?> {
        return utsMapOf("show" to false, "popup" to null as Element?);
    }
    override fun `$initMethods`() {
        this.open = fun() {
            this.show = true;
            this.popup?.style?.setProperty(this.mode, "0px");
        }
        ;
        this.close = fun() {
            this.popup?.style?.setProperty(this.mode, "-1000px");
            setTimeout(fun(){
                this.show = false;
            }
            , 200);
        }
        ;
    }
    open lateinit var open: () -> Unit;
    open lateinit var close: () -> Unit;
    companion object {
        var name = "CatPopup";
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("popup-info" to padStyleMapOf(utsMapOf("position" to "fixed", "top" to 0, "right" to 0, "bottom" to 0, "left" to 0, "zIndex" to 999, "width" to "100%", "height" to "100%", "display" to "none", "backgroundColor" to "rgba(0,0,0,0.5)")), "bottom" to padStyleMapOf(utsMapOf("position" to "absolute", "left" to 0, "bottom" to -1000, "transitionProperty" to "bottom", "transitionTimingFunction" to "ease-in-out")), "top" to padStyleMapOf(utsMapOf("position" to "absolute", "left" to 0, "top" to -1000, "transitionProperty" to "top", "transitionTimingFunction" to "ease-in-out")), "left" to padStyleMapOf(utsMapOf("position" to "absolute", "left" to -1000, "top" to 0, "transitionProperty" to "left", "transitionTimingFunction" to "ease-in-out")), "right" to padStyleMapOf(utsMapOf("position" to "absolute", "right" to -1000, "top" to 0, "transitionProperty" to "right", "transitionTimingFunction" to "ease-in-out")), "show" to padStyleMapOf(utsMapOf("display" to "flex")), "@TRANSITION" to utsMapOf("bottom" to utsMapOf("property" to "bottom", "timingFunction" to "ease-in-out"), "top" to utsMapOf("property" to "top", "timingFunction" to "ease-in-out"), "left" to utsMapOf("property" to "left", "timingFunction" to "ease-in-out"), "right" to utsMapOf("property" to "right", "timingFunction" to "ease-in-out")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf("mode" to utsMapOf("type" to "String", "default" to "bottom")));
        var propsNeedCastKeys = utsArrayOf(
            "mode"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
