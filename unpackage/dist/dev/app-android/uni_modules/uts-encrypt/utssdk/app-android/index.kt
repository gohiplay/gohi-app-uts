@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME")
package uts.sdk.modules.utsEncrypt;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
fun encryptAES(data: String, key: String): String {
    var secretKeySpec = SecretKeySpec(getStringBytes(key), "AES");
    var cipher: Cipher = Cipher.getInstance("AES");
    cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
    var encryptedBytes = cipher.doFinal(getStringBytes(data));
    return Base64.getEncoder().encodeToString(encryptedBytes);
}
fun encryptRSA(data: String, key: String): String {
    var secretKeySpec = SecretKeySpec(getStringBytes(key), "RSA");
    var cipher: Cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
    var encryptedBytes = cipher.doFinal(getStringBytes(data));
    return Base64.getEncoder().encodeToString(encryptedBytes);
}
fun getStringBytes(str: String): ByteArray {
    val bytes: UTSArray<Number> = UTSArray<Number>();
    run {
        var i: Number = 0;
        while(i < str.length){
            var code = str.charCodeAt(i.toInt());
            if (code == null) {
                i++;
                continue;
            }
            if (code <= 0x7f) {
                bytes.push(code);
            } else if (code <= 0x7ff) {
                bytes.push(0xc0 or (code shr 6), 0x80 or (code and 0x3f));
            } else if (code >= 0xd800 && code <= 0xdfff) {
                val next = str.charCodeAt(i + 1);
                if (next == null) {
                    i++;
                    continue;
                }
                if (code >= 0xd800 && code <= 0xdbff && next >= 0xdc00 && next <= 0xdfff) {
                    val surrogatePair = (code - 0xd800) * 0x400 + next - 0xdc00 + 0x10000;
                    bytes.push(0xf0 or (surrogatePair shr 18), 0x80 or ((surrogatePair shr 12) and 0x3f));
                    bytes.push(0x80 or ((surrogatePair shr 6) and 0x3f), 0x80 or (surrogatePair and 0x3f));
                    i++;
                } else {
                    throw UTSError("Unmatched surrogate pair");
                }
            } else {
                bytes.push(0xe0 or (code shr 12), 0x80 or ((code shr 6) and 0x3f), 0x80 or (code and 0x3f));
            }
            i++;
        }
    }
    var length: Number = bytes.length;
    var byteArray: ByteArray = ByteArray(length.toInt());
    run {
        var i: Number = 0;
        while(i < bytes.length){
            byteArray.set(i.toInt(), bytes[i].toByte());
            i++;
        }
    }
    return byteArray;
}
